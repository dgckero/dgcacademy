/**
 * @author david
 */
package com.dgc.academy.service;

import javax.mail.MessagingException;

public interface MailService {
    void sendMail(String from, String to, String subject, String body, String name) throws MessagingException;
}
