/**
 * @author david
 */
package com.dgc.academy.service;

import com.dgc.academy.model.dao.LevelDao;
import com.dgc.academy.model.dao.SubjectDao;
import com.dgc.academy.model.dao.UserSubjectDao;
import com.dgc.academy.model.dao.WeekDayDao;
import com.dgc.academy.model.dto.*;
import com.dgc.academy.model.entity.Level;
import com.dgc.academy.model.entity.Subject;
import com.dgc.academy.model.entity.UserSubject;
import com.dgc.academy.model.entity.WeekDay;
import lombok.extern.log4j.Log4j2;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.Collection;
import java.util.List;

@Log4j2
@Service
public class SubjectServiceImpl implements SubjectService {

    @Autowired
    private ModelMapper modelMapper;
    @Autowired
    private SubjectDao subjectDao;
    @Autowired
    private LevelDao levelDao;
    @Autowired
    private UserSubjectDao userSubjectDao;
    @Autowired
    private WeekDayDao weekDayDao;
    @Autowired
    private ScheduleService scheduleService;

    @Override
    @Transactional
    public List<SubjectDto> getAllActiveSubjects() {
        log.debug("Obteniendo todas las asignaturas activas");
        try {
            List<Subject> subjects = subjectDao.findAll(Subject.class);
            if (subjects == null) {
                log.warn("No se han encontrado asignaturas activas");
                return null;
            } else {
                return mapSubjectEntityList(subjects);
            }
        } catch (Exception e) {
            log.error("Error mapeando a DTO " + e.getMessage());
            e.printStackTrace();
            return null;
        }
    }

    @Override
    @Transactional
    public void createSubject(SubjectDto subject) {

        log.info("Creando asignatura " + subject.toString());
        try {
            Subject subjectEntity = modelMapper.map(subject, Subject.class);

            subjectDao.save(subjectEntity);

            log.info("asignatura " + subject.getName() + " creada correctamente");

        } catch (Exception e) {
            log.error("Error creando asignatura " + subject.getName() + " error:" + e.getMessage());
            e.printStackTrace();
        }

    }

    @Override
    @Transactional
    public List<LevelDto> getActiveLevels() {
        log.debug("Obteniendo todos los cursos activos");
        try {
            List<Level> levels = levelDao.findAll(Level.class);
            if (levels == null) {
                log.warn("No se han encontrado cursos activos");
                return null;
            } else {
                List<LevelDto> levelsDto = modelMapper.map(levels, (new TypeToken<List<LevelDto>>() {
                }.getType()));

                log.info("Obtenidos " + levelsDto.size() + " curso(s) activos");
                return levelsDto;
            }
        } catch (Exception e) {
            log.error("Error mapeando a DTO " + e.getMessage());
            e.printStackTrace();
            return null;
        }
    }

    @Override
    @Transactional
    public Collection<SubjectDto> getAllSubjects() {
        log.debug("Obteniendo todas las asignaturas");

        try {
            Collection<Subject> subjectEntities = subjectDao.findAllIncludingInactives(Subject.class);
            if (subjectEntities == null) {
                log.warn("No se han encontrado asignaturas");
                return null;
            } else {
                return mapSubjectEntityList(subjectEntities);
            }
        } catch (Exception e) {
            log.error("Error mapeando a DTO " + e.getMessage());
            e.printStackTrace();
            return null;
        }

    }

    @Override
    @Transactional
    public void deleteSubject(SubjectDto selectedSubject) {
        log.info("Borrando Asignatura con nombre " + selectedSubject.getName());

        Subject subjectEntity = subjectDao.findById(selectedSubject.getId(), true);
        scheduleService.deleteAllSchedulerBySubject(modelMapper.map(subjectEntity, SubjectDto.class));

        subjectDao.delete(subjectEntity);

        log.info("Asignatura con nombre " + selectedSubject.getName() + " borrada correctamente");
    }

    @Override
    @Transactional
    public void assignUserToSubject(UserSubjectDto newUserSubject) {
        log.info("Creando la asignación " + newUserSubject);
        try {
            UserSubject userSubjectEntity = modelMapper.map(newUserSubject, UserSubject.class);

            userSubjectDao.save(userSubjectEntity);

            log.info("asignación " + userSubjectEntity + " creada correctamente");

        } catch (Exception e) {
            log.error("Error creando asignación " + newUserSubject + " error:" + e.getMessage());
            e.printStackTrace();
        }
    }

    @Override
    @Transactional
    public void updateSubject(SubjectDto selectedSubject) throws Exception {

        log.info("Modificando Asignatura " + selectedSubject);
        Subject subjectEntity = modelMapper.map(selectedSubject, Subject.class);

        try {
            subjectDao.update(subjectEntity);

            log.info("Asignatura  " + subjectEntity + " modificada correctamente");
        } catch (DataIntegrityViolationException e) {
            log.error("Ya existe una asignatura con el mismo nombre " + selectedSubject.getName() + " y curso " + selectedSubject.getLevel().getName() + " activa " + e.getCause());
            throw new Exception("Ya existe una asignatura con el nombre " + selectedSubject.getName() + " y curso " + selectedSubject.getLevel().getName() + " activa");
        }
    }

    @Override
    @Transactional
    public List<UserSubjectDto> getTeacherSubjects(UserDto user) {
        log.debug("Obteniendo todas las asignaturas que imparte el profesor " + user);

        Collection<UserSubject> userSubjectEntities = userSubjectDao.getTeacherSubjects(user.getId());
        if (userSubjectEntities == null) {
            log.warn("No se han encontrado asignaturas impartidas por el profesor " + user);
            return null;
        } else {
            try {
                List<UserSubjectDto> userSubjectDtos = modelMapper.map(userSubjectEntities, (new TypeToken<List<UserSubjectDto>>() {
                }.getType()));

                log.info("Obtenidas " + userSubjectDtos.size() + " asignaturas(s) impartidas por el profesor " + user);
                return userSubjectDtos;
            } catch (Exception e) {
                log.error("Error mapeando a DTO " + e.getMessage());
                e.printStackTrace();
                return null;
            }
        }
    }

    @Override
    @Transactional
    public Collection<WeekDayDto> getActiveWeekDays() {
        log.debug("Obteniendo todas los días activas");
        try {
            List<WeekDay> days = weekDayDao.findAll(WeekDay.class);
            if (days == null) {
                log.warn("No se han encontrado días activas");
                return null;
            } else {
                List<WeekDayDto> daysDto = modelMapper.map(days, (new TypeToken<List<WeekDayDto>>() {
                }.getType()));

                log.info("Obtenidos " + daysDto.size() + " dia(s)");
                return daysDto;
            }
        } catch (Exception e) {
            log.error("Error mapeando a DTO " + e.getMessage());
            e.printStackTrace();
            return null;
        }
    }

    @Override
    @Transactional
    public boolean subjectHasAlreadyTeacherAssigned(SubjectDto subject) {
        log.info("Comprobando si ya se ha asignado un profesor a la asignatura " + subject.getName());

        Collection<UserSubject> teacherAssigned = userSubjectDao.subjectHasAlreadyTeacherAssigned(subject.getId());
        if (CollectionUtils.isEmpty(teacherAssigned)) {
            log.info("NO existe un profesor asignado a la asignatura " + subject.getName());
        } else {
            log.info("Ya existe un profesor(" + teacherAssigned.iterator().next().getUser().getName() + ") asignado a la asignatura " + subject.getName());
        }
        return (!CollectionUtils.isEmpty(teacherAssigned));
    }

    @Override
    @Transactional
    public boolean studentAlreadyAssignedToSubject(SubjectDto subject, UserDto user) {
        log.info("Comprobando si ya se ha asignado previamente el usuario " + user.getLogin() + " a la asignatura " + subject.getName());

        Collection<UserSubject> studentAssigned = userSubjectDao.studentAlreadyAssignedToSubject(subject.getId(), user.getId());
        if (CollectionUtils.isEmpty(studentAssigned)) {
            log.info("El estudiante " + user.getLogin() + " NO esta asignado a la asignatura " + subject.getName());
        } else {
            log.info("El estudiante " + user.getLogin() + " ya ha sido previamente asignado a la asignatura " + subject.getName());
        }
        return (!CollectionUtils.isEmpty(studentAssigned));
    }

    @Override
    @Transactional
    public SubjectDto getSubjectById(Integer subjectId, boolean includeInactiveSubjects) {
        log.info("Obtener Asignatura con id " + subjectId + ", incluyendo asignaturas inactivas (" + includeInactiveSubjects + ")");

        Subject subjectEntity = null;
        if (includeInactiveSubjects) {
            subjectEntity = subjectDao.findById(subjectId, false);
        } else {
            subjectEntity = subjectDao.findById(subjectId, true);
        }

        if (subjectEntity == null) {
            log.warn("No se ha obtenido asignatura con id " + subjectId);
            return null;
        } else {
            log.info("Obtenida asignatura " + subjectEntity);
            return modelMapper.map(subjectEntity, SubjectDto.class);
        }
    }

    private List<SubjectDto> mapSubjectEntityList(Collection<Subject> subjectEntities) {
        try {
            List<SubjectDto> subjectsDto = modelMapper.map(subjectEntities, (new TypeToken<List<SubjectDto>>() {
            }.getType()));

            log.info("Obtenidas " + subjectsDto.size() + " asignaturas(s)");
            return subjectsDto;
        } catch (Exception e) {
            log.error("Error mapeando a DTO " + e.getMessage());
            e.printStackTrace();
            return null;
        }
    }
}
