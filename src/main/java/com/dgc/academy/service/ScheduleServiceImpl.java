/**
 * @author david
 */
package com.dgc.academy.service;

import com.dgc.academy.model.dao.ScheduleDao;
import com.dgc.academy.model.dto.ScheduleDto;
import com.dgc.academy.model.dto.SubjectDto;
import com.dgc.academy.model.dto.UserDto;
import com.dgc.academy.model.entity.Schedule;
import lombok.extern.log4j.Log4j2;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;

@Log4j2
@Service
public class ScheduleServiceImpl implements ScheduleService {

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private ScheduleDao scheduleDao;


    @Override
    @Transactional
    public List<ScheduleDto> getUserSchedule(UserDto user) {
        try {
            log.debug("Obteniendo el horario del usuario " + user);
            Collection<Schedule> schedules = scheduleDao.getUserSchedule(user.getId());
            if (schedules == null) {
                log.warn("No se han encontrado horarios activos");
                return null;
            } else {
                return mapSchedulerDtoList(schedules);
            }
        } catch (Exception e) {
            log.error("Error Obteniendo el horario del usuario " + user + " " + e.getMessage());
            e.printStackTrace();
            return null;
        }
    }

    @Override
    @Transactional
    public void createScheduler(ScheduleDto schedule) {

        log.info("Creando scheduler " + schedule);
        Schedule scheduleEntity = modelMapper.map(schedule, Schedule.class);

        scheduleDao.save(scheduleEntity);

        log.info("Horario " + scheduleEntity + " creado correctamente");
    }

    @Override
    @Transactional
    public List<ScheduleDto> getSchedulerBySubject(SubjectDto selectedSubject) {
        try {
            log.debug("Obteniendo horarios de la asignatura " + selectedSubject);
            Collection<Schedule> schedules = scheduleDao.getSchedulerBySubject(selectedSubject.getId());
            if (schedules == null) {
                log.warn("No se han encontrado horarios de la asignatura " + selectedSubject);
                return null;
            } else {
                return mapSchedulerDtoList(schedules);
            }
        } catch (Exception e) {
            log.error("Error Obteniendo horarios de la asignatura " + selectedSubject + ",error: " + e.getMessage());
            e.printStackTrace();
            return null;
        }
    }

    @Override
    @Transactional
    public void deleteScheduler(ScheduleDto schedule) {
        log.info("Borrando Horario " + schedule);

        Schedule scheduleEntity = modelMapper.map(schedule, Schedule.class);
        scheduleDao.delete(scheduleEntity);

        log.info("Horario borrado correctamente");
    }

    @Override
    public void deleteAllSchedulerBySubject(SubjectDto selectedSubject) {
        log.debug("Obteniendo horarios de la asignatura " + selectedSubject);
        Collection<Schedule> schedules = scheduleDao.getSchedulerBySubject(selectedSubject.getId());
        if (schedules == null) {
            log.warn("No se han encontrado horarios de la asignatura " + selectedSubject);
        } else {
            for (Schedule schedule : schedules) {
                scheduleDao.delete(schedule);
            }
        }
    }

    private List<ScheduleDto> mapSchedulerDtoList(Collection<Schedule> schedules) {

        List<ScheduleDto> schedulesDto = modelMapper.map(schedules, (new TypeToken<List<ScheduleDto>>() {
        }.getType()));
        log.info("Mapeados " + schedulesDto.size() + " horario(s)");
        return schedulesDto;
    }

}
