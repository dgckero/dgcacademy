/**
 * @author david
 */
package com.dgc.academy.service;

import com.dgc.academy.model.dto.ScheduleDto;
import com.dgc.academy.model.dto.SubjectDto;
import com.dgc.academy.model.dto.UserDto;
import lombok.NonNull;

import java.util.List;

public interface ScheduleService {
    List<ScheduleDto> getUserSchedule(@NonNull UserDto user);

    void createScheduler(@NonNull ScheduleDto newSchedule);

    List<ScheduleDto> getSchedulerBySubject(@NonNull SubjectDto selectedSubject);

    void deleteScheduler(@NonNull ScheduleDto schedule);

    void deleteAllSchedulerBySubject(@NonNull SubjectDto selectedSubject);
}
