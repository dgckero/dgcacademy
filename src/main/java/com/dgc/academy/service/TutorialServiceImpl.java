/**
 * @author david
 */
package com.dgc.academy.service;

import com.dgc.academy.model.dao.TutorialDao;
import com.dgc.academy.model.dao.TutorialStatusDao;
import com.dgc.academy.model.dto.TutorialDto;
import com.dgc.academy.model.dto.TutorialStatusDto;
import com.dgc.academy.model.dto.UserDto;
import com.dgc.academy.model.entity.Tutorial;
import com.dgc.academy.model.entity.TutorialStatus;
import lombok.extern.log4j.Log4j2;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;

@Log4j2
@Service
public class TutorialServiceImpl implements TutorialService {

    @Autowired
    private ModelMapper modelMapper;
    @Autowired
    private TutorialDao tutorialDao;
    @Autowired
    private TutorialStatusDao tutorialStatusDao;

    @Override
    @Transactional
    public void createTutorial(TutorialDto tutorial) {

        log.info("Creando Tutoria " + tutorial.toString());

        Tutorial tutorialEntity = modelMapper.map(tutorial, Tutorial.class);
        tutorialDao.save(tutorialEntity);

        log.info("Tutoría " + tutorial.toString() + " creada correctamente");

    }

    @Override
    @Transactional
    public void updateTutorial(TutorialDto tutorial) {

        log.info("Modificando tutoria " + tutorial.toString());

        Tutorial tutorialEntity = modelMapper.map(tutorial, Tutorial.class);

        tutorialDao.update(tutorialEntity);

        log.info("Tutoría " + tutorial.toString() + " modififcada correctamente");
    }


    @Override
    @Transactional
    public List<TutorialStatusDto> getActiveTutorialStatus() {
        log.debug("Obteniendo todos los estados de las tutorías activos");
        try {
            List<TutorialStatus> tutorialStatus = tutorialStatusDao.findAll(TutorialStatus.class);
            if (tutorialStatus == null) {
                log.warn("No se han encontrado estados de tutorias activos");
                return null;
            } else {
                List<TutorialStatusDto> tutorialStatusDto = modelMapper.map(tutorialStatus, (new TypeToken<List<TutorialStatusDto>>() {
                }.getType()));

                log.info("Obtenidos " + tutorialStatusDto.size() + " estados(s) de tutorías activos");
                return tutorialStatusDto;
            }
        } catch (Exception e) {
            log.error("Error Obteniendo todos los estados de las tutorías activos" + e.getMessage());
            e.printStackTrace();
            return null;
        }
    }

    @Override
    @Transactional
    public Collection<TutorialDto> getTutorialsByUser(UserDto user) {
        log.info("Obtener tutorías del usuario " + user.getLogin());

        Collection<Tutorial> tutorialsEntity = tutorialDao.getTutorialsByUserId(user.getId());

        if (tutorialsEntity == null) {
            log.warn("No se han obtenido tutorías para el usuario " + user.getLogin());
            return null;
        } else {
            log.info("Obtenidas " + tutorialsEntity.size() + " tutoría(s) para el usuario " + user.getLogin());

            markReadAllPendingUserTutorial(tutorialsEntity);
            log.info("Marcadas tutorías como leídas");

            return mapTutorialEntityListToDto(tutorialsEntity);
        }
    }


    private void markReadAllPendingUserTutorial(Collection<Tutorial> tutorials) {
        try {
            for (Tutorial tutorial : tutorials) {
                tutorial.setNotified(true);
                tutorialDao.update(tutorial);
            }

            log.debug("Marcadas todas las tutorías pendientes como leídas");
        } catch (Exception e) {
            log.error("Error marcando tutorías como leídas, error " + e.getMessage());
            e.printStackTrace();
        }
    }


    @Override
    @Transactional
    public Collection<TutorialDto> getTutorialsByTeacher(UserDto teacher) {
        log.info("Obtener tutorías del profesor " + teacher.getLogin());

        Collection<Tutorial> tutorialsEntity = tutorialDao.getTutorialsByTeacherId(teacher.getId());

        if (tutorialsEntity == null) {
            log.warn("No se han obtenido tutorías para el profesor " + teacher.getLogin());
            return null;
        } else {
            log.info("Obtenidas " + tutorialsEntity.size() + " tutoría(s) para el profesor " + teacher.getLogin());
            return mapTutorialEntityListToDto(tutorialsEntity);
        }
    }

    @Override
    @Transactional
    public Collection<TutorialDto> getUnReadTutorialsByTeacher(UserDto teacher) {
        log.info("Obtener tutorías No leidas del profesor " + teacher.getLogin());

        Collection<Tutorial> tutorialsEntity = tutorialDao.getUnReadTutorialsByTeacherId(teacher.getId());

        if (tutorialsEntity == null) {
            log.warn("No se han obtenido tutorías No Leídas para el profesor " + teacher.getLogin());
            return null;
        } else {
            log.info("Obtenidas " + tutorialsEntity.size() + " tutoría(s) No Leídas para el profesor " + teacher.getLogin());
            return mapTutorialEntityListToDto(tutorialsEntity);
        }
    }

    @Override
    @Transactional
    public Collection<TutorialDto> getUnReadTutorialsByUser(UserDto user) {
        log.info("Obtener tutorías No leídas del usuario " + user.getLogin());

        Collection<Tutorial> tutorialsEntity = tutorialDao.getUnReadTutorialsByUserId(user.getId());

        if (tutorialsEntity == null) {
            log.warn("No se han obtenido tutorías No leídas para el usuario " + user.getLogin());
            return null;
        } else {
            log.info("Obtenidas " + tutorialsEntity.size() + " tutoría(s) No leídas para el usuario " + user.getLogin());
            return mapTutorialEntityListToDto(tutorialsEntity);
        }
    }

    @Override
    @Transactional
    public TutorialDto getTutorialById(Integer tutorialId) {
        log.info("Obtener tutorías con id " + tutorialId);

        Tutorial tutorialEntity = tutorialDao.findById(tutorialId, true);

        if (tutorialEntity == null) {
            log.warn("No se ha obtenido tutoría con id " + tutorialId);
            return null;
        } else {
            log.info("Obtenida tutoría " + tutorialEntity);
            return modelMapper.map(tutorialEntity, TutorialDto.class);
        }
    }

    private Collection<TutorialDto> mapTutorialEntityListToDto(Collection<Tutorial> tutorialsEntity) {
        try {
            Collection<TutorialDto> tutorialsDto = modelMapper.map(tutorialsEntity, (new TypeToken<List<TutorialDto>>() {
            }.getType()));

            return tutorialsDto;

        } catch (Exception e) {
            log.error("Error mapeando tutorías a DTO " + e.getMessage());
            e.printStackTrace();
            return null;
        }
    }

}
