/**
 * @author david
 */
package com.dgc.academy.service;

import lombok.extern.log4j.Log4j2;
import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;
import org.jasypt.properties.PropertyValueEncryptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

@Log4j2
@Service
public class MailServiceImpl implements MailService {

    @Autowired
    JavaMailSenderImpl mailSender;

    @Autowired
    StandardPBEStringEncryptor configurationEncryptor;

    @Override
    public void sendMail(String from, String to, String subject, String body, String name) throws MessagingException {

        MimeMessage message = generateEmailMessage(from, to, subject, body, name);

        log.info("Enviando mensaje " + message.toString());
        decodeSmtpPassword();
        mailSender.send(message);
        log.info("Mensaje enviado correctamente");

    }

    private void decodeSmtpPassword() {
        log.debug("Decoding SMTP password");

        String encryptedPassword = mailSender.getPassword();

        if (PropertyValueEncryptionUtils.isEncryptedValue(encryptedPassword)) {
            log.debug("Valor de SMTP password esta encriptado desencriptando valor ");
            mailSender.setPassword(PropertyValueEncryptionUtils.decrypt(encryptedPassword, configurationEncryptor));
        } else {
            log.debug("Valor de hibernate.connection.driver_class NO esta encriptado");
        }

    }

    private MimeMessage generateEmailMessage(String from, String to, String subject, String body, String name) throws MessagingException {

        MimeMessage mail = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(mail, true);
        helper.setFrom(from);
        helper.setTo(to);
        helper.setSubject(subject);
        helper.setText(generateEmailBodyHtml(from, body, name), true);

        return mail;
    }

    private String generateEmailBodyHtml(String from, String body, String name) {

        String htmlMessage = "\n" +
                "<html>\n" +
                "<body>\n" +
                "<h4>Se ha recibido un mensaje en la aplicación DGCAcademy con los siguientes datos:</h4>" +
                "    <h5>Nombre del emisor: " + name + "</h5>\n" +
                "    <h5>Contacto del emisor: <a title=\"" + from + "\" href=\"mailto:" + from + "\">" + from + "</a>\n" +
                "  \t<h5>Mensaje:</h5>\n" +
                "    <h7>" + body + "</h7>  " +
                "</body>\n" +
                "</html>";

        log.debug("Mensaje HTML a enviar " + htmlMessage);

        return htmlMessage;
    }
}
