/**
 * @author david
 */
package com.dgc.academy.service;

import com.dgc.academy.model.dto.TutorialDto;
import com.dgc.academy.model.dto.TutorialStatusDto;
import com.dgc.academy.model.dto.UserDto;
import lombok.NonNull;

import java.util.Collection;
import java.util.List;

public interface TutorialService {

    void createTutorial(@NonNull TutorialDto tutorial);

    void updateTutorial(@NonNull TutorialDto tutorial);

    List<TutorialStatusDto> getActiveTutorialStatus();

    Collection<TutorialDto> getTutorialsByUser(@NonNull UserDto user);

    Collection<TutorialDto> getTutorialsByTeacher(@NonNull UserDto teacher);

    Collection<TutorialDto> getUnReadTutorialsByTeacher(@NonNull UserDto user);

    Collection<TutorialDto> getUnReadTutorialsByUser(@NonNull UserDto user);

    TutorialDto getTutorialById(@NonNull Integer tutorialId);
}
