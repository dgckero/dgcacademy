/**
 * @author david
 */
package com.dgc.academy.service;

import com.dgc.academy.model.dao.RoleDao;
import com.dgc.academy.model.dao.UserDao;
import com.dgc.academy.model.dto.RoleDto;
import com.dgc.academy.model.dto.UserDto;
import com.dgc.academy.model.entity.Role;
import com.dgc.academy.model.entity.User;
import com.dgc.academy.util.Encryption;
import lombok.extern.log4j.Log4j2;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import static com.dgc.academy.util.GlobalConstant.STUDENT_ROLE;
import static com.dgc.academy.util.GlobalConstant.TEACHER_ROLE;

@Log4j2
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserDao userDao;
    @Autowired
    RoleDao roleDao;
    @Autowired
    private ModelMapper modelMapper;

    @Override
    @Transactional
    public UserDto login(String login, String password) {
        log.info("Login de usuario " + login);
        User userEntity = userDao.login(login, Encryption.encrypt(password));

        if (userEntity == null) {
            log.warn("Login de usuario " + login + " No encontrado");
            return null;
        } else {
            try {
                log.debug("añadir fecha último login actual");
                userEntity.setLastLogin(new Date());
                userDao.update(userEntity);

                UserDto user = modelMapper.map(userEntity, UserDto.class);
                log.info("Login de usuario obtiene " + ((user == null) ? "Usuario no encontrado" : user));
                return user;
            } catch (Exception e) {
                log.error("Error mapeando a DTO " + e.getMessage());
                e.printStackTrace();
                return null;
            }
        }
    }

    @Override
    @Transactional
    public void createUser(UserDto user) {

        log.info("Creando Usuario con login " + user.getLogin());

        User userEntity = modelMapper.map(user, User.class);
        userEntity.setPassword(Encryption.encrypt(userEntity.getPassword()));

        if (user.getRole() == null) {
            log.debug("Asignando rol Alumno al usuario con login " + user.getLogin());
            userEntity.setRole(getRoleByDescription(STUDENT_ROLE));
        }

        userDao.save(userEntity);

        log.info("Usuario con login " + user.getLogin() + " creado correctamente");
    }

    @Override
    @Transactional
    public void updateUser(UserDto user, boolean updatePassword) {

        log.info("Modificando Usuario con login " + user.getLogin());

        User userEntity = modelMapper.map(user, User.class);

        if (updatePassword) {
            log.debug("Modificando contraseña del usuario con login " + user.getLogin());
            userEntity.setPassword(Encryption.encrypt(userEntity.getPassword()));
        }

        if (user.getRole() == null) {
            log.debug("Asignando rol Alumno al usuario con login " + user.getLogin());
            userEntity.setRole(getRoleByDescription(STUDENT_ROLE));
        }

        userDao.update(userEntity);

        log.info("Usuario con login " + user.getLogin() + " modififcado correctamente");
    }

    @Override
    @Transactional
    public List<RoleDto> getActiveRoles() {
        log.debug("Obteniendo todos los roles activos");

        try {
            List<Role> roles = roleDao.findAll(Role.class);
            if (roles == null) {
                log.warn("No se han encontrado roles activos");
                return null;
            } else {
                List<RoleDto> subjectsDto = modelMapper.map(roles, (new TypeToken<List<RoleDto>>() {
                }.getType()));

                log.info("Obtenidos " + subjectsDto.size() + " role(s) activos");
                return subjectsDto;
            }
        } catch (Exception e) {
            log.error("Error mapeando a DTO " + e.getMessage());
            e.printStackTrace();
            return null;
        }
    }

    @Override
    @Transactional
    public List<UserDto> getActiveStudents() {
        Role roleStudent = roleDao.getRoleByDescription(STUDENT_ROLE);
        return getActiveUsersByRole(roleStudent);
    }

    @Override
    @Transactional
    public List<UserDto> getActiveTeachers() {
        Role roleStudent = roleDao.getRoleByDescription(TEACHER_ROLE);
        return getActiveUsersByRole(roleStudent);
    }

    @Override
    @Transactional
    public Collection<UserDto> getAllUsers() {
        log.debug("Obteniendo todos los usuarios");

        try {
            Collection<User> userEntities = userDao.findAllIncludingInactives(User.class);
            if (userEntities == null) {
                log.warn("No se han encontrado usuarios");
                return null;
            } else {
                List<UserDto> usersDto = modelMapper.map(userEntities, (new TypeToken<List<UserDto>>() {
                }.getType()));

                log.info("Obtenidos " + usersDto.size() + " usuario(s)");
                return usersDto;
            }
        } catch (Exception e) {
            log.error("Error mapeando a DTO " + e.getMessage());
            e.printStackTrace();
            return null;
        }

    }

    @Override
    @Transactional
    public void deleteUser(UserDto user) {
        log.info("Borrando Usuario con login " + user.getLogin());

        User userEntity = modelMapper.map(user, User.class);
        userDao.delete(userEntity);

        log.info("Usuario con login " + user.getLogin() + " borrado correctamente");
    }

    @Override
    @Transactional
    public UserDto getUserByLogin(String login) {
        log.info("Obteniendo usuario con login " + login);
        User userEntity = userDao.getUserByLogin(login);

        if (userEntity == null) {
            log.warn("Usuario con login " + login + " No encontrado");
            return null;
        } else {
            try {
                UserDto user = modelMapper.map(userEntity, UserDto.class);
                log.info("Usuario obtenido " + ((user == null) ? "Usuario no encontrado" : user));
                return user;
            } catch (Exception e) {
                log.error("Error mapeando a DTO " + e.getMessage());
                e.printStackTrace();
                return null;
            }
        }
    }

    @Override
    @Transactional
    public UserDto getUserById(Integer userId, boolean includeInactiveUsers) {
        log.info("Obtener Usuario con id " + userId + ", incluyendo usuarios inactivos (" + includeInactiveUsers + ")");

        User userEntity = null;
        if (includeInactiveUsers) {
            userEntity = userDao.findById(userId, false);
        } else {
            userEntity = userDao.findById(userId, true);
        }

        if (userEntity == null) {
            log.warn("No se ha obtenido usuario con id " + userId);
            return null;
        } else {
            log.info("Obtenido usuario " + userEntity);
            return modelMapper.map(userEntity, UserDto.class);
        }
    }

    private List<UserDto> getActiveUsersByRole(Role role) {
        log.info("get Active Users By Role " + role.toString());
        List<User> users = userDao.getActiveUsersByRole(role);

        if (users == null || users.isEmpty()) {
            log.warn("No encontrados ususarios con rol " + role.toString());
            return null;
        } else {
            try {
                List<UserDto> usersDto = modelMapper.map(users, (new TypeToken<List<UserDto>>() {
                }.getType()));

                log.info("Encontrados " + usersDto.size() + " ususarios con rol " + role.toString());

                return usersDto;
            } catch (Exception e) {
                log.error("Error mapeando a DTO " + e.getMessage());
                e.printStackTrace();
                return null;
            }
        }
    }

    private Role getRoleByDescription(String roleDescription) {
        log.info("Obteniendo role por descripción " + roleDescription);
        Role rol = roleDao.getRoleByDescription(roleDescription);
        log.info((rol == null) ? "ROL No encontrado " : "rol Encontrado: " + rol.toString());
        return rol;
    }

}
