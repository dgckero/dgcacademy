/**
 * @author david
 */
package com.dgc.academy.service;

import com.dgc.academy.model.dto.RoleDto;
import com.dgc.academy.model.dto.UserDto;
import lombok.NonNull;

import java.util.Collection;
import java.util.List;

public interface UserService {

    /**
     * @param login
     * @param password
     * @return user
     */
    UserDto login(@NonNull String login, @NonNull String password);

    void createUser(@NonNull UserDto user);

    void updateUser(@NonNull UserDto user, boolean updatePassword);

    List<RoleDto> getActiveRoles();

    List<UserDto> getActiveStudents();

    List<UserDto> getActiveTeachers();

    Collection<UserDto> getAllUsers();

    void deleteUser(@NonNull UserDto selectedUser);

    UserDto getUserByLogin(@NonNull String login);

    UserDto getUserById(@NonNull Integer userId, boolean includeInactiveUsers);
}
