/**
 * @author david
 */
package com.dgc.academy.service;

import com.dgc.academy.model.dto.*;
import lombok.NonNull;

import java.util.Collection;
import java.util.List;

public interface SubjectService {

    List<SubjectDto> getAllActiveSubjects();

    void createSubject(@NonNull SubjectDto subject);

    List<LevelDto> getActiveLevels();

    Collection<SubjectDto> getAllSubjects();

    void deleteSubject(@NonNull SubjectDto selectedSubject);

    void assignUserToSubject(@NonNull UserSubjectDto newUserSubject);

    void updateSubject(@NonNull SubjectDto selectedSubject) throws Exception;

    List<UserSubjectDto> getTeacherSubjects(@NonNull UserDto user);

    Collection<WeekDayDto> getActiveWeekDays();

    boolean subjectHasAlreadyTeacherAssigned(@NonNull SubjectDto subject);

    boolean studentAlreadyAssignedToSubject(@NonNull SubjectDto subject, UserDto user);

    SubjectDto getSubjectById(@NonNull Integer subjectId, boolean includeInactiveSubjects);
}
