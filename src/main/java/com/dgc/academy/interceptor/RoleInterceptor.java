/**
 * @author david
 */
package com.dgc.academy.interceptor;

import com.dgc.academy.model.dto.UserDto;
import com.dgc.academy.util.GlobalConstant;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.AbstractInterceptor;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.StrutsStatics;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

@Log4j2
@Getter
@Setter
@Component("roleInterceptor")
public class RoleInterceptor extends AbstractInterceptor implements
        StrutsStatics {

    private List<String> allowedRoles = new ArrayList<>();
    private List<String> disallowedRoles = new ArrayList<>();

    public void init() {
        log.info("Inicializando RoleInterceptor");
    }

    public void destroy() {
        log.info("Destroy RoleInterceptor");
    }


    public String intercept(ActionInvocation invocation) throws Exception {
        HttpServletRequest request = ServletActionContext.getRequest();
        HttpServletResponse response = ServletActionContext.getResponse();
        String result;
        if (!isAllowed(request, invocation.getAction())) {
            result = handleRejection(invocation, response);
        } else {
            result = invocation.invoke();
        }
        return result;
    }

    /**
     * Determines if the request should be allowed for the action
     *
     * @param request The request
     * @param action  The action object
     * @return True if allowed, false otherwise
     */
    protected boolean isAllowed(HttpServletRequest request, Object action) {
        HttpSession session = request.getSession(false);
        boolean result = false;
        UserDto loginUser = null;
        if (session != null) {
            loginUser = (UserDto) session.getAttribute(GlobalConstant.USER_HANDLE);
        }

        if (allowedRoles.size() > 0) {
            if (session == null || loginUser == null) {
                return false;
            }
            for (String role : allowedRoles) {
                if (role.equalsIgnoreCase(loginUser.getRole().getDescription())) {
                    log.debug("Usuario " + loginUser.getLogin() + " Permitido");
                    result = true;
                }
            }
            return result;
        } else if (disallowedRoles.size() > 0) {
            if (session != null) {
                for (String role : disallowedRoles) {
                    if (role.equalsIgnoreCase(loginUser.getRole().getDescription())) {
                        log.warn("Usuario " + loginUser.getLogin() + " NO Permitido");
                        return false;
                    }
                }
            }
        }
        return true;
    }

    /**
     * Handles a rejection by sending a 403 HTTP error
     *
     * @param invocation
     * @param response
     * @return
     * @throws Exception
     */
    protected String handleRejection(ActionInvocation invocation,
                                     HttpServletResponse response)
            throws Exception {

        response.sendError(HttpServletResponse.SC_FORBIDDEN);
        return GlobalConstant.GO_TO_INDEX;
    }
}
