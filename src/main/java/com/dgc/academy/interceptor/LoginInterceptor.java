/**
 * @author david
 */
package com.dgc.academy.interceptor;

import com.dgc.academy.action.user.LoginAction;
import com.dgc.academy.util.GlobalConstant;
import com.dgc.academy.util.NoLogginRequired;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.AbstractInterceptor;
import lombok.extern.log4j.Log4j2;
import org.apache.struts2.StrutsStatics;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import static com.dgc.academy.util.GlobalConstant.GO_TO_INDEX;

@Log4j2
@Component("loginInterceptor")
public class LoginInterceptor extends AbstractInterceptor implements
        StrutsStatics {

    public void init() {
        log.info("Inicializando LoginInterceptor");
    }

    public void destroy() {
        log.info("Destroy LoginInterceptor");
    }

    public String intercept(ActionInvocation invocation) throws Exception {

        Class<?> clazz = invocation.getAction().getClass();

        if (clazz.isAnnotationPresent(NoLogginRequired.class)) {
            log.info("No loggin requerido para " + clazz.getName());
            return invocation.invoke();
        } else {
            log.debug("Loggin requerido para " + clazz.getName());

            final ActionContext context = invocation.getInvocationContext();
            HttpServletRequest request = (HttpServletRequest) context
                    .get(HTTP_REQUEST);
            HttpSession session = request.getSession(true);

            // Is there a "user" object stored in the user's HttpSession?
            Object user = session.getAttribute(GlobalConstant.USER_HANDLE);
            if (user == null) { // The user has not logged in yet.
                /* The user is attempting to log in. */
                if (clazz.equals(LoginAction.class)) {
                    return invocation.invoke();
                }
                return GO_TO_INDEX;
            } else {
                return invocation.invoke();
            }
        }

    }

}
