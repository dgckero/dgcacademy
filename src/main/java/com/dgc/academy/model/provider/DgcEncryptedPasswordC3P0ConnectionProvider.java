/**
 * @author david
 */

package com.dgc.academy.model.provider;

import lombok.extern.log4j.Log4j2;
import org.hibernate.c3p0.internal.C3P0ConnectionProvider;
import org.hibernate.cfg.AvailableSettings;
import org.jasypt.encryption.pbe.PBEStringEncryptor;
import org.jasypt.encryption.pbe.config.EnvironmentStringPBEConfig;
import org.jasypt.exceptions.EncryptionInitializationException;
import org.jasypt.hibernate4.connectionprovider.ParameterNaming;
import org.jasypt.hibernate4.encryptor.HibernatePBEEncryptorRegistry;
import org.jasypt.properties.PropertyValueEncryptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import java.util.Map;

@Log4j2
@Configuration
public class DgcEncryptedPasswordC3P0ConnectionProvider extends C3P0ConnectionProvider {

    @Autowired
    private EnvironmentStringPBEConfig environmentVariablesConfiguration;

    public DgcEncryptedPasswordC3P0ConnectionProvider() {
        super();
    }

    public void configure(Map props) {

        log.info("Configurando ecnrypted password provider");
        final String encryptorRegisteredName = (String) props.get(ParameterNaming.ENCRYPTOR_REGISTERED_NAME);
        log.info("encryptorRegisteredName " + encryptorRegisteredName);

        PBEStringEncryptor encryptor = getEncryptor(encryptorRegisteredName);

        log.info("encryptor " + encryptor);

        if (encryptor == null) {
            throw new EncryptionInitializationException(
                    "No string encryptor registered for hibernate " +
                            "with name \"" + encryptorRegisteredName + "\"");
        }

        // Get the original values, which may be encrypted
        try {
            decryptValue(encryptor, props, AvailableSettings.DRIVER);
            decryptValue(encryptor, props, AvailableSettings.URL);
            decryptValue(encryptor, props, AvailableSettings.USER);
            decryptValue(encryptor, props, AvailableSettings.PASS);
        } catch (NullPointerException e) {
            log.fatal("NO SE HA CREADO LA VAIRABLE DE ENTORNO (DGCACADEMY_ENC_PASSWORD) CON LA CONTRASEÑA DE ENCRIPTACIÓN");
        } catch (Exception e) {
            log.error("Error desencriptando valor " + e.getMessage());
            e.printStackTrace();
        }

        super.configure(props);

    }

    /**
     * @param encryptorRegisteredName
     * @return
     */
    private PBEStringEncryptor getEncryptor(String encryptorRegisteredName) {
        final HibernatePBEEncryptorRegistry encryptorRegistry =
                HibernatePBEEncryptorRegistry.getInstance();

        return encryptorRegistry.getPBEStringEncryptor(encryptorRegisteredName);
    }

    /**
     * Perform decryption operations as needed and store the new values
     *
     * @param encryptor
     * @param props
     * @param propertyName
     * @throws Exception
     */
    private void decryptValue(PBEStringEncryptor encryptor, Map props, String propertyName) throws Exception {
        String propertyValue = (String) props.get(propertyName);
        if (PropertyValueEncryptionUtils.isEncryptedValue(propertyValue)) {
            log.debug("Valor de " + propertyName + " esta encriptado desencriptando valor ");
            props.put(
                    propertyName,
                    PropertyValueEncryptionUtils.decrypt(propertyValue, encryptor));
        } else {
            log.debug("Valor de " + propertyName + " NO esta encriptado");
        }
    }
}