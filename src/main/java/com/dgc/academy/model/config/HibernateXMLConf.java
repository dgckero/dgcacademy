/**
 * @author david
 */

package com.dgc.academy.model.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
@ImportResource(
        {
                "classpath:spring/applicationContext.xml",
                "classpath:spring/persistenceContext.xml",
                "classpath:bbdd/hibernate.cfg.xml"
        }
)
public class HibernateXMLConf {
}
