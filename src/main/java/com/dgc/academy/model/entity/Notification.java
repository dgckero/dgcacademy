/**
 * @author david
 */
package com.dgc.academy.model.entity;
// Generated 10-oct-2019 16:17:29 by Hibernate Tools 4.3.1

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.util.Date;

import static javax.persistence.GenerationType.IDENTITY;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "notification", catalog = "DBAcademia"
)
@DynamicUpdate
public class Notification implements java.io.Serializable {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "ID", unique = true, updatable = false, nullable = false)
    private Integer id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "DESTINATION_USER", nullable = false)
    private User userByDestinationUser;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "ORIGIN_USER", nullable = false)
    private User userByOriginUser;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "CREATE_DATE", nullable = false, length = 19)
    private Date createDate;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "READ_TIME", nullable = false, length = 19)
    private Date readTime;

    @Column(name = "MESSAGE", nullable = false, length = 150)
    private String message;

}
