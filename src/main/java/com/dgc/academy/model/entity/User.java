/**
 * @author david
 */
package com.dgc.academy.model.entity;
// Generated 10-oct-2019 16:17:29 by Hibernate Tools 4.3.1

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.*;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.*;
import java.util.Date;

import static com.dgc.academy.util.GlobalConstant.ACTIVE_FILTER;
import static javax.persistence.GenerationType.IDENTITY;

@Data
@NoArgsConstructor
@AllArgsConstructor

@FilterDef(name = ACTIVE_FILTER, parameters = @ParamDef(name = "active", type = "boolean"))
@Filters({
        @Filter(name = ACTIVE_FILTER, condition = "active = :active")
})

@Entity
@Table(name = "user", catalog = "DBAcademia", uniqueConstraints = @UniqueConstraint(columnNames = {"LOGIN", "ACTIVE"})
)
@DynamicUpdate
public class User implements java.io.Serializable {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "ID", unique = true, updatable = false, nullable = false)
    private Integer id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "ROLE", nullable = false)
    private Role role;

    @Column(name = "NAME", length = 16)
    private String name;

    @Column(name = "SURNAME", length = 32)
    private String surname;

    @Column(name = "ACTIVE", nullable = false)
    private boolean active;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "LAST_LOGIN", length = 19)
    private Date lastLogin;

    @Column(name = "LOGIN", length = 10)
    private String login;

    @Column(name = "PASSWORD", length = 60, columnDefinition = "char")
    private String password;

}
