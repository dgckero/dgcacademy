/**
 * @author david
 */
package com.dgc.academy.model.entity;
// Generated 10-oct-2019 16:17:29 by Hibernate Tools 4.3.1

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.util.Date;

import static javax.persistence.GenerationType.IDENTITY;

@Data
@NoArgsConstructor
@AllArgsConstructor

@Entity
@Table(name = "tutorial", catalog = "DBAcademia", uniqueConstraints = @UniqueConstraint(columnNames = {"START_TIME", "END_TIME", "TEACHER", "STUDENT", "TUTORIAL_STATUS"})
)
@DynamicUpdate
public class Tutorial implements java.io.Serializable {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "ID", unique = true, updatable = false, nullable = false)
    private Integer id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "TUTORIAL_STATUS", nullable = false)
    private TutorialStatus tutorialStatus;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "STUDENT", nullable = false)
    private User student;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "TEACHER", nullable = false)
    private User teacher;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "START_TIME", nullable = false, length = 19)
    private Date startTime;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "END_TIME", nullable = false, length = 19)
    private Date endTime;

    @Column(name = "COMMENT")
    private String comment;

    @Column(name = "NOTIFIED", nullable = false)
    private boolean notified;
}
