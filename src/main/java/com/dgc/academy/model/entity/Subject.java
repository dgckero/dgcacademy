/**
 * @author david
 */
package com.dgc.academy.model.entity;
// Generated 10-oct-2019 16:17:29 by Hibernate Tools 4.3.1

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.*;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.*;

import static com.dgc.academy.util.GlobalConstant.ACTIVE_FILTER;
import static javax.persistence.GenerationType.IDENTITY;

@Data
@NoArgsConstructor
@AllArgsConstructor

@FilterDef(name = ACTIVE_FILTER, parameters = @ParamDef(name = "active", type = "boolean"))
@Filters({
        @Filter(name = ACTIVE_FILTER, condition = "active = :active")
})

@Entity
@Table(name = "subject", catalog = "DBAcademia", uniqueConstraints = @UniqueConstraint(columnNames = {"NAME", "LEVEL", "ACTIVE"})
)
@DynamicUpdate
public class Subject implements java.io.Serializable {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "ID", unique = true, updatable = false, nullable = false)
    private Integer id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "LEVEL", nullable = false)
    private Level level;

    @Column(name = "NAME", length = 40)
    private String name;

    @Column(name = "ACTIVE", nullable = false)
    private boolean active;

}
