/**
 * @author david
 */
package com.dgc.academy.model.entity;
// Generated 10-oct-2019 16:17:29 by Hibernate Tools 4.3.1

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;

import static javax.persistence.GenerationType.IDENTITY;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "tutorial_status", catalog = "DBAcademia", uniqueConstraints = @UniqueConstraint(columnNames = "NAME")
)
@DynamicUpdate
public class TutorialStatus implements java.io.Serializable {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "ID", unique = true, updatable = false, nullable = false)
    private int id;

    @Column(name = "NAME", unique = true, length = 10)
    private String name;

    @Column(name = "DESCRIPTION", length = 100)
    private String description;

}
