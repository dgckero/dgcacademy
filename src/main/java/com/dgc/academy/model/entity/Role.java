/**
 * @author david
 */
package com.dgc.academy.model.entity;
// Generated 10-oct-2019 16:17:29 by Hibernate Tools 4.3.1

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "role", catalog = "DBAcademia", uniqueConstraints = @UniqueConstraint(columnNames = "DESCRIPTION")
)
@DynamicUpdate
public class Role implements java.io.Serializable {

    @Id
    @Column(name = "ID", unique = true, updatable = false, nullable = false)
    private int id;

    @Column(name = "DESCRIPTION", unique = true, length = 16)
    private String description;

}
