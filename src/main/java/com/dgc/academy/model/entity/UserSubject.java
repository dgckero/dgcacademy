/**
 * @author david
 */
package com.dgc.academy.model.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.*;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.*;
import java.io.Serializable;

import static com.dgc.academy.util.GlobalConstant.ACTIVE_FILTER;
import static javax.persistence.GenerationType.IDENTITY;

@Data
@NoArgsConstructor
@AllArgsConstructor

@FilterDef(name = ACTIVE_FILTER, parameters = @ParamDef(name = "active", type = "boolean"))
@Filters({
        @Filter(name = ACTIVE_FILTER, condition = "active = :active")
})

@Entity
@Table(name = "user_subject", catalog = "DBAcademia", uniqueConstraints = @UniqueConstraint(columnNames = {"USER", "SUBJECT", "ACTIVE"})
)
@DynamicUpdate
public class UserSubject implements Serializable {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "ID", unique = true, updatable = false, nullable = false)
    private Integer id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "USER", nullable = false)
    private User user;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "SUBJECT", nullable = false)
    @Fetch(value = FetchMode.SELECT)
    private Subject subject;

    @Column(name = "ACTIVE", nullable = false)
    private boolean active;
}
