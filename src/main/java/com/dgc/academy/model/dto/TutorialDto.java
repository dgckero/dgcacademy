/**
 * @author david
 */
package com.dgc.academy.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class TutorialDto implements Serializable {

    private Integer id;
    private TutorialStatusDto tutorialStatus;
    private UserDto student;
    private UserDto teacher;
    private Date startTime;
    private Date endTime;
    private String comment;
    private boolean notified;
}