/**
 * @author david
 */
package com.dgc.academy.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserSubjectDto implements Serializable {

    private Integer id;
    private UserDto user;
    private SubjectDto subject;
    private boolean active;

}
