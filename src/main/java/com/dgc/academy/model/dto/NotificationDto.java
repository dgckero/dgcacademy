/**
 * @author david
 */
package com.dgc.academy.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class NotificationDto implements Serializable {

    private Integer id;
    private UserDto userByDestinationUser;
    private UserDto userByOriginUser;
    private Date createDate;
    private Date readTime;
    private String message;
}
