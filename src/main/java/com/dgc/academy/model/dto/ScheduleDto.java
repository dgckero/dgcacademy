/**
 * @author david
 */
package com.dgc.academy.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ScheduleDto implements Serializable {

    private Integer id;
    private SubjectDto subject;
    private WeekDayDto day;
    private String startTime;
    private String endTime;
    private boolean active;

}
