/**
 * @author david
 */
package com.dgc.academy.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserDto implements Serializable {

    private Integer id;
    private RoleDto role;
    private String name;
    private String surname;
    private boolean active;
    private Date lastLogin;
    private String login;
    private String password;
}
