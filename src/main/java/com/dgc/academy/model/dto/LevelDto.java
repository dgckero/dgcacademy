/**
 * @author david
 */
package com.dgc.academy.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class LevelDto implements Serializable {

    private Integer id;
    private String name;
    private boolean active;

}
