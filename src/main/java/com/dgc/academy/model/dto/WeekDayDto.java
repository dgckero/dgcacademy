/**
 * @author david
 */
package com.dgc.academy.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class WeekDayDto implements Serializable {

    private Byte id;
    private String name;
    private boolean active;

}
