/**
 * @author david
 */
package com.dgc.academy.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class EventDto implements Serializable {
    private Integer id;
    private String start;
    private String end;
    private String textColor;
    private boolean allDay;
    private String title;
}
