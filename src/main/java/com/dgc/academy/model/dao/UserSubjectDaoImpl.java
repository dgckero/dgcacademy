/**
 * @author david
 */
package com.dgc.academy.model.dao;

import com.dgc.academy.model.entity.UserSubject;
import com.dgc.academy.util.GlobalConstant;
import lombok.extern.log4j.Log4j2;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Log4j2
@Repository
public class UserSubjectDaoImpl extends CommonDaoImpl<Integer, UserSubject> implements UserSubjectDao {

    @Autowired
    public void init(SessionFactory sessionFactory) {
        setSessionFactory(sessionFactory);
    }

    @Override
    public Collection<UserSubject> getTeacherSubjects(Integer teacherId) {
        try {
            Session session = getOpenSession(true);

            Query query = session.createQuery("from UserSubject us where us.user.id = :teacherId");
            query.setParameter("teacherId", teacherId);
            Collection<UserSubject> result = query.list();

            log.debug("Asignaturas obtenidas correctamente para el profesor con Id" + teacherId);

            return result;
        } catch (Exception e) {
            log.error("Error obteniendo asignaturas para el profesor con Id " + teacherId + ", error " + e.getMessage());
            e.printStackTrace();

            return null;
        }

    }

    @Override
    public Collection<UserSubject> subjectHasAlreadyTeacherAssigned(Integer subjectId) {
        try {
            Session session = getOpenSession(true);

            Query query = session.createQuery("from UserSubject us where us.subject.id = :subjectId " +
                    "and us.user.role.description =:teacherRole");
            query.setParameter("subjectId", subjectId);
            query.setParameter("teacherRole", GlobalConstant.TEACHER_ROLE);

            Collection<UserSubject> result = query.list();

            if (result != null && !result.isEmpty()) {
                log.debug("Se ha encontrado profesor asignado a la asignatura ");
            } else {
                log.debug("NO Se han encontrado profesor asignado a la asignatura ");
            }

            return result;
        } catch (Exception e) {
            log.error("Error obteniendo profesores asignados a la asignatura con Id " + subjectId + ", error " + e.getMessage());
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Collection<UserSubject> studentAlreadyAssignedToSubject(Integer subjectId, Integer studentId) {
        try {
            Session session = getOpenSession(true);

            Query query = session.createQuery("from UserSubject us where us.subject.id = :subjectId " +
                    "and us.user.id =:studentId");
            query.setParameter("subjectId", subjectId);
            query.setParameter("studentId", studentId);

            Collection<UserSubject> result = query.list();

            if (result != null && !result.isEmpty()) {
                log.debug("El usuario con id " + studentId + " NO esta asignado a la asignatura con id " + subjectId);
            } else {
                log.debug("El usuario con id " + studentId + " esta asignado a la asignatura con id " + subjectId);
            }

            return result;
        } catch (Exception e) {
            log.error("Error obteniendo profesores asignados a la asignatura con Id " + subjectId + ", error " + e.getMessage());
            e.printStackTrace();
            return null;
        }
    }
}
