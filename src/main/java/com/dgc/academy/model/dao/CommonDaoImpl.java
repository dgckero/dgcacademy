/**
 * @author david
 */
package com.dgc.academy.model.dao;

import lombok.extern.log4j.Log4j2;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate5.support.HibernateDaoSupport;
import org.springframework.stereotype.Repository;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;

import static com.dgc.academy.util.GlobalConstant.ACTIVE_FILTER;

@Log4j2
@Repository
public abstract class CommonDaoImpl<K extends Serializable, E> extends HibernateDaoSupport implements CommonDao<K, E> {


    protected Class<E> entityClass;

    @Autowired
    private SessionFactory sessionFactory;

    public CommonDaoImpl() {
        ParameterizedType genericSuperclass = (ParameterizedType) getClass().getGenericSuperclass();
        try {
            this.entityClass = (Class<E>) genericSuperclass.getActualTypeArguments()[1];
        } catch (ArrayIndexOutOfBoundsException e) {
            log.error("Error obteniendo entityClass " + e.getMessage());
        }
    }

    @Override
    public void save(E entity) {

        Session session = getOpenSession(true);
        session.saveOrUpdate(entity);

        log.debug(entityClass.getName() + " guardado correctamente");
    }

    @Override
    public void update(E entity) {

        Session session = getOpenSession(true);
        session.update(entity);

        log.debug(entityClass.getName() + " modificado correctamente");
    }

    @Override
    public void delete(E entity) {

        Session session = getOpenSession(true);

        session.delete(entity);
        session.flush();

        log.debug(entityClass.getName() + " borrada correctamente");
    }

    @Override
    public E findById(K id, boolean activeFilterEnabled) {

        try {
            Session session = getOpenSession(activeFilterEnabled);

            E ent = (E) session.byId(entityClass).getReference(id);

            log.debug(entityClass.getName() + " busqueda por id " + id + " correcta");
            return ent;
        } catch (Exception e) {
            log.error("Error obteniendo " + entityClass.getName() + " por id (" + id + "), error " + e.getMessage());
            e.printStackTrace();

            return null;
        }
    }

    @Override
    public List<E> findAll(Class<? extends Serializable> entityClass) {

        try {
            Session session = getOpenSession(true);

            Query query = session.createQuery("from " + entityClass.getSimpleName());
            List<E> result = query.list();

            log.debug(entityClass.getName() + " búsqueda de todos los elementos devuelve " + result.size() + " elementos");
            return result;
        } catch (Exception e) {
            log.error("Error obteniendo todas las entidades de " + entityClass.getName() + ", error " + e.getMessage());
            e.printStackTrace();

            return null;
        }
    }

    @Override
    public List<E> findAllIncludingInactives(Class<? extends Serializable> entityClass) {

        try {
            Session session = getOpenSession(false);

            Query query = session.createQuery("from " + entityClass.getSimpleName());
            List<E> result = query.list();

            log.debug(entityClass.getName() + " búsqueda de todos los elementos devuelve " + result.size() + " elementos");
            return result;
        } catch (Exception e) {
            log.error("Error obteniendo todas las entidades de " + entityClass.getName() + ", error " + e.getMessage());
            e.printStackTrace();

            return null;
        }
    }

    protected Session getOpenSession(boolean activeFilterEnabled) {
        Session session = getOpenSession();

        enableDisableFilter(session, activeFilterEnabled);

        return session;
    }

    private Session getOpenSession() {
        Session session = sessionFactory.getCurrentSession();
        if (session == null) {
            log.debug("No hay sesión activa, obteniendo nueva sesión");
            return sessionFactory.openSession();
        }
        log.debug("obtenida sesión activa");
        return session;
    }

    private void enableDisableFilter(Session session, boolean activeFilterEnabled) {
        if (activeFilterEnabled) {
            log.debug("Fitro activeFilter activado");
            if (session.getEnabledFilter(ACTIVE_FILTER) == null) {
                session.enableFilter(ACTIVE_FILTER).setParameter("active", true);
            }
        } else {
            log.debug("Fitro activeFilter desactivado");
            session.disableFilter(ACTIVE_FILTER);
        }
    }
}
