/**
 * @author david
 */
package com.dgc.academy.model.dao;

import com.dgc.academy.model.entity.UserSubject;
import lombok.NonNull;

import java.util.Collection;

public interface UserSubjectDao extends CommonDao<Integer, UserSubject> {
    Collection<UserSubject> getTeacherSubjects(@NonNull Integer teacherId);

    Collection<UserSubject> subjectHasAlreadyTeacherAssigned(@NonNull Integer subjectId);

    Collection<UserSubject> studentAlreadyAssignedToSubject(@NonNull Integer subjectId, @NonNull Integer studentId);
}
