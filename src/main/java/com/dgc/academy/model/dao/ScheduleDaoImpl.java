/**
 * @author david
 */
package com.dgc.academy.model.dao;

import com.dgc.academy.model.entity.Schedule;
import lombok.extern.log4j.Log4j2;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Log4j2
@Repository
public class ScheduleDaoImpl extends CommonDaoImpl<Integer, Schedule> implements ScheduleDao {

    @Override
    public Collection<Schedule> getUserSchedule(Integer userId) {
        try {
            Session session = getOpenSession(true);

            Query query = session.createQuery("select sc from Schedule sc, UserSubject us where " +
                    "sc.subject = us.subject and us.user.id =:userId");

            query.setParameter("userId", userId);
            Collection<Schedule> result = query.list();

            log.debug("Horarios obtenidos correctamente para el usuario con Id " + userId);

            return result;
        } catch (Exception e) {
            log.error("Error obteniendo horarios para el usuario con Id " + userId + ", error " + e.getMessage());
            e.printStackTrace();

            return null;
        }
    }

    @Override
    public Collection<Schedule> getSchedulerBySubject(Integer subjectId) {
        try {
            Session session = getOpenSession(true);

            Query query = session.createQuery("select sc from Schedule sc where " +
                    "sc.subject.id = :subjectId");
            query.setParameter("subjectId", subjectId);

            Collection<Schedule> result = query.list();

            log.debug("Horarios obtenidos correctamente para la asignatura con Id " + subjectId);

            return result;
        } catch (Exception e) {
            log.error("Error obteniendo horarios para la asignatura con Id " + subjectId + ", error " + e.getMessage());
            e.printStackTrace();

            return null;
        }
    }

    @Autowired
    public void init(SessionFactory sessionFactory) {
        setSessionFactory(sessionFactory);
    }
}
