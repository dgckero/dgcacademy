/**
 * @author david
 */
package com.dgc.academy.model.dao;

import com.dgc.academy.model.entity.TutorialStatus;

public interface TutorialStatusDao extends CommonDao<Integer, TutorialStatus> {

}