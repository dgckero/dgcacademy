/**
 * @author david
 */
package com.dgc.academy.model.dao;

import lombok.NonNull;

import java.io.Serializable;
import java.util.List;

public interface CommonDao<K extends Serializable, E> {

    void save(@NonNull E entity);

    void update(@NonNull E entity);

    void delete(@NonNull E entity);

    E findById(@NonNull K id, boolean activeFilterEnabled);

    List<E> findAll(@NonNull Class<? extends Serializable> entityClass);

    List<E> findAllIncludingInactives(@NonNull Class<? extends Serializable> entityClass);
}
