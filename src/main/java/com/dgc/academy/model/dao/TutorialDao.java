/**
 * @author david
 */
package com.dgc.academy.model.dao;

import com.dgc.academy.model.entity.Tutorial;
import lombok.NonNull;

import java.util.Collection;

public interface TutorialDao extends CommonDao<Integer, Tutorial> {

    Collection<Tutorial> getTutorialsByUserId(@NonNull Integer id);

    Collection<Tutorial> getTutorialsByTeacherId(@NonNull Integer id);

    Collection<Tutorial> getUnReadTutorialsByTeacherId(@NonNull Integer id);

    Collection<Tutorial> getUnReadTutorialsByUserId(@NonNull Integer id);

}
