/**
 * @author david
 */
package com.dgc.academy.model.dao;

import com.dgc.academy.model.entity.WeekDay;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class WeekDayDaoImpl extends CommonDaoImpl<Integer, WeekDay> implements WeekDayDao {
    @Autowired
    public void init(SessionFactory sessionFactory) {
        setSessionFactory(sessionFactory);
    }

}
