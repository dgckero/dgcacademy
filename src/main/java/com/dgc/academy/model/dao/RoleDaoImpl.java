/**
 * @author david
 */
package com.dgc.academy.model.dao;

import com.dgc.academy.model.entity.Role;
import lombok.extern.log4j.Log4j2;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Log4j2
@Repository
public class RoleDaoImpl extends CommonDaoImpl<Integer, Role> implements RoleDao {

    @Override
    public Role getRoleByDescription(String roleDescription) {
        log.debug("Obteniendo rol para la descripción " + roleDescription);
        try {
            Session session = getOpenSession(true);

            Query query = session.createQuery("from Role r where r.description = :description");
            query.setParameter("description", roleDescription);
            Role result = (Role) query.uniqueResult();

            log.debug("Rol obtenido correctamente para la descripción " + roleDescription);

            return result;
        } catch (Exception e) {
            log.error("Error obteniendo rol para descripción " + roleDescription + ", error " + e.getMessage());
            e.printStackTrace();

            return null;
        }
    }

    @Autowired
    public void init(SessionFactory sessionFactory) {
        setSessionFactory(sessionFactory);
    }
}