/**
 * @author david
 */
package com.dgc.academy.model.dao;

import com.dgc.academy.model.entity.WeekDay;

public interface WeekDayDao extends CommonDao<Integer, WeekDay> {
}
