/**
 * @author david
 */
package com.dgc.academy.model.dao;

import com.dgc.academy.model.entity.Level;

public interface LevelDao extends CommonDao<Integer, Level> {

}