/**
 * @author david
 */
package com.dgc.academy.model.dao;

import com.dgc.academy.model.entity.Role;
import lombok.NonNull;

public interface RoleDao extends CommonDao<Integer, Role> {

    Role getRoleByDescription(@NonNull String roleDescription);

}
