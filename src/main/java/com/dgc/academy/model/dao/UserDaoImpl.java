/**
 * @author david
 */
package com.dgc.academy.model.dao;

import com.dgc.academy.model.entity.Role;
import com.dgc.academy.model.entity.User;
import lombok.extern.log4j.Log4j2;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Log4j2
@Repository
public class UserDaoImpl extends CommonDaoImpl<Integer, User> implements UserDao {

    @Override
    public User login(String login, String password) {

        try {
            Session session = getOpenSession(true);

            Query query = session.createQuery("from User u where u.login = :login and u.password = :password");
            query.setParameter("login", login);
            query.setParameter("password", password);
            User result = (User) query.uniqueResult();

            log.debug("Login ejecutado correctamente para " + login);

            return result;
        } catch (Exception e) {
            log.error("Error en login de usuario de " + login + ", error " + e.getMessage());
            e.printStackTrace();

            return null;
        }
    }

    @Override
    public List<User> getActiveUsersByRole(Role role) {
        try {
            Session session = getOpenSession(true);

            Query query = session.createQuery("from User u where u.role = :role");
            query.setParameter("role", role);
            List<User> result = query.list();

            log.debug("Obtenidos " + ((result == null) ? " 0 " : result.size() + " usuarios por rol " + role));

            return result;
        } catch (Exception e) {
            log.error("Error en obtener usuarios por rol " + role + ", error " + e.getMessage());
            e.printStackTrace();

            return null;
        }
    }

    @Override
    public User getUserByLogin(String login) {
        try {
            Session session = getOpenSession(true);

            Query query = session.createQuery("from User u where u.login = :login");
            query.setParameter("login", login);
            User result = (User) query.uniqueResult();

            log.debug("Obtenido usuario " + result);

            return result;
        } catch (Exception e) {
            log.error("Error en obtener usuario por login " + login + ", error " + e.getMessage());
            e.printStackTrace();

            return null;
        }
    }

    @Autowired
    public void init(SessionFactory sessionFactory) {
        setSessionFactory(sessionFactory);
    }

}
