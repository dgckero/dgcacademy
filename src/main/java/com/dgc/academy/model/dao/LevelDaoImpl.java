/**
 * @author david
 */
package com.dgc.academy.model.dao;


import com.dgc.academy.model.entity.Level;
import lombok.extern.log4j.Log4j2;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Log4j2
@Repository
public class LevelDaoImpl extends CommonDaoImpl<Integer, Level> implements LevelDao {
    @Autowired
    public void init(SessionFactory sessionFactory) {
        setSessionFactory(sessionFactory);
    }
}