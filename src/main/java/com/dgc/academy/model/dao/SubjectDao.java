/**
 * @author david
 */
package com.dgc.academy.model.dao;

import com.dgc.academy.model.entity.Subject;

public interface SubjectDao extends CommonDao<Integer, Subject> {

}
