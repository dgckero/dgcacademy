/**
 * @author david
 */
package com.dgc.academy.model.dao;

import com.dgc.academy.model.entity.Schedule;
import lombok.NonNull;

import java.util.Collection;

public interface ScheduleDao extends CommonDao<Integer, Schedule> {
    Collection<Schedule> getUserSchedule(@NonNull Integer userId);

    Collection<Schedule> getSchedulerBySubject(@NonNull Integer subjectId);
}
