/**
 * @author david
 */
package com.dgc.academy.model.dao;

import com.dgc.academy.model.entity.Tutorial;
import lombok.extern.log4j.Log4j2;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Collection;

import static com.dgc.academy.util.GlobalConstant.PENDING_TUTORIAL_STATUS;

@Log4j2
@Repository
public class TutorialDaoImpl extends CommonDaoImpl<Integer, Tutorial> implements TutorialDao {

    @Autowired
    public void init(SessionFactory sessionFactory) {
        setSessionFactory(sessionFactory);
    }

    @Override
    public Collection<Tutorial> getTutorialsByUserId(Integer userId) {
        try {
            Session session = getOpenSession(true);

            Query query = session.createQuery("from Tutorial t where t.student.id = :userId");
            query.setParameter("userId", userId);
            Collection<Tutorial> result = query.list();

            log.debug("Tutorías obtenidas correctamente para el usuario con Id" + userId);

            return result;
        } catch (Exception e) {
            log.error("Error obteniendo tutorías para el usuario con Id " + userId + ", error " + e.getMessage());
            e.printStackTrace();

            return null;
        }
    }

    @Override
    public Collection<Tutorial> getTutorialsByTeacherId(Integer teacherId) {
        try {
            Session session = getOpenSession(true);

            Query query = session.createQuery("from Tutorial t where t.teacher.id = :teacherId");
            query.setParameter("teacherId", teacherId);
            Collection<Tutorial> result = query.list();

            log.debug("Tutorías obtenidas correctamente para el profesor con Id" + teacherId);

            return result;
        } catch (Exception e) {
            log.error("Error obteniendo tutorías para el profesor con Id " + teacherId + ", error " + e.getMessage());
            e.printStackTrace();

            return null;
        }
    }

    @Override
    public Collection<Tutorial> getUnReadTutorialsByTeacherId(Integer teacherId) {
        try {
            Session session = getOpenSession(true);

            Query query = session.createQuery("from Tutorial t where t.teacher.id = :teacherId and t.tutorialStatus.name = :tutorialStatus");
            query.setParameter("teacherId", teacherId);
            query.setParameter("tutorialStatus", PENDING_TUTORIAL_STATUS);
            Collection<Tutorial> result = query.list();

            log.debug("Tutorías No leídas obtenidas correctamente para el profesor con Id" + teacherId);

            return result;
        } catch (Exception e) {
            log.error("Error obteniendo tutorías para el profesor con Id " + teacherId + ", error " + e.getMessage());
            e.printStackTrace();

            return null;
        }
    }

    @Override
    public Collection<Tutorial> getUnReadTutorialsByUserId(Integer userId) {
        try {
            Session session = getOpenSession(true);

            Query query = session.createQuery("from Tutorial t where t.student.id = :userId and t.notified = :unread and t.tutorialStatus.name != :tutorialStatus");
            query.setParameter("userId", userId);
            query.setParameter("unread", false);
            query.setParameter("tutorialStatus", PENDING_TUTORIAL_STATUS);
            Collection<Tutorial> result = query.list();

            log.debug("Tutorías No leídas obtenidas correctamente para el usuario con Id" + userId);

            return result;
        } catch (Exception e) {
            log.error("Error obteniendo tutorías No leídas para el usuario con Id " + userId + ", error " + e.getMessage());
            e.printStackTrace();

            return null;
        }
    }
}
