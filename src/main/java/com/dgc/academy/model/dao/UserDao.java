/**
 * @author david
 */
package com.dgc.academy.model.dao;

import com.dgc.academy.model.entity.Role;
import com.dgc.academy.model.entity.User;
import lombok.NonNull;

import java.util.List;

public interface UserDao extends CommonDao<Integer, User> {

    User login(@NonNull String login, @NonNull String password);

    List<User> getActiveUsersByRole(@NonNull Role role);

    User getUserByLogin(@NonNull String login);
}
