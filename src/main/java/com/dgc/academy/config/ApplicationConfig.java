/**
 * @author david
 */
package com.dgc.academy.config;

import lombok.extern.log4j.Log4j2;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Log4j2
@Configuration
public class ApplicationConfig {

    @Bean
    public ModelMapper modelMapper() {
        log.debug("Configurando modelMapper");

        ModelMapper modelMapper = new ModelMapper();
        modelMapper.getConfiguration()
                .setMatchingStrategy(MatchingStrategies.STRICT);
        log.debug("modelMapper Configurado correctamente");

        return modelMapper;
    }

}