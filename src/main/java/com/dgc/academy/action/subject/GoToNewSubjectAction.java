/**
 * @author david
 */
package com.dgc.academy.action.subject;

import com.dgc.academy.action.common.BasicAction;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;

@Log4j2
@Getter
@Setter
@NoArgsConstructor
@Component("goToNewSubjectAction")
public class GoToNewSubjectAction extends BasicAction {

    @Override
    public String execute() throws Exception {
        super.execute();

        initActiveLevels(true);
        initActiveWeekDays(false);
        return SUCCESS;
    }
}
