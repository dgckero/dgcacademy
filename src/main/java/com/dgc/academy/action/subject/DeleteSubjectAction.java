/**
 * @author david
 */
package com.dgc.academy.action.subject;

import com.dgc.academy.action.common.BasicAction;
import com.dgc.academy.model.dto.SubjectDto;
import com.opensymphony.xwork2.ModelDriven;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.stereotype.Component;

import javax.persistence.PersistenceException;

@Log4j2
@Getter
@Setter
@NoArgsConstructor
@Component("deleteSubjectAction")
public class DeleteSubjectAction extends BasicAction implements ModelDriven<SubjectDto> {

    private SubjectDto selectedSubject = new SubjectDto();

    @Override
    public String execute() {

        try {
            super.execute();

            log.info("Borrando asignatura " + selectedSubject);
            getSubjectService().deleteSubject(selectedSubject);
            log.info("Asignatura " + selectedSubject + " borrado correctamente");

            log.info("Obteniendo listado de asignaturas");
            initSubjects(true);
            selectedSubject = new SubjectDto();
            return SUCCESS;
        } catch (PersistenceException e) {
            if (e.getCause() != null && e.getCause().getClass().equals(ConstraintViolationException.class)) {
                log.warn("La asignatura " + selectedSubject.getName() + " Tiene relacciones con información creada en el sistema, para evitar su pérdida la asignatura será marcado como inactivo");
                setMessage("La asignatura " + selectedSubject.getName() + " Tiene relacciones con información creada en el sistema, para evitar su pérdida la asignatura será marcado como inactivo");
                try {
                    log.info("Marcando asignatura " + selectedSubject.getName() + " como inactiva");
                    selectedSubject.setActive(false);
                    getSubjectService().updateSubject(selectedSubject);
                    log.info("Marcanda asignatura " + selectedSubject.getName() + " como inactiva correctamente");
                    selectedSubject = new SubjectDto();
                } catch (Exception ex) {
                    log.error("Error marcando asignatura " + selectedSubject.getName() + " como inactiva");
                }
                return SUCCESS;
            } else {
                log.error("Error borrando Asignatura con nombre " + selectedSubject.getName());
                setMessageError("Error borrando Asignatura con nombre " + selectedSubject.getName());
                return ERROR;
            }
        } catch (Exception e) {
            log.error("Error borrando Asignatura con nombre " + selectedSubject.getName());
            setMessageError("Error borrando Asignatura con nombre " + selectedSubject.getName());
            return ERROR;
        }
    }

    @Override
    public SubjectDto getModel() {
        if (selectedSubject != null && selectedSubject.getId() != null) {
            selectedSubject = getSubjectService().getSubjectById(selectedSubject.getId(), false);
        }
        return selectedSubject;
    }
}
