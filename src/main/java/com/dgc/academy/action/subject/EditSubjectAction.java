/**
 * @author david
 */
package com.dgc.academy.action.subject;

import com.dgc.academy.action.common.BasicAction;
import com.dgc.academy.model.dto.SubjectDto;
import com.opensymphony.xwork2.ModelDriven;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Component;

@Log4j2
@Getter
@Setter
@NoArgsConstructor
@Component("editSubjectAction")
public class EditSubjectAction extends BasicAction implements ModelDriven<SubjectDto> {

    private SubjectDto selectedSubject;
    private Integer selectedLevelId;

    @Override
    public String execute() {

        try {
            super.execute();

            getSubjectService().updateSubject(selectedSubject);
            selectedSubject = new SubjectDto();
            return SUCCESS;
        } catch (DataIntegrityViolationException e) {
            log.error("Ya existe una asignatura activa con el nombre " + selectedSubject.getName() + " para el curso " + selectedSubject.getLevel().getName());
            addActionError("Ya existe una asignatura activa con el nombre " + selectedSubject.getName() + " para el curso " + selectedSubject.getLevel().getName());
            return INPUT;
        } catch (Exception e) {
            log.error("Error modificando Asignatura " + selectedSubject.toString());
            addActionError(e.getMessage());
            return INPUT;
        }
    }

    @Override
    public SubjectDto getModel() {
        initActiveLevels(false);

        if (selectedSubject != null && selectedLevelId != null) {
            selectedSubject.setLevel(getLevelById(selectedLevelId, true));
        }

        return selectedSubject;
    }

    @Override
    public void validate() {
        clearErrorsAndMessages();
        if (selectedSubject != null && selectedSubject.getName() == null) {
            log.warn("El nombre de la asignatura no puede ser vacío");
            addFieldError("selectedSubject.name", "El nombre de la asignatura no puede ser vacío");
            addActionError("El nombre de la asignatura no puede ser vacío");
        }
        if (selectedSubject != null && selectedLevelId == null) {
            log.warn("El curso no puede ser vacío");
            addFieldError("selectedLevelId", "Debe seleccionar un curso");
            addActionError("Debe seleccionar un curso");
        }
    }
}
