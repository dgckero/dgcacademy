/**
 * @author david
 */
package com.dgc.academy.action.subject;

import com.dgc.academy.action.common.BasicAction;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;

@Log4j2
@Getter
@Setter
@NoArgsConstructor
@Component("goToAddUsersToSubjectAction")
public class GoToAddUsersToSubjectAction extends BasicAction {

    @Override
    public String execute() {
        log.info("Mostrando pantalla de asignación de asignaturas a usuarios");

        try {
            super.execute();

            initTeachers(true);
            initStudents(true);
            initSubjects(true);

            return SUCCESS;

        } catch (Exception e) {
            log.error("ERROR " + e.getMessage());
            setMessageError("ERROR " + e.getMessage());
            e.printStackTrace();
        }
        return ERROR;
    }

}
