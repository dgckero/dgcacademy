/**
 * @author david
 */
package com.dgc.academy.action.subject;

import com.dgc.academy.action.common.BasicAction;
import com.dgc.academy.model.dto.UserDto;
import com.dgc.academy.model.dto.UserSubjectDto;
import com.dgc.academy.util.GlobalConstant;
import com.opensymphony.xwork2.ModelDriven;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;

@Log4j2
@Getter
@Setter
@NoArgsConstructor
@Component("assignUserToSubjectAction")
public class AssignUserToSubjectAction extends BasicAction implements ModelDriven<UserSubjectDto> {

    private Integer selectedSubjectId;
    private Integer selectedTeacherId;
    private Integer selectedStudentId;
    private UserSubjectDto newUserSubject = new UserSubjectDto();

    @Override
    public String execute() {

        try {
            super.execute();
            log.info("Asignando el usuario " + newUserSubject.getUser() + " a la asignatura " + newUserSubject.getSubject());

            getSubjectService().assignUserToSubject(newUserSubject);
            cleanAction();
            return SUCCESS;
        } catch (Exception e) {
            log.error("Error Asignando el usuario " + newUserSubject.getUser() + " a la asignatura " + newUserSubject.getSubject() + " Error: " + e.getMessage());
            setMessageError("Error Asignando el usuario " + newUserSubject.getUser() + " a la asignatura " + newUserSubject.getSubject() + " Error: " + e.getMessage());
            return ERROR;
        }
    }

    private void cleanAction() {
        newUserSubject = new UserSubjectDto();
        selectedTeacherId = null;
        selectedStudentId = null;
        selectedSubjectId = null;
    }

    @Override
    public void validate() {
        clearErrorsAndMessages();
        if (selectedSubjectId == null || Integer.signum(selectedSubjectId) < 1) {
            log.warn("No se ha seleccionado una asignatura");
            addFieldError("selectedSubjectId", "Debe seleccionar una asignatura");
            addActionError("Debe seleccionar una asignatura");
        } else if ((selectedTeacherId == null || Integer.signum(selectedTeacherId) < 1) && (selectedStudentId == null || Integer.signum(selectedStudentId) < 1)) {
            log.warn("No se ha seleccionado estudiante o profesor");
            addActionError("Debe seleccionar un estudiante o un profesor");
        } else if (newUserSubject.getUser() != null && newUserSubject.getUser().getRole().getDescription().equals(GlobalConstant.TEACHER_ROLE)) {
            log.debug("Comprobar si ya existe un profesor asignado a la asignatura " + newUserSubject.getSubject().getName());
            try {
                if (getSubjectService().subjectHasAlreadyTeacherAssigned(newUserSubject.getSubject())) {
                    addFieldError("selectedTeacherId", "Ya se ha asignado un profesor a esa asignatura");
                    cleanAction();
                }
            } catch (Exception e) {
                log.error("Comprobando si ya existe un profesor asignado a la asignatura " + newUserSubject.getSubject().getName());
                setMessageError("Error Asignando el usuario " + newUserSubject.getUser() + " a la asignatura " + newUserSubject.getSubject() + " Error: " + e.getMessage());
            }
        } else if (newUserSubject.getUser() != null && newUserSubject.getUser().getRole().getDescription().equals(GlobalConstant.STUDENT_ROLE)) {
            log.debug("Comprobar si el estudiante " + newUserSubject.getUser().getRole() + " ya ha sido asignado a la asignatura " + newUserSubject.getSubject().getName());
            try {
                if (getSubjectService().studentAlreadyAssignedToSubject(newUserSubject.getSubject(), newUserSubject.getUser())) {
                    addFieldError("selectedStudentId", "El alumno " + newUserSubject.getUser().getLogin() + " ya ha sido previamente asignado a esa asignatura");
                    cleanAction();
                }
            } catch (Exception e) {
                log.error("Comprobando si el estudiante " + newUserSubject.getUser().getRole() + " ya ha sido asignado a la asignatura " + newUserSubject.getSubject().getName());
                setMessageError("Error si el estudiante " + newUserSubject.getUser().getRole() + " ya ha sido asignado a la asignatura " + newUserSubject.getSubject().getName());
            }
        }

    }


    @Override
    public UserSubjectDto getModel() {
        initSubjects(false);
        initStudents(false);
        initTeachers(false);

        if (selectedSubjectId != null && Integer.signum(selectedSubjectId) >= 0) {
            UserDto selectedUser = null;

            if (selectedStudentId != null && Integer.signum(selectedStudentId) >= 0) {
                selectedUser = getStudentById(selectedStudentId, false);
            } else if (selectedTeacherId != null && Integer.signum(selectedTeacherId) >= 0) {
                selectedUser = getTeacherById(selectedTeacherId, false);
            }
            newUserSubject = UserSubjectDto.builder().
                    subject(getSubjectById(selectedSubjectId, true)).
                    user(selectedUser).
                    active(true).
                    build();
        }

        return newUserSubject;
    }

}
