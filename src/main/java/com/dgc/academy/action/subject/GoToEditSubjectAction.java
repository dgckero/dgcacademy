/**
 * @author david
 */
package com.dgc.academy.action.subject;

import com.dgc.academy.action.common.BasicAction;
import com.dgc.academy.model.dto.SubjectDto;
import com.opensymphony.xwork2.ModelDriven;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;

@Log4j2
@Getter
@Setter
@NoArgsConstructor
@Component("goToEditSubjectAction")
public class GoToEditSubjectAction extends BasicAction implements ModelDriven<SubjectDto> {
    private SubjectDto selectedSubject;

    @Override
    public String execute() {

        try {
            super.execute();

            initActiveLevels(true);
            log.info("Ir a la pantalla de editar con asignatura " + selectedSubject.toString());

            return SUCCESS;
        } catch (Exception e) {
            log.error("Error al acceder a la pantalla de edición con asignatura " + selectedSubject.getName());
            setMessageError("Error al acceder a la pantalla de edición con asignatura " + selectedSubject.getName());
            return ERROR;
        }
    }

    @Override
    public SubjectDto getModel() {
        if (selectedSubject != null && selectedSubject.getId() != null) {
            selectedSubject = getSubjectService().getSubjectById(selectedSubject.getId(), false);
        }
        return selectedSubject;
    }
}
