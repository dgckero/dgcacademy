/**
 * @author david
 */
package com.dgc.academy.action.subject;

import com.dgc.academy.action.common.BasicAction;
import com.dgc.academy.model.dto.LevelDto;
import com.dgc.academy.model.dto.ScheduleDto;
import com.opensymphony.xwork2.ModelDriven;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

@Log4j2
@Getter
@Setter
@NoArgsConstructor
@Component("newSubjectAction")
public class NewSubjectAction extends BasicAction implements ModelDriven<ScheduleDto> {

    private ScheduleDto newSchedule = new ScheduleDto();
    private LevelDto selectedLevel;
    private Integer selectedLevelId;
    private Byte weekDayId;

    @Override
    public String execute() {

        try {
            super.execute();

            log.info("Creando horario " + newSchedule);
            getScheduleService().createScheduler(newSchedule);

            newSchedule = new ScheduleDto();
            return SUCCESS;
        } catch (ConstraintViolationException e) {
            log.error("Ya existe una asignatura activa con el nombre " + newSchedule.getSubject().getName() + " para el curso " + newSchedule.getSubject().getLevel().getName());
            addActionError("Ya existe una asignatura activa con el nombre " + newSchedule.getSubject().getName() + " para el curso " + newSchedule.getSubject().getLevel().getName());
            return INPUT;
        } catch (Exception e) {
            log.error("Error creando Asignatura con nombre " + newSchedule.getSubject().getName());
            return INPUT;
        }
    }


    @Override
    public void validate() {
        clearErrorsAndMessages();
        if (newSchedule != null && StringUtils.isEmpty(newSchedule.getSubject().getName())) {
            log.warn("Nombre vacío");
            addActionError("nombre no puede ser vacío");
        }
        if (selectedLevelId == null || Integer.signum(selectedLevelId) < 1) {
            log.warn("selectedLevel vacío");
            addActionError("Curso no puede ser vacío");
        }
        if (weekDayId == null || Integer.signum(weekDayId) < 1) {
            log.warn("Día de la semana vacío");
            addActionError("Día de la semana no puede ser vacío");
        }
        if (StringUtils.isEmpty(newSchedule.getStartTime()) || StringUtils.isEmpty(newSchedule.getEndTime())) {
            log.warn("Hora a la que se imparte vacía");
            addActionError("Hora a la que se imparte no puede ser vacía");
        }
    }

    @Override
    public ScheduleDto getModel() {

        initActiveLevels(false);
        if (selectedLevelId != null && weekDayId != null && newSchedule.getStartTime() != null) {
            selectedLevel = getLevelById(selectedLevelId, false);

            newSchedule.getSubject().setLevel(selectedLevel);
            newSchedule.getSubject().setActive(true);
            newSchedule.setDay(getWeekDayById(weekDayId, false));
            newSchedule.setActive(true);
        }

        return newSchedule;
    }
}
