/**
 * @author david
 */
package com.dgc.academy.action.subject;

import com.dgc.academy.action.common.BasicAction;
import com.dgc.academy.model.dto.UserSubjectDto;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;

import java.util.List;

@Log4j2
@Getter
@Setter
@NoArgsConstructor
@Component("showSubjectsAction")
public class ShowSubjectsAction extends BasicAction {

    private List<UserSubjectDto> userSubjectList;

    @Override
    public String execute() {
        try {
            super.execute();
            log.info("Obteniendo todas las asignaturas para el profesor " + getUser());

            userSubjectList = getSubjectService().getTeacherSubjects(getUser());
        } catch (Exception e) {
            log.error("Error obteniendo las asignaturas activas " + e.getMessage());
            setMessageError("Error obteniendo las asignaturas activas " + e.getMessage());
            e.printStackTrace();
            return ERROR;
        }

        return SUCCESS;
    }


}
