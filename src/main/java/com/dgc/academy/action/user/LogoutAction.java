/**
 * @author david
 */

package com.dgc.academy.action.user;

import com.dgc.academy.action.common.BasicAction;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;

@Log4j2
@Component("logoutAction")
public class LogoutAction extends BasicAction {

    @Override
    public String execute() {
        log.info("Invalidando usuario");
        setUser(null);
        return SUCCESS;
    }
}
