/**
 * @author david
 */
package com.dgc.academy.action.user;

import com.dgc.academy.action.common.BasicAction;
import com.dgc.academy.model.dto.UserDto;
import com.dgc.academy.util.GlobalConstant;
import com.dgc.academy.util.NoLogginRequired;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;
import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

@Log4j2
@Getter
@Setter
@NoLogginRequired
@NoArgsConstructor
@Component("loginAction")
public class LoginAction extends BasicAction {

    private String userName;
    private String password;

    @Override
    public String execute() throws Exception {
        log.info("Login user: " + userName);

        UserDto user = getUserService().login(userName, password);
        if (user == null) {
            ServletActionContext.getRequest().getSession().setAttribute(GlobalConstant.USER_HANDLE, null);
            ServletActionContext.getRequest().getSession().invalidate();
            super.setUser(null);
            log.warn("Login incorrecto con userName " + userName);
            addFieldError("userName", "Usuario/Contraseña Erróneos");
            addFieldError("password", "Usuario/Contraseña Erróneos");
            addActionError("Usuario/Contraseña Erróneos");
            return INPUT;
        } else {
            super.setUser(user);
            ServletActionContext.getRequest().getSession().setAttribute(GlobalConstant.USER_HANDLE, super.getUser());
            log.info("Login correcto con usuario " + super.getUser().toString());

            return SUCCESS;
        }
    }

    @Override
    public void validate() {
        clearErrorsAndMessages();
        if (StringUtils.isEmpty(userName)) {
            log.warn("userName vacío");
            addFieldError("userName", "login no puede ser vacío");
            addActionError("Login no puede ser vacío");
        }
        if (StringUtils.isEmpty(password)) {
            log.warn("userName/password vacío");
            addFieldError("password", "password no puede ser vacío");
            addActionError("Contraseña no puede ser vacío");
        }
    }

}
