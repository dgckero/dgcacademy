/**
 * @author david
 */
package com.dgc.academy.action.user;

import com.dgc.academy.action.common.BasicAction;
import com.dgc.academy.model.dto.UserDto;
import com.opensymphony.xwork2.ModelDriven;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

@Log4j2
@Getter
@Setter
@NoArgsConstructor
@Component("editUserAction")
public class EditUserAction extends BasicAction implements ModelDriven<UserDto> {

    private UserDto selectedUser = new UserDto();
    private Integer selectedRoleId;
    private String password1;
    private String password2;
    private boolean modifyPasswordActive;

    @Override
    public String execute() {

        try {
            super.execute();

            getUserService().updateUser(selectedUser, modifyPasswordActive);
            selectedUser = new UserDto();
            return SUCCESS;
        } catch (ConstraintViolationException e) {
            log.error("Ya existe un usuario con login: " + selectedUser.getLogin() + " activo, por favor cambie el login");
            addFieldError("newUser.login", "Ya existe un usuario con login: " + selectedUser.getLogin() + " activo, por favor cambie el login");
            addActionError("Ya existe un usuario con login: " + selectedUser.getLogin() + ", por favor cambie el login");
            selectedUser = new UserDto();
            return INPUT;
        } catch (Exception e) {
            log.error("Error modificando Usuario " + selectedUser.toString());
            return INPUT;
        }
    }

    @Override
    public void validate() {
        clearErrorsAndMessages();
        if (StringUtils.isEmpty(selectedUser.getLogin())) {
            log.warn("login vacío");
            addFieldError("login", "login no puede ser vacío");
            addActionError("login no puede ser vacío");
        }
        if (StringUtils.isEmpty(selectedUser.getName())) {
            log.warn("name vacío");
            addFieldError("name", "name no puede ser vacío");
            addActionError("Nombre no puede ser vacío");
        }
        if (StringUtils.isEmpty(selectedUser.getSurname())) {
            log.warn("surname vacío");
            addFieldError("surname", "surname no puede ser vacío");
            addActionError("Apellido no puede ser vacío");
        }
        if (modifyPasswordActive) {
            if (StringUtils.isEmpty(password1)) {
                log.warn("password vacío");
                addFieldError("password1", "password no puede ser vacío");
                addActionError("Contraseña no puede ser vacío");
            }
            if (StringUtils.isEmpty(password2)) {
                log.warn("password2 vacío");
                addFieldError("password2", "password2 no puede ser vacío");
                addActionError("Confirmar contraseña no puede ser vacío");
            }

            if (!password1.equals(password2)) {
                log.warn("Las contraseñas no coinciden");
                addFieldError("password2", "Las contraseñas no coinciden");
                addActionError("Las contraseñas no coinciden");
            }
        }

    }

    @Override
    public UserDto getModel() {

        if (selectedUser != null && selectedUser.getName() != null && selectedRoleId != null) {
            UserDto userOriginal = getUserService().getUserById(selectedUser.getId(), true);
            selectedUser.setLastLogin(userOriginal.getLastLogin());

            if (modifyPasswordActive) {
                selectedUser.setPassword(password1);
            } else {
                selectedUser.setPassword(userOriginal.getPassword());
            }
            selectedUser.setRole(getRoleById(selectedRoleId, false));
        }

        return selectedUser;
    }
}
