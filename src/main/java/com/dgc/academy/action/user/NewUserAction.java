/**
 * @author david
 */
package com.dgc.academy.action.user;

import com.dgc.academy.action.common.BasicAction;
import com.dgc.academy.model.dto.UserDto;
import com.dgc.academy.util.NoLogginRequired;
import com.opensymphony.xwork2.ModelDriven;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import static com.dgc.academy.util.GlobalConstant.STUDENT_ROLE;

@Log4j2
@Getter
@Setter
@NoArgsConstructor
@NoLogginRequired
@Component("newUserAction")
public class NewUserAction extends BasicAction implements ModelDriven<UserDto> {
    private UserDto newUser = new UserDto();
    private String password2;
    private Integer selectedRoleId;

    @Override
    public String execute() {

        try {
            super.execute();

            log.info("Creando usuario " + newUser);
            getUserService().createUser(newUser);

            if (isAdmin()) {
                setMessage("Usuario con login " + newUser.getLogin() + " creado correctamente, no olvide activarlo");
            } else {
                setMessage("Usuario con login " + newUser.getLogin() + " creado correctamente, Contacte con el administrador para que el usuario sea activado");
            }
            newUser = new UserDto();
            return SUCCESS;
        } catch (ConstraintViolationException e) {
            log.error("Ya existe un usuario con login: " + newUser.getLogin() + ", por favor cambie el login");
            addFieldError("login", "Ya existe un usuario con login: " + newUser.getLogin() + ", por favor cambie el login");
            addActionError("Ya existe un usuario con login: " + newUser.getLogin() + ", por favor cambie el login");
            return INPUT;
        } catch (Exception e) {
            log.error("Error creando Usuario con login " + newUser.getLogin() + ", error: " + e.getMessage());
            e.printStackTrace();
            setMessageError("Error creando Usuario con login " + newUser.getLogin());
            return ERROR;
        }
    }

    @Override
    public void validate() {
        clearErrorsAndMessages();
        initUser(false);
        if (StringUtils.isEmpty(newUser.getLogin())) {
            log.warn("login vacío");
            addActionError("login no puede ser vacío");
        }
        if (StringUtils.isEmpty(newUser.getName())) {
            log.warn("name vacío");
            addActionError("nombre no puede ser vacío");
        }
        if (StringUtils.isEmpty(newUser.getSurname())) {
            log.warn("surname vacío");
            addActionError("surname no puede ser vacío");
        }
        if (StringUtils.isEmpty(newUser.getPassword())) {
            log.warn("password vacío");
            addActionError("password no puede ser vacío");
        }
        if (StringUtils.isEmpty(password2)) {
            log.warn("password2 vacío");
            addActionError("Confirmar contraseña no puede ser vacío");
        }

        if (!newUser.getPassword().equals(password2)) {
            newUser.setPassword(null);
            setPassword2(null);
            log.warn("Las contraseñas no coinciden");
            addActionError("Las contraseñas no coinciden");
        }
        if (isAdmin() && (selectedRoleId == null || Integer.signum(selectedRoleId) < 1)) {
            log.warn("Debe seleccionar un rol");
            addActionError("Debe seleccionar un rol");
        }
    }

    @Override
    public UserDto getModel() {
        if ((isAdmin() && selectedRoleId != null) || !isAdmin()) {
            initActiveRoles(false);
            if (isAdmin()) {
                newUser.setRole(getRoleById(selectedRoleId, false));
            } else {
                newUser.setRole(getRoleByDescription(STUDENT_ROLE, false));
            }
        }
        return newUser;
    }
}
