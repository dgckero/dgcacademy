/**
 * @author david
 */
package com.dgc.academy.action.user;

import com.dgc.academy.action.common.BasicAction;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;

@Log4j2
@Getter
@Setter
@NoArgsConstructor
@Component("goToNewUserAction")
public class GoToNewUserAction extends BasicAction {

    @Override
    public String execute() throws Exception {
        super.execute();

        log.info("Obteniendo roles");
        initActiveRoles(true);

        return SUCCESS;
    }

}
