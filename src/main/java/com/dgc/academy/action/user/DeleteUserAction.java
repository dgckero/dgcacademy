/**
 * @author david
 */
package com.dgc.academy.action.user;

import com.dgc.academy.action.common.BasicAction;
import com.dgc.academy.model.dto.UserDto;
import com.opensymphony.xwork2.ModelDriven;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.stereotype.Component;

import javax.persistence.PersistenceException;
import java.util.Collection;

@Log4j2
@Getter
@Setter
@NoArgsConstructor
@Component("deleteUserAction")
public class DeleteUserAction extends BasicAction implements ModelDriven<UserDto> {

    private UserDto selectedUser;
    private Collection<UserDto> users;

    @Override
    public String execute() {

        try {
            super.execute();

            log.info("Borrando usuario " + selectedUser);
            getUserService().deleteUser(selectedUser);
            log.info("Usuario " + selectedUser + " borrado correctamente");

            log.info("Obteniendo listado de usuarios");
            users = getUserService().getAllUsers();
            if (users == null || users.isEmpty()) {
                log.warn("No se han obtenido usuarios");
            } else {
                log.info("Obtenidos " + users.size() + " users");
            }

            setMessage("Usuario con login " + selectedUser.getLogin() + " borrado correctamente");
            selectedUser = new UserDto();
            return SUCCESS;
        } catch (PersistenceException e) {
            if (e.getCause() != null && e.getCause().getClass().equals(ConstraintViolationException.class)) {
                log.warn("El Usuario con login " + selectedUser.getLogin() + " Tiene relacciones con información creada en el sistema, para evitar su pérdida el usuario será marcado como inactivo");
                setMessage("El Usuario con login " + selectedUser.getLogin() + " Tiene relacciones con información creada en el sistema,\n para evitar su pérdida el usuario será marcado como inactivo");
                try {
                    log.info("Marcando Usuario con login " + selectedUser.getLogin() + " como inactivo");
                    selectedUser.setActive(false);
                    getUserService().updateUser(selectedUser, false);
                    log.info("Marcado Usuario con login " + selectedUser.getLogin() + " como inactivo correctamente");
                    selectedUser = new UserDto();
                } catch (Exception ex) {
                    log.error("Error marcando Usuario con login " + selectedUser.getLogin() + " como inactivo");
                }
                return SUCCESS;
            } else {
                log.error("Error borrando Usuario con login " + selectedUser.getLogin());
                setMessageError("Error borrando Usuario con login " + selectedUser.getLogin());
                return ERROR;
            }
        } catch (Exception e) {
            log.error("Error borrando Usuario con login " + selectedUser.getLogin());
            setMessageError("Error borrando Usuario con login " + selectedUser.getLogin());
            return ERROR;
        }
    }

    @Override
    public UserDto getModel() {
        if (selectedUser != null && selectedUser.getId() != null) {
            selectedUser = getUserService().getUserById(selectedUser.getId(), false);
        }
        return selectedUser;
    }
}
