/**
 * @author david
 */

package com.dgc.academy.action.user;

import com.dgc.academy.action.common.BasicAction;
import com.dgc.academy.model.dto.UserDto;
import com.opensymphony.xwork2.ModelDriven;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;

@Log4j2
@Getter
@Setter
@NoArgsConstructor
@Component("goToEditUserAction")
public class GoToEditUserAction extends BasicAction implements ModelDriven<UserDto> {
    private UserDto selectedUser = new UserDto();

    @Override
    public String execute() {

        try {
            super.execute();
            initActiveRoles(true);

            log.info("Ir a la pantalla de editar con usuario " + selectedUser.toString());

            return SUCCESS;
        } catch (Exception e) {
            log.error("Error al acceder a la pantalla de edición con usuario " + selectedUser.getLogin());
            setMessageError("Error al acceder a la pantalla de edición con usuario " + selectedUser.getLogin());
            return ERROR;
        }
    }

    @Override
    public UserDto getModel() {
        if (selectedUser != null && selectedUser.getId() != null) {
            selectedUser = getUserService().getUserById(selectedUser.getId(), false);
        }
        return selectedUser;
    }
}
