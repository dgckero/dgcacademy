/**
 * @author david
 */
package com.dgc.academy.action.user;

import com.dgc.academy.action.common.BasicAction;
import com.dgc.academy.model.dto.UserDto;
import com.opensymphony.xwork2.ModelDriven;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;

@Log4j2
@Getter
@Setter
@NoArgsConstructor
@Component("goToViewUserAction")
public class GoToViewUserAction extends BasicAction implements ModelDriven<UserDto> {

    private UserDto selectedUser;

    @Override
    public String execute() {

        try {
            super.execute();

            log.info("Ir a la pantalla de ver usuario " + selectedUser.toString());

            return SUCCESS;
        } catch (Exception e) {
            log.error("Error al acceder a la pantalla de vista de usuario " + selectedUser.getLogin());
            setMessageError("Error al acceder a la pantalla de vista de usuario " + selectedUser.getLogin());
            return ERROR;
        }
    }

    @Override
    public UserDto getModel() {
        if (selectedUser != null && selectedUser.getId() != null) {
            selectedUser = getUserService().getUserById(selectedUser.getId(), false);
        }
        return selectedUser;
    }
}
