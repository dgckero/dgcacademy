/**
 * @author david
 */
package com.dgc.academy.action.user;

import com.dgc.academy.action.common.BasicAction;
import com.dgc.academy.model.dto.UserDto;
import com.opensymphony.xwork2.ModelDriven;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;

@Log4j2
@Getter
@Setter
@NoArgsConstructor
@Component("goToEditProfileAction")
public class GoToEditProfileAction extends BasicAction implements ModelDriven<UserDto> {

    private UserDto userToBeUpdated = new UserDto();

    @Override
    public String execute() {
        String res = ERROR;
        try {
            super.execute();
            if (getUser() != null && getUser().getId() != null) {
                log.info("Redirigiendo a la página de editar perfil del usuario con login " + getUser().getLogin());

                userToBeUpdated = getUser();
                res = SUCCESS;
            }
        } catch (Exception e) {
            log.error("ERROR " + e.getMessage());
            e.printStackTrace();
        }
        return res;
    }

    @Override
    public UserDto getModel() {
        return userToBeUpdated;
    }
}
