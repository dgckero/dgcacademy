/**
 * @author david
 */
package com.dgc.academy.action.user;

import com.dgc.academy.action.common.BasicAction;
import com.dgc.academy.model.dto.UserDto;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;

import java.util.Collection;

@Log4j2
@Getter
@Setter
@NoArgsConstructor
@Component("goToListAllUsersAction")
public class GoToListAllUsersAction extends BasicAction {

    private Collection<UserDto> users;

    @Override
    public String execute() throws Exception {
        super.execute();

        log.info("Obteniendo listado de usuarios");

        users = getUserService().getAllUsers();

        if (users == null || users.isEmpty()) {
            log.warn("No se han obtenido usuarios");
        } else {
            log.info("Obtenidos " + users.size() + " users");
        }

        return SUCCESS;
    }

}
