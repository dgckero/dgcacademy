/**
 * @author david
 */
package com.dgc.academy.action.user;

import com.dgc.academy.action.common.BasicAction;
import com.dgc.academy.model.dto.UserDto;
import com.dgc.academy.util.GlobalConstant;
import com.opensymphony.xwork2.ModelDriven;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;
import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

@Log4j2
@Getter
@Setter
@NoArgsConstructor
@Component("editProfileAction")
public class EditProfileAction extends BasicAction implements ModelDriven<UserDto> {

    private UserDto userToBeUpdated = new UserDto();

    private String newPassword;
    private String newPasswordConfirmed;
    private boolean modifyPasswordActive;

    @Override
    public String execute() {

        try {
            log.info("Editando Usuario con login " + userToBeUpdated.getLogin() + ", modificando contraseña (" + modifyPasswordActive + ")");
            super.execute();

            getUserService().updateUser(userToBeUpdated, modifyPasswordActive);
            reloadUserSession(getUserService().getUserByLogin(userToBeUpdated.getLogin()));

            log.info("Usuario con login " + userToBeUpdated.getLogin() + " modificado correctamente");
            userToBeUpdated = new UserDto();
            return SUCCESS;
        } catch (Exception e) {
            log.error("Error " + e.getMessage() + " modificando Usuario con login " + userToBeUpdated.getLogin());
            addActionError("Error modificando el usuario");
            return INPUT;
        }
    }

    private void reloadUserSession(UserDto userByLogin) {
        log.info("Actualizando los datos del usuario " + userByLogin + " en sesión");
        super.setUser(userByLogin);
        ServletActionContext.getRequest().getSession().setAttribute(GlobalConstant.USER_HANDLE, userByLogin);
    }

    @Override
    public UserDto getModel() {

        if (userToBeUpdated != null && userToBeUpdated.getId() != null) {
            if (modifyPasswordActive) {
                userToBeUpdated.setPassword(newPassword);
            } else {
                userToBeUpdated.setRole(getUser().getRole());
                userToBeUpdated.setPassword(getUser().getPassword());
                userToBeUpdated.setLastLogin(getUser().getLastLogin());
            }
            userToBeUpdated.setActive(true);
        }

        return userToBeUpdated;
    }

    @Override
    public void validate() {
        clearErrorsAndMessages();
        if (StringUtils.isEmpty(userToBeUpdated.getLogin())) {
            log.warn("login vacío");
            addFieldError("login", "login no puede ser vacío");
            addActionError("login no puede ser vacío");
        }
        if (StringUtils.isEmpty(userToBeUpdated.getName())) {
            log.warn("name vacío");
            addFieldError("name", "nombre no puede ser vacío");
            addActionError("nombre no puede ser vacío");
        }
        if (StringUtils.isEmpty(userToBeUpdated.getSurname())) {
            log.warn("surname vacío");
            addFieldError("surname", "apellido no puede ser vacío");
            addActionError("apellido no puede ser vacío");
        }
        if (modifyPasswordActive) {
            if (StringUtils.isEmpty(newPassword)) {
                log.warn("newPassword vacío");
                addFieldError("newPassword", "nueva contraseña no puede ser vacío");
                addActionError("Nueva contraseña no puede ser vacío");
            }
            if (StringUtils.isEmpty(newPasswordConfirmed)) {
                log.warn("newPasswordConfirmed vacío");
                addFieldError("newPasswordConfirmed", "confirmar nueva contraseña no puede ser vacío");
                addActionError("El campo confirmar contraseña no puede ser vacío");
            }

            if (!newPassword.equals(newPasswordConfirmed)) {
                log.warn("Las contraseñas no coinciden");
                addFieldError("newPassword", "Las contraseñas no coinciden");
                addActionError("Las contraseñas no coinciden");
            }
        }
    }
}
