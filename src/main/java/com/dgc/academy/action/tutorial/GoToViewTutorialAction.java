/**
 * @author david
 */
package com.dgc.academy.action.tutorial;

import com.dgc.academy.action.common.BasicAction;
import com.dgc.academy.model.dto.TutorialDto;
import com.opensymphony.xwork2.ModelDriven;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;

@Log4j2
@Getter
@Setter
@NoArgsConstructor
@Component("goToViewTutorialAction")
public class GoToViewTutorialAction extends BasicAction implements ModelDriven<TutorialDto> {

    private TutorialDto selectedTutorial;

    @Override
    public String execute() {

        try {
            super.execute();

            log.info("Ir a la pantalla de ver tutoría " + selectedTutorial);

            return SUCCESS;
        } catch (Exception e) {
            log.error("Error al acceder a la pantalla de vista de tutoría con identificador " + selectedTutorial.getId());
            setMessageError("Error al acceder a la pantalla de vista de tutoría con identificador " + selectedTutorial.getId());
            return ERROR;
        }
    }

    @Override
    public TutorialDto getModel() {
        clearErrorsAndMessages();
        if (selectedTutorial != null && selectedTutorial.getId() != null) {
            selectedTutorial = getTutorialService().getTutorialById(selectedTutorial.getId());
        }
        return selectedTutorial;
    }
}
