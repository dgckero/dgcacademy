/**
 * @author david
 */
package com.dgc.academy.action.tutorial;

import com.dgc.academy.action.common.BasicAction;
import com.dgc.academy.model.dto.TutorialDto;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;

import java.util.Collection;


@Log4j2
@Getter
@Setter
@NoArgsConstructor
@Component("goToViewTutorialRequestedAction")
public class GoToViewTutorialRequestedAction extends BasicAction {

    private Collection<TutorialDto> tutorials;

    @Override
    public String execute() throws Exception {
        super.execute();

        log.info("Obteniendo listado de tutorías del usuario " + getUser().getLogin());
        tutorials = getTutorialService().getTutorialsByUser(getUser());

        if (tutorials == null || tutorials.isEmpty()) {
            log.warn("No se han obtenido tutorías para el usuario " + getUser().getLogin());
        } else {
            log.info("Obtenidas " + tutorials.size() + " tutorías para el usuario " + getUser().getLogin());
        }

        return SUCCESS;
    }


}
