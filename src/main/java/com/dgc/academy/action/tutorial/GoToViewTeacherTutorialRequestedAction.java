/**
 * @author david
 */
package com.dgc.academy.action.tutorial;

import com.dgc.academy.action.common.BasicAction;
import com.dgc.academy.model.dto.TutorialDto;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;

import java.util.Collection;


@Log4j2
@Getter
@Setter
@NoArgsConstructor
@Component("goToViewTeacherTutorialRequestedAction")
public class GoToViewTeacherTutorialRequestedAction extends BasicAction {

    private Collection<TutorialDto> tutorials;

    @Override
    public String execute() throws Exception {
        super.execute();

        log.info("Obteniendo listado de tutorías del profesor " + getUser().getLogin());

        tutorials = getTutorialService().getTutorialsByTeacher(getUser());

        if (tutorials == null || tutorials.isEmpty()) {
            log.warn("No se han obtenido tutorías para el profesor " + getUser().getLogin());
        } else {
            log.info("Obtenidas " + tutorials.size() + " tutorías para el profesor " + getUser().getLogin());
        }

        return SUCCESS;
    }


}
