/**
 * @author david
 */
package com.dgc.academy.action.tutorial;

import com.dgc.academy.action.common.BasicAction;
import com.dgc.academy.model.dto.TutorialDto;
import com.dgc.academy.util.GlobalConstant;
import com.opensymphony.xwork2.ModelDriven;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

@Log4j2
@Getter
@Setter
@NoArgsConstructor
@Component("newTutorialAction")
public class NewTutorialAction extends BasicAction implements ModelDriven<TutorialDto> {

    private TutorialDto newTutorial = new TutorialDto();
    private Integer selectedTeacherId;

    @Override
    public String execute() {

        try {
            super.execute();

            getTutorialService().createTutorial(newTutorial);

            newTutorial = new TutorialDto();
            return SUCCESS;
        } catch (Exception e) {
            log.error("Error creando Tutoría Error: " + e.toString());
            addActionError("Error creando Tutoría, por favor inténtelo de nuevo más tarde");
            return INPUT;
        }
    }


    @Override
    public void validate() {
        clearErrorsAndMessages();
        if (StringUtils.isEmpty(newTutorial.getStartTime()) || StringUtils.isEmpty(newTutorial.getEndTime())) {
            log.warn("Fecha de tutoría vacía");
            addFieldError("datetimes", "Debe seleccionar una fecha y hora de tutoría");
            addActionError("Debe seleccionar una fecha y hora de tutoría");
        }
        if (StringUtils.isEmpty(selectedTeacherId)) {
            log.warn("Profesor vacío");
            addFieldError("selectedTeacherId", "Debe seleccionar un profesor");
            addActionError("Debe seleccionar un profesor");
        }
    }

    @Override
    public TutorialDto getModel() {

        if (newTutorial.getStartTime() != null && newTutorial.getEndTime() != null && selectedTeacherId != null) {

            newTutorial.setTutorialStatus(getTutorialStatusByName(GlobalConstant.PENDING_TUTORIAL_STATUS, false));
            newTutorial.setStudent(getUser());
            newTutorial.setTeacher(getTeacherById(selectedTeacherId, false));
            newTutorial.setNotified(false);
        }

        return newTutorial;
    }
}
