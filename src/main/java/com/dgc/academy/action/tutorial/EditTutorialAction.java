/**
 * @author david
 */
package com.dgc.academy.action.tutorial;

import com.dgc.academy.action.common.BasicAction;
import com.dgc.academy.model.dto.TutorialDto;
import com.dgc.academy.model.dto.TutorialStatusDto;
import com.opensymphony.xwork2.ModelDriven;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;

@Log4j2
@Getter
@Setter
@NoArgsConstructor
@Component("editTutorialAction")
public class EditTutorialAction extends BasicAction implements ModelDriven<TutorialDto> {

    private TutorialDto selectedTutorial = new TutorialDto();
    private TutorialStatusDto selectedTutorialStatus;
    private Integer selectedTutorialStatusId;

    @Override
    public String execute() {
        log.info("Modificando tutoría " + selectedTutorial);
        try {
            super.execute();

            getTutorialService().updateTutorial(selectedTutorial);
            selectedTutorial = new TutorialDto();
            return SUCCESS;
        } catch (Exception e) {
            log.error("Error modificando tutoría " + selectedTutorial + ", error " + e.getMessage());
            setMessageError("Error modificando tutoría " + selectedTutorial + ", error " + e.getMessage());
            e.printStackTrace();
            return ERROR;
        }
    }

    @Override
    public TutorialDto getModel() {

        if (selectedTutorialStatusId != null && Integer.signum(selectedTutorialStatusId) >= 0) {
            selectedTutorial.setTutorialStatus(getTutorialStatusById(selectedTutorialStatusId, false));
            selectedTutorial.setNotified(false);
            selectedTutorial.setTeacher(getUser());
        }

        return selectedTutorial;
    }
}
