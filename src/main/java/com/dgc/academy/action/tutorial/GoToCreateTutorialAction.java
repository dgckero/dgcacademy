/**
 * @author david
 */
package com.dgc.academy.action.tutorial;

import com.dgc.academy.action.common.BasicAction;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;

@Log4j2
@Getter
@Setter
@NoArgsConstructor
@Component("goToCreateTutorialAction")
public class GoToCreateTutorialAction extends BasicAction {

    @Override
    public String execute() {
        try {
            super.execute();

            initActiveTutorialStatus(false);
            initTeachers(false);
            initActiveStudents();

            return SUCCESS;

        } catch (Exception e) {
            log.error("ERROR " + e.getMessage());
            setMessageError("ERROR " + e.getMessage());
            e.printStackTrace();
        }
        return ERROR;
    }

    private void initActiveStudents() {
        if (isTeacher()) {
            initStudents(false);
        } else {
            log.info("No se cargarán los usuarios activos");
        }
    }


}

