/**
 * @author david
 */
package com.dgc.academy.action.schedule;

import com.dgc.academy.action.common.BasicAction;
import com.dgc.academy.model.dto.EventDto;
import com.dgc.academy.model.dto.ScheduleDto;
import com.dgc.academy.model.dto.WeekDayDto;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Log4j2
@Getter
@Setter
@NoArgsConstructor
@Component("getCalendarAction")
public class GetCalendarAction extends BasicAction {

    private final String DATE_TIME_PATTERN = "yyyy-MM-dd HH:mm:ss";

    ArrayList<EventDto> allEventList;

    public String execute() throws Exception {
        super.execute();

        log.info("Obteniendo el horario para el usuario " + getUser());

        List<ScheduleDto> scheduleList = getScheduleService().getUserSchedule(getUser());

        if (scheduleList == null || scheduleList.isEmpty()) {
            log.warn("No se han obtenido Horarios");
            allEventList = new ArrayList<>();
        } else {
            getAllEvents(scheduleList);
            log.info("Obtenidos " + scheduleList.size() + " horarios");
        }

        return SUCCESS;
    }

    public void getAllEvents(@NonNull List<ScheduleDto> scheduleList) {
        allEventList = new ArrayList<>();

        for (ScheduleDto schedule : scheduleList) {
            log.debug("Procesando evento " + schedule);
            setEventIntoToAllMonth(schedule, getDaysOfMonth(schedule.getDay()));

        }
    }

    private List<LocalDate> getDaysOfMonth(@NonNull WeekDayDto day) {
        log.debug("Obtener todos los " + day.getName() + " del mes");
        LocalDate firstDayOfMonth = LocalDate.now().withDayOfMonth(1);
        LocalDate lastDayOfMonth = LocalDate.now().withDayOfMonth(
                Calendar.getInstance().getActualMaximum(Calendar.DAY_OF_MONTH)
        );

        long numDaysOfCurrentMonth = ChronoUnit.DAYS.between(firstDayOfMonth, lastDayOfMonth);

        return Stream.iterate(
                firstDayOfMonth, date -> date.plusDays(1)
        ).limit(numDaysOfCurrentMonth)
                .filter(date -> date.getDayOfWeek().getValue() == day.getId())
                .collect(Collectors.toList());
    }

    private void setEventIntoToAllMonth(@NonNull ScheduleDto schedule, @NonNull List<LocalDate> daysRange) {
        log.debug("Replicando evento de asignatura " + schedule.getSubject().getName() + " en todo el mes");


        for (LocalDate localDate : daysRange) {

            EventDto event =
                    EventDto.builder()
                            .start(getFormattedTime(localDate, getCalendarWithTime(schedule.getDay(), schedule.getStartTime())))
                            .end(getFormattedTime(localDate, getCalendarWithTime(schedule.getDay(), schedule.getEndTime())))
                            .allDay(false)
                            .title(schedule.getSubject().getName())
                            .build();

            allEventList.add(event);

            log.debug("Creado evento " + event);
        }

    }


    private Calendar getCalendarWithTime(@NonNull WeekDayDto day, @NonNull String time) {
        Calendar calendar = Calendar.getInstance();

        calendar.set(Calendar.DAY_OF_WEEK, day.getId());
        calendar.set(Calendar.HOUR_OF_DAY, Integer.parseInt(time.split(":")[0]));
        calendar.set(Calendar.MINUTE, Integer.parseInt(time.split(":")[1]));

        return calendar;
    }

    private String getFormattedTime(@NonNull LocalDate localDate, @NonNull Calendar time) {
        LocalDateTime localDateTime = localDate.atTime(time.get(Calendar.HOUR_OF_DAY), time.get(Calendar.MINUTE));
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(DATE_TIME_PATTERN);

        return localDateTime.format(dateTimeFormatter);
    }


}