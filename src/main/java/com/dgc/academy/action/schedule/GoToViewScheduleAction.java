/**
 * @author david
 */
package com.dgc.academy.action.schedule;

import com.dgc.academy.action.common.BasicAction;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;

@Log4j2
@Getter
@Setter
@NoArgsConstructor
@Component("goToViewScheduleAction")
public class GoToViewScheduleAction extends BasicAction {

    @Override
    public String execute() throws Exception {
        super.execute();

        log.info("Ir a la página de mostrar el calendario del usuario" + getUser());

        return SUCCESS;
    }

}
