/**
 * @author david
 */
package com.dgc.academy.action.common;


import com.dgc.academy.model.dto.*;
import com.dgc.academy.service.ScheduleService;
import com.dgc.academy.service.SubjectService;
import com.dgc.academy.service.TutorialService;
import com.dgc.academy.service.UserService;
import com.dgc.academy.util.GlobalConstant;
import com.opensymphony.xwork2.ActionSupport;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;
import org.apache.struts2.ServletActionContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;

import java.util.Collection;

@Getter
@Setter
@Log4j2
public class BasicAction extends ActionSupport {

    @Autowired
    private UserService userService;
    @Autowired
    private TutorialService tutorialService;
    @Autowired
    private SubjectService subjectService;
    @Autowired
    ScheduleService scheduleService;

    private UserDto user;
    private Collection<RoleDto> activeRoles;
    private Collection<LevelDto> activeLevels;
    private Collection<TutorialStatusDto> activeTutorialStatus;
    private Collection<UserDto> teachers;
    private Collection<UserDto> students;
    private Collection<SubjectDto> subjects;
    private Collection<WeekDayDto> activeWeekDays;
    private String messageError;
    private String message;

    @Override
    public String execute() throws Exception {
        initUser(true);
        return super.execute();
    }

    protected void initUser(boolean forceReload) {
        if (forceReload || user == null || user.getLogin() == null) {
            setUser((UserDto) ServletActionContext.getRequest()
                    .getSession().getAttribute(GlobalConstant.USER_HANDLE));
        }
    }

    protected void initStudents(boolean forceReload) {
        if (forceReload || CollectionUtils.isEmpty(students)) {
            students = getUserService().getActiveStudents();
            if (students == null || students.isEmpty()) {
                log.warn("No se han obtenido Estudiantes activos");
            } else {
                log.info("Obtenidos " + students.size() + " estudiantes activos");
            }
        }
    }

    protected void initTeachers(boolean forceReload) {
        if (forceReload || CollectionUtils.isEmpty(teachers)) {
            teachers = userService.getActiveTeachers();
            if (teachers == null || teachers.isEmpty()) {
                log.warn("No se han obtenido Profesores activos");
            } else {
                log.info("Obtenidos " + teachers.size() + " profesores activos");
            }
        }
    }

    protected void initSubjects(boolean forceReload) {
        initSubjects(forceReload, false);
    }

    protected void initSubjects(boolean forceReload, boolean displayDisabled) {
        if (forceReload || CollectionUtils.isEmpty(subjects)) {
            if (displayDisabled) {
                log.info("Obteniendo todas las asignaturas");
                subjects = getSubjectService().getAllSubjects();
            } else {
                log.info("Obteniendo todas las asignaturas activas");
                subjects = getSubjectService().getAllActiveSubjects();
            }

            if (subjects == null || subjects.isEmpty()) {
                log.warn("No se han obtenido asignaturas");
            } else {
                log.info("Obtenidas " + subjects.size() + " asignaturas");
            }
        }
    }

    protected void initActiveTutorialStatus(boolean forceReload) {
        if (forceReload || CollectionUtils.isEmpty(activeTutorialStatus)) {
            activeTutorialStatus = tutorialService.getActiveTutorialStatus();

            if (activeTutorialStatus == null || activeTutorialStatus.isEmpty()) {
                log.warn("No se han obtenido estados de Tutorías activos");
            } else {
                log.info("Obtenidos " + activeTutorialStatus.size() + " estados de Tutorías activos");
            }
        }
    }

    protected void initActiveLevels(boolean forceReload) {
        if (forceReload || CollectionUtils.isEmpty(activeLevels)) {
            activeLevels = subjectService.getActiveLevels();

            if (activeLevels == null || activeLevels.isEmpty()) {
                log.warn("No se han obtenido cursos activos");
            } else {
                log.info("Obtenidos " + activeLevels.size() + " cursos activos");
            }
        }
    }

    protected void initActiveRoles(boolean forceReload) {
        if (forceReload || CollectionUtils.isEmpty(activeRoles)) {
            activeRoles = userService.getActiveRoles();

            if (activeRoles == null || activeRoles.isEmpty()) {
                log.warn("No se han obtenido roles activos");
            } else {
                log.info("Obtenidos " + activeRoles.size() + " cursos activos");
            }
        }
    }

    protected void initActiveWeekDays(boolean forceReload) {
        if (forceReload || CollectionUtils.isEmpty(activeWeekDays)) {
            activeWeekDays = subjectService.getActiveWeekDays();

            if (activeWeekDays == null || activeWeekDays.isEmpty()) {
                log.warn("No se han obtenido días activos");
            } else {
                log.info("Obtenidos " + activeWeekDays.size() + " días activos");
            }
        }
    }

    protected WeekDayDto getWeekDayById(Byte dayId, boolean forceReload) {
        if (forceReload || CollectionUtils.isEmpty(activeWeekDays)) {
            initActiveWeekDays(forceReload);
        }

        return activeWeekDays.stream()
                .filter(day -> dayId.equals(day.getId()))
                .findAny()
                .orElse(null);
    }

    protected UserDto getTeacherById(Integer teacherId, boolean forceReload) {
        if (forceReload || CollectionUtils.isEmpty(teachers)) {
            initTeachers(forceReload);
        }

        return teachers.stream()
                .filter(teacher -> teacherId.equals(teacher.getId()))
                .findAny()
                .orElse(null);
    }

    protected UserDto getStudentById(Integer studentId, boolean forceReload) {
        if (forceReload || CollectionUtils.isEmpty(students)) {
            initStudents(forceReload);
        }

        return students.stream()
                .filter(student -> studentId.equals(student.getId()))
                .findAny()
                .orElse(null);
    }

    protected SubjectDto getSubjectById(Integer subjectId, boolean forceReload) {
        if (forceReload || CollectionUtils.isEmpty(subjects)) {
            initSubjects(forceReload);
        }

        return subjects.stream()
                .filter(subject -> subjectId.equals(subject.getId()))
                .findAny()
                .orElse(null);
    }

    protected TutorialStatusDto getTutorialStatusByName(String statusName, boolean forceReload) {
        if (forceReload || CollectionUtils.isEmpty(activeTutorialStatus)) {
            initActiveTutorialStatus(forceReload);
        }

        return activeTutorialStatus.stream()
                .filter(status -> statusName.equals(status.getName()))
                .findAny()
                .orElse(null);
    }

    protected TutorialStatusDto getTutorialStatusById(Integer statusId, boolean forceReload) {
        if (forceReload || CollectionUtils.isEmpty(activeTutorialStatus)) {
            initActiveTutorialStatus(forceReload);
        }

        return activeTutorialStatus.stream()
                .filter(status -> statusId.equals(status.getId()))
                .findAny()
                .orElse(null);
    }

    protected LevelDto getLevelById(Integer levelId, boolean forceReload) {
        if (forceReload || CollectionUtils.isEmpty(activeLevels)) {
            initActiveLevels(forceReload);
        }

        return activeLevels.stream()
                .filter(level -> levelId.equals(level.getId()))
                .findAny()
                .orElse(null);
    }

    protected RoleDto getRoleById(Integer roleId, boolean forceReload) {
        if (forceReload || CollectionUtils.isEmpty(activeRoles)) {
            initActiveRoles(forceReload);
        }

        return activeRoles.stream()
                .filter(role -> roleId.equals(role.getId()))
                .findAny()
                .orElse(null);
    }

    protected RoleDto getRoleByDescription(String roleDesc, boolean forceReload) {
        if (forceReload || CollectionUtils.isEmpty(activeRoles)) {
            initActiveRoles(forceReload);
        }

        return activeRoles.stream()
                .filter(role -> roleDesc.equals(role.getDescription()))
                .findAny()
                .orElse(null);
    }

    public boolean hasNotifications() {
        if (user == null || user.getLogin() == null) {
            return false;
        } else {
            Collection<TutorialDto> unreadTutorials = null;
            if (isTeacher()) {
                log.debug("Obteniendo notificaciones pendientes para el profesor " + user.getLogin());
                unreadTutorials = tutorialService.getUnReadTutorialsByTeacher(getUser());
            } else if (isStudent()) {
                log.debug("Obteniendo notificaciones pendientes para el alumno " + user.getLogin());
                unreadTutorials = tutorialService.getUnReadTutorialsByUser(getUser());
            } else {
                return false;
            }
            boolean hasNotifications = !CollectionUtils.isEmpty(unreadTutorials);
            log.debug("El usuario " + user + " " + (hasNotifications ? "No" : "Si") + " tiene peticiones pendientes");
            return hasNotifications;
        }
    }

    public boolean isAdmin() {
        return user != null && user.getRole() != null
                && user.getRole().getDescription().equals(GlobalConstant.ADMIN_ROLE);
    }

    public boolean isTeacher() {
        return user != null && user.getRole() != null
                && user.getRole().getDescription().equals(GlobalConstant.TEACHER_ROLE);
    }

    public boolean isStudent() {
        return user != null && user.getRole() != null
                && user.getRole().getDescription().equals(GlobalConstant.STUDENT_ROLE);
    }

    public UserDto getUser() {
        if (user == null || user.getLogin() == null) {
            initUser(false);
        }
        return user;
    }
}
