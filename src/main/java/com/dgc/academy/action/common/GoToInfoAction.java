/**
 * @author david
 */
package com.dgc.academy.action.common;

import com.dgc.academy.util.NoLogginRequired;
import org.springframework.stereotype.Component;

@NoLogginRequired
@Component("goToInfoAction")
public class GoToInfoAction extends BasicAction {

    @Override
    public String execute() throws Exception {
        super.execute();

        return SUCCESS;
    }
}
