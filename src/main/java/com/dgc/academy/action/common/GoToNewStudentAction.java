/**
 * @author david
 */
package com.dgc.academy.action.common;

import com.dgc.academy.util.NoLogginRequired;
import org.springframework.stereotype.Component;

@NoLogginRequired
@Component("goToNewStudentAction")
public class GoToNewStudentAction extends BasicAction {

    @Override
    public String execute() throws Exception {
        super.execute();

        return SUCCESS;
    }

}
