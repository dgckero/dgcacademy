/**
 * @author david
 */
package com.dgc.academy.action.common;

import com.dgc.academy.service.MailService;
import com.dgc.academy.util.GlobalConstant;
import com.dgc.academy.util.NoLogginRequired;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Log4j2
@Getter
@Setter
@NoLogginRequired
@Component("sendMailAction")
public class SendMailAction extends BasicAction {

    private String EMAIL_SUBJECT = "contacto DGCAcademy";

    private String name;
    private String emailFrom;

    @Autowired
    private MailService mailService;

    @Override
    public String execute() throws Exception {
        String res = ERROR;
        try {
            mailService.sendMail(emailFrom, GlobalConstant.EMAIL_CONTACT, EMAIL_SUBJECT, getMessage(), name);
            setMessageError(null);
            setMessage(null);
            res = SUCCESS;
        } catch (Exception e) {
            log.error("Error al enviar el email " + e.getMessage());
            setMessageError("Error al enviar el email " + e.getMessage());
            e.printStackTrace();
            res = ERROR;
        }
        return res;
    }

    @Override
    public void validate() {
        clearErrorsAndMessages();
        if (StringUtils.isEmpty(name)) {
            log.warn("No se ha informado el nombre");
            addFieldError("name", "Debe informar el Nombre");
        } else if (StringUtils.isEmpty(emailFrom)) {
            log.warn("No se ha informado el email");
            addFieldError("emailFrom", "Debe informar el correo de contacto");
        } else if (StringUtils.isEmpty(getMessage())) {
            log.warn("No se ha informado mensaje");
            addFieldError("message", "Debe cumplimentar el mensaje a enviar");
        }
    }

}
