/**
 * @author david
 */
package com.dgc.academy.action.common;

import org.springframework.stereotype.Component;

@Component("goToIndexAction")
public class GoToIndexAction extends BasicAction {

    @Override
    public String execute() throws Exception {
        super.execute();

        return SUCCESS;
    }
}
