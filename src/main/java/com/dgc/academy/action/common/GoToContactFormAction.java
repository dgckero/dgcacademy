/**
 * @author david
 */
package com.dgc.academy.action.common;

import com.dgc.academy.util.NoLogginRequired;
import org.springframework.stereotype.Component;

@NoLogginRequired
@Component("goToContactFormAction")
public class GoToContactFormAction extends BasicAction {

    @Override
    public String execute() throws Exception {
        super.execute();

        return SUCCESS;
    }
}
