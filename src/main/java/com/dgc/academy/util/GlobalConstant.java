/**
 * @author david
 */
package com.dgc.academy.util;

public interface GlobalConstant {

    String USER_HANDLE = "userLogged";
    String GO_TO_INDEX = "index";
    String ACTIVE_FILTER = "activeFilter";
    String STUDENT_ROLE = "ALUMNO";
    String ADMIN_ROLE = "ADMINISTRADOR";
    String TEACHER_ROLE = "PROFESOR";
    String PENDING_TUTORIAL_STATUS = "Pendiente";

    String EMAIL_CONTACT = "dgctrips@gmail.com";
}
