/**
 * @author david
 */
package com.dgc.academy.util;

import lombok.extern.log4j.Log4j2;

import java.math.BigInteger;
import java.security.MessageDigest;

@Log4j2
public class Encryption {

    public static String encrypt(String source) {
        String md5;
        try {
            MessageDigest mdEnc = MessageDigest.getInstance("MD5"); // Encryption algorithm
            mdEnc.update(source.getBytes(), 0, source.length());
            md5 = new BigInteger(1, mdEnc.digest()).toString(16); // Encrypted string
        } catch (Exception ex) {
            log.error("Error encriptando sorce " + source + " " + ex.getMessage());
            return null;
        }
        return md5;
    }

}
