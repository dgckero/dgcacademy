CREATE USER 'root'@'localhost' IDENTIFIED BY 'root';
CREATE USER 'academyApp'@'localhost' IDENTIFIED BY 'password';

CREATE DATABASE DBAcademia DEFAULT CHARACTER SET utf8;

GRANT ALL PRIVILEGES ON DBAcademia. * TO 'root'@'localhost';
GRANT SELECT, INSERT, UPDATE, DELETE ON DBAcademia. * TO 'academyApp'@'localhost';

use DBAcademia;


/*Tabla `role` */

DROP TABLE IF EXISTS `role`;

CREATE TABLE `role` (
  `ID` int(2) NOT NULL,
  `DESCRIPTION` varchar(16) ,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `DESCRIPTION` (`DESCRIPTION`)
) ENGINE=InnoDB ;

/*Datos para la tabla  `role */
insert  into `role` (`ID`,`DESCRIPTION`) values (1,'ADMINISTRADOR'),(2,'PROFESOR'),(3,'ALUMNO');


/*Tabla `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `ID` int(4) NOT NULL AUTO_INCREMENT,
  `ROLE` int(2) NOT NULL,
  `NAME` varchar(16) ,
  `SURNAME` varchar(32) ,
  `ACTIVE` tinyint(1) NOT NULL  DEFAULT 1,
  `LAST_LOGIN` DATETIME DEFAULT NULL,
  `LOGIN` varchar(10) ,
  `PASSWORD` char(60) ,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `LOGIN_ACTIVE` (`LOGIN`, `ACTIVE`),
  KEY `ROLE` (`ROLE`),
  CONSTRAINT `user_role` FOREIGN KEY (`ROLE`) REFERENCES `role` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=1 ;


/*Datos para la tabla  `user` */
insert  into `user`(`ROLE`,`NAME`,`SURNAME`,`ACTIVE`,`LAST_LOGIN`,`LOGIN`,`PASSWORD`) values 
(1,'Administrador',null,1,now(),'admin',md5('admin')),
(1,'David','Gómez Castellanos',1,now(),'David',md5('password')),
(2,'Laura','López López',1,now(),'Laura',md5('password')),
(3,'Pepe','Pérez López',1,now(),'Pepe',md5('password')),
(3,'Manuel','López Sánchez',1,	now(),'Manuel',md5('password')),
(2,'Paco','López López',0,NULL,'paco',md5('password')),
(3,'Jose','Pérez López',1,now(),'jose',md5('password')),
(3,'Perico','Sánchez Sánchez',1,now(),'Perico',md5('password'));


/* Tabla  `level` */

DROP TABLE IF EXISTS `level`;

CREATE TABLE `level` (
  `ID` int(3) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(60) ,
  `ACTIVE` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `NAME_ACTIVE` (`NAME`, `ACTIVE`)
) ENGINE=InnoDB AUTO_INCREMENT=1 ;

/*Datos para la tabla  `level` */
insert  into `level`(`NAME`) values 
('1º ESO'),
('2º ESO'),
('3º ESO'),
('4º ESO'),
('Grado en Ingeniería de Sistemas de Informacion'),
('Enferfería'),
('Economía'),
('Derecho');


/*Tabla `subject` */

DROP TABLE IF EXISTS `subject`;

CREATE TABLE `subject` (
  `ID` int(4) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(40) ,
  `ACTIVE` tinyint(1) NOT NULL  DEFAULT 1,
  `LEVEL` int(3) NOT NULL,
  PRIMARY KEY (`ID`),
  CONSTRAINT `subject_level` FOREIGN KEY (`LEVEL`) REFERENCES `level` (`ID`),
  UNIQUE KEY `NAME_LEVEL_ACTIVE` (`NAME`,`LEVEL`, `ACTIVE`)
) ENGINE=InnoDB AUTO_INCREMENT=1 ;

/*Datos para la tabla  `subject` */
insert  into `subject`(`NAME`,`LEVEL`) values 
('Matemáticas',3),
('Matemáticas',4),
('Inglés',3),
('Inglés',1),
('Análisis Matemático',5),
('Inglés',5),
('Inglés',4),
('Fundamentos Físicos de la Informática',5),
('Álgebra',5),
('Sistemas Operativos I',5),
('Sistemas Operativos II',5),
('Programación Orientada a Objetos I',5),
('Programación Orientada a Objetos II',5)
;

/*Tabla `user_subject` */
DROP TABLE IF EXISTS `user_subject`;


CREATE TABLE `user_subject`  (
	`ID` int(5) NOT NULL AUTO_INCREMENT,
	`USER` int(4) NOT NULL,
	`SUBJECT` int(4) NOT NULL,
	`ACTIVE` tinyint(1) NOT NULL DEFAULT 1,
	  PRIMARY KEY (`ID`),
	CONSTRAINT `subject` FOREIGN KEY (`SUBJECT`) REFERENCES `subject` (`ID`),
	CONSTRAINT `user` FOREIGN KEY (`USER`) REFERENCES `user` (`ID`),
	UNIQUE KEY `NAME_LEVEL_ACTIVE` (`USER`,`SUBJECT`,`ACTIVE`)
) ENGINE=InnoDB AUTO_INCREMENT=1 ;

/*Datos para la tabla  `schedule` */
insert  into user_subject (`USER`,`SUBJECT`) values 
((select id from user where name='Laura') , (select ID from subject where name='Matemáticas' and level=4)),
((select id from user where name='Pepe') , (select ID from subject where name='Sistemas Operativos I' and level=5)),
((select id from user where name='Manuel') , (select ID from subject where name='Álgebra' and level=5)),
((select id from user where name='Paco') , (select ID from subject where name='Fundamentos Físicos de la Informática' and level=5))
;

/* Tabla `week_day` */
DROP TABLE IF EXISTS `week_day`;

CREATE TABLE `week_day` (
  `ID` TINYINT NOT NULL AUTO_INCREMENT,
  `NAME` varchar(9) ,
  `ACTIVE` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `NAME` (`NAME`)
) ENGINE=InnoDB AUTO_INCREMENT=1;

/*Datos para la tabla  `week_day` */
insert  into `week_day`(`NAME`,`ACTIVE`) values 
('Domingo',0),
('Lunes',1),
('Martes',1),
('Miércoles',1),
('Jueves',1),
('Viernes',1),
('Sábado',0);


/*Tabla `schedule` */
DROP TABLE IF EXISTS `schedule`;

CREATE TABLE `schedule`  (
	`ID` int(5) NOT NULL AUTO_INCREMENT,
	`DAY` TINYINT NOT NULL,
	`SUBJECT` int(4) NOT NULL,
	`START_TIME` varchar(5) NOT NULL,
	`END_TIME` varchar(5) NOT NULL,
	`ACTIVE` tinyint(1) NOT NULL DEFAULT 1,
	  PRIMARY KEY (`ID`),
	CONSTRAINT `subject_schedule` FOREIGN KEY (`SUBJECT`) REFERENCES `subject` (`ID`),
	CONSTRAINT `day_schedule` FOREIGN KEY (`DAY`) REFERENCES `week_day` (`ID`),
	UNIQUE KEY `NAME_LEVEL_ACTIVE` (`DAY`,`SUBJECT`, `START_TIME`,`END_TIME`, `ACTIVE`)
) ENGINE=InnoDB AUTO_INCREMENT=1 ;

/*Datos para la tabla  `schedule` */
insert  into schedule (`DAY`,`SUBJECT`,`START_TIME`,`END_TIME`) values 
( (select ID from week_day where name='Lunes'), (select ID from subject where name='Matemáticas' and level=4), '19:00', '20:00'),
( (select ID from week_day where name='Martes'), (select ID from subject where name='Inglés' and level=4), '19:00', '20:00'),
( (select ID from week_day where name='Lunes'), (select ID from subject where name='Álgebra' and level=5), '20:00', '21:00'),
( (select ID from week_day where name='Martes'), (select ID from subject where name='Sistemas Operativos I' and level=5), '20:00', '21:00'),
( (select ID from week_day where name='Miércoles'), (select ID from subject where name='Sistemas Operativos II' and level=5), '19:00', '20:00'),
( (select ID from week_day where name='Jueves'), (select ID from subject where name='Álgebra' and level=5), '19:00', '20:00'),
( (select ID from week_day where name='Jueves'), (select ID from subject where name='Fundamentos Físicos de la Informática' and level=5), '20:00', '21:00'),
( (select ID from week_day where name='Viernes'), (select ID from subject where name='Matemáticas' and level=4), '18:00', '19:00')
;


/*Tabla `tutorial_status` */
DROP TABLE IF EXISTS `tutorial_status`;

CREATE TABLE `tutorial_status` (
  `ID` int(2) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(10) ,
  `DESCRIPTION` varchar(100) ,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `NAME` (`NAME`)
) ENGINE=InnoDB AUTO_INCREMENT=1 ;

/*Datos para la tabla  `tutorial_status */
insert  into `tutorial_status` (`NAME`, `DESCRIPTION`) values 
('Pendiente','La petición de tutoría todavía no ha sido leída por el profesor'),
('Aceptado','La petición de tutoría ha sido aceptada por el profesor'),
('Rechazado','La petición de tutoría ha sido rechazada por el profesor');


/*Tabla `tutorial` */

DROP TABLE IF EXISTS `tutorial`;

CREATE TABLE `tutorial`  (
	`ID` int(4) NOT NULL AUTO_INCREMENT,
	`START_TIME` timestamp  NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`END_TIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`TEACHER` int(4) NOT NULL,
	`STUDENT` int(4) NOT NULL,
  `COMMENT` varchar(200),
	`TUTORIAL_STATUS` int(2) NOT NULL,
  `NOTIFIED` tinyint(1) NOT NULL DEFAULT 0,  
	  PRIMARY KEY (`ID`),
	CONSTRAINT `student_tutorial` FOREIGN KEY (`STUDENT`) REFERENCES `user` (`ID`),
	CONSTRAINT `teacher_tutorial` FOREIGN KEY (`TEACHER`) REFERENCES `user` (`ID`),
	CONSTRAINT `tutorial_status` FOREIGN KEY (`TUTORIAL_STATUS`) REFERENCES `tutorial_status` (`ID`),
	UNIQUE KEY `TUTORIAL_TIME` (`START_TIME`,`END_TIME`, `TEACHER`,`STUDENT`, `TUTORIAL_STATUS`)
) ENGINE=InnoDB AUTO_INCREMENT=1 ;


/*Datos para la tabla  `tutorial */
insert  into `tutorial` (`START_TIME`, `END_TIME`,  `TEACHER`, `STUDENT`, `TUTORIAL_STATUS`) values 
( now(), date_add(now(), interval 1 HOUR) , (select id from user where name='Laura'), (select id from user where name='Pepe'), (select id from tutorial_status where name='Aceptado')  ),
( (select addtime(now(), "48:00")), (select addtime(now(), "49:00")), (select id from user where name='Paco'), (select id from user where name='Manuel'), (select id from tutorial_status where name='Pendiente')  ) ,
( (select addtime(now(), "72:00")), (select addtime(now(), "73:00")), (select id from user where name='Paco'), (select id from user where name='Jose'), (select id from tutorial_status where name='Pendiente')  );



/*Tabla `notification` */

DROP TABLE IF EXISTS `notification`;

CREATE TABLE `notification`  (
	`ID` int(6) NOT NULL AUTO_INCREMENT,
	`CREATE_DATE` timestamp  NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`READ_TIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`ORIGIN_USER` int(4) NOT NULL,
	`DESTINATION_USER` int(4) NOT NULL,
	`MESSAGE` varchar(150) NOT NULL,
	  PRIMARY KEY (`ID`),
	CONSTRAINT `origin_user` FOREIGN KEY (`ORIGIN_USER`) REFERENCES `user` (`ID`),
	CONSTRAINT `destination_user` FOREIGN KEY (`DESTINATION_USER`) REFERENCES `user` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=1 ;

