<%@ taglib prefix="dgcacademy" tagdir="/WEB-INF/tags/" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>

<!DOCTYPE HTML>
<html>
    <head>
        <dgcacademy:header tittle="Operaci�n realizada"></dgcacademy:header>
    </head>
    <body>
        <div class="container-fluid" id="generalContainer">
            <div class="navbar">
                <dgcacademy:container/>
            </div>

            <div class="row" id="internalContainer">
                <aside id="application-status" class="span3">
                    <dgcacademy:menu/>
                </aside>
                <section id="main" class="span10">
                    <div class="dgc-operation-ok">
                        <h1 align="center">Operaci�n realizada con �xito</h1>
                        <img src="/DgcAcademy/images/toolbar/success.jpg" alt="Success">
                        <c:if test="${not empty message }">
                            <h4>${message}</h4>
                        </c:if>
                    </div>
                </section>
            </div>
        </div>
    </body>
    <jsp:include page="/WEB-INF/pages/common/footer.jsp">
        <jsp:param value="" name="" />
    </jsp:include>
</html>