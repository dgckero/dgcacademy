<%@ taglib prefix="dgcacademy" tagdir="/WEB-INF/tags/" %>

<!DOCTYPE HTML>
<html>
    <head>
        <dgcacademy:header tittle="Informaci�n"></dgcacademy:header>
    </head>

    <body>

        <div class="container-fluid" id="generalContainer">
            <div class="navbar">
                <dgcacademy:container/>
            </div>

            <div class="row" id="internalContainer">
                <aside id="application-status" class="span3">
                    <dgcacademy:menu/>
                </aside>
                <section id="main" class="span9 ml-5">
                    <div class="dgc-inner-main" align="center">
                        <p class="page_title"><h1>Informaci�n</h1>
                        <div class="ml-5">
                            <h4>Datos e informaci�n mostrada en la Web</h4>
                            <p>Toda la informaci�n referente a usuarios mostrados en la aplicaci�n no se corresponde con usuarios reales</p>
                            <br>
                            <h4>A cerca del proyecto</h4>
                            <p>Este website se ha desarrollado como trabajo obligatorio de las asignaturas desarrollo de Aplicaciones Web I y II del Curso de Adaptaci�n al Grado en Ingenier�a de Sistemas de la Informaci�n.</p>
                            <p>Consiste en el dise�o e implementaci�n de un sistema capaz de gestionar una academia de clases particulares</p>
                            <p><a class="dgc-brand" href="/DgcAcademy/goToIndexAction.action">DgcAcademy</a> est� desarrollado por <a href="mailto:dgckero@gmail.com">David G�mez Castellanos</a></p>
                        </div>
                    </div>
                    <div class="row-fluid" align="center" >
                        <h2 align="center">Esta Web ha sido desarrollada utilizando </h2>
                        <section id="topbarInfoBuilt">
                            <aside>
                                <a id="topInfoJava" rel="nofollow" href="http://www.java.com" title="Java" target="_blank">Java</a>
                                <a id="toptInfoHibernate" rel="nofollow" href="http://www.hibernate.org" title="Hibernate" target="_blank">Hibernate</a>
                                <a id="topInfoMaven" rel="nofollow" href="http://maven.apache.org" title="Maven" target="_blank">Maven</a>
                                <a id="topInfoSpring" rel="nofollow" href="https://spring.io/" title="Spring" target="_blank">Spring</a>
                                <a id="topInfoStruts" href="http://struts.apache.org" title="Struts" target="_blank">Struts</a>
                                <a id="topInfoBootstrap" href="https://getbootstrap.com/" title="Bootstrap" target="_blank">Bootstrap</a>
                            </aside>
                        </section>
                    </div>
                </section>
                <div class="mb44" align="center">
                    <p class="font-weight-bold ml-5">Tambi�n estoy en las principales redes sociales</p>
                    <div align="center">
                        <section id="topbarInfo" class="ml-5">
                            <aside>
                                <a id="topInfofacebook" rel="nofollow" href="http://www.facebook.com/DavidGzazG" title="Facebook" target="_blank">Facebook</a>
                                <a id="toptInfowitter" rel="nofollow" href="http://twitter.com/DavidGzazG" title="Twitter" target="_blank">Twitter</a>
                                <a id="topInfogoogleplus" rel="nofollow" href="https://plus.google.com/103081947162358255015" title="Google+" target="_blank">Google+</a>
                                <a id="topInfolinkedin" href="www.linkedin.com/in/david-g�mez-castellanos-9b200635" title="LinkedIn" target="_blank">LinkedIn</a>
                            </aside>
                        </section>
                    </div>
                </div>
            </div>
        </div>
    </body>
    <jsp:include page="/WEB-INF/pages/common/footer.jsp">
        <jsp:param value="" name="" />
    </jsp:include>
</html>