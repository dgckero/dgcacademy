<footer>
    Por favor utilice el <a title="formulario de contacto" href="/DgcAcademy/goToContactFormAction.action">formulario de contacto</a>
    o la direcci�n de correo electr�nico
    <a title="Email dgckero@gmail.com" href="mailto:dgckero@gmail.com">dgckero@gmail.com</a>
    para ponerse en contacto con el administrador.
    <span class="license-cc-footer">
        Este proyecto est� bajo una
        <a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/">licencia de Creative Commons Reconocimiento-NoComercial 4.0 Internacional</a>.
        <a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/">
            <img alt="Licencia de Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-nc/4.0/80x15.png" />
        </a>
    </span>
</footer>