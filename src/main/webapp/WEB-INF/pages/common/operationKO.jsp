<%@ taglib prefix="dgcacademy" tagdir="/WEB-INF/tags/" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>

<!DOCTYPE HTML>
<html>
    <head>
        <dgcacademy:header tittle="Operación NO realizada"></dgcacademy:header>
    </head>
    <body>
        <div class="container-fluid" id="generalContainer">
            <div class="navbar">
                <dgcacademy:container/>
            </div>

            <div class="row" id="internalContainer">
                <aside id="application-status" class="span3">
                    <dgcacademy:menu/>
                </aside>
                <section id="main" class="span9">
                    <div class="dgc-operation-ko">
                        <h1 align="center">ERROR</h1>
                        <c:if test="${not empty messageError }">
                            <h2>${messageError}</h2>
                        </c:if>
                        <p>Por favor repita de nuevo la operación</p>
                        </br>
                        <img src="/DgcAcademy/images/toolbar/system-error-command.jpg" alt="Error">
                    </div>
                </section>
            </div>
        </div>
    </body>
    <jsp:include page="/WEB-INF/pages/common/footer.jsp">
        <jsp:param value="" name="" />
    </jsp:include>
</html>