<%@ taglib prefix="dgcacademy" tagdir="/WEB-INF/tags/" %>
<%@ taglib prefix="s" uri="/struts-tags" %>

<!DOCTYPE HTML>
<html>
    <head>
        <s:head/>
        <dgcacademy:header tittle="Ver Asignatura"></dgcacademy:header>
    </head>
    <body>
        <div class="container-fluid" id="generalContainer">
            <div class="navbar">
                <dgcacademy:container/>
            </div>

            <div class="row" id="internalContainer">
                <aside id="application-status" class="span3">
                    <dgcacademy:menu/>
                </aside>
                <section id="main" class="span12 ml-5">
                    <div class="dgc-inner-main" align="center">
                        <table cellpadding="0" cellspacing="0" border="0" id="editProfileTable">
                            <tbody>
                                <tr>
                                    <td>
                                        <p>Identificador</p>
                                        <input type="text" size="11" id="ident" name="ident"
                                               value="${selectedSubject.id}" disabled="disabled" />
                                        <input type="hidden" id="idSubject" name="idSubject"
                                               value="${selectedSubject.id}"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <p>Nombre</p>
                                        <input type="text" size="32" name="name" id="name"
                                               value="${selectedSubject.name}" required readonly="readonly" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <p>Curso</p>
                                        <input type="text" size="32" name="level" id="level"
                                               value="${selectedSubject.level.name}" disabled="disabled" />
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <br>
                        <button class="btn" type="button" onclick="javascript:history.back();"><i class='cus-no-sign'></i> volver</button>
                    </div>
                    <div class="row-fluid">
                        <s:if test="hasActionErrors()">
                            <div class="alert alert-danger" role="alert">
                                <s:actionerror/>
                            </div>
                        </s:if>
                    </div>
                </section>
            </div>
        </div>
    </body>
    <jsp:include page="/WEB-INF/pages/common/footer.jsp">
        <jsp:param value="" name="" />
    </jsp:include>
</html>