<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="dgcacademy" tagdir="/WEB-INF/tags/" %>
<%@ taglib prefix="s" uri="/struts-tags" %>


<!DOCTYPE HTML>
<html>
    <head>
        <s:head/>
        <dgcacademy:header tittle="Listado de Tutorías"></dgcacademy:header>
            <script>
                $(document).ready(function () {
                    var table = $('#tutorialsTable').dataTable({
                        "columnDefs": [{
                                "targets": 'no-sort',
                                "orderable": false,
                            }]
                    });
                });
            </script>

    </head>
    <body>
        <div class="container-fluid" id="generalContainer">
            <div class="navbar">
                <dgcacademy:container/>
            </div>
            <div class="row" id="internalContainer">
                <aside id="application-status" class="span3">
                    <dgcacademy:menu/>
                </aside>
                <section id="main" class="span12 ml-5">
                    <div class="dgc-inner-main mt-4" align="center">
                        <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="tutorialsTable">
                            <thead>
                                <tr class="success">
                                    <th>Identificador</th>
                                    <th>Fecha de Comienzo</th>
                                    <th>Fecha de Fin</th>
                                        <s:if test="isStudent()">
                                        <th>Profesor</th>
                                        </s:if>
                                        <s:elseif test="isTeacher()">
                                        <th>Alumno</th>
                                        </s:elseif>
                                    <th>Estado de la tutoría</th>
                                        <s:if test="isTeacher()">
                                        <th class="no-sort">Acciones</th>
                                        </s:if>
                                </tr>
                            </thead>
                            <tbody>
                                <s:iterator value="tutorials" var="tutoria" status="status">
                                    <tr>
                                        <td>
                                            <s:property value="id" />
                                        </td>
                                        <td>
                                            <s:property value="startTime" />
                                        </td>
                                        <td>
                                            <s:property value="endTime" />
                                        </td>
                                        <s:if test="isStudent()">
                                            <td>
                                                <s:property value="teacher.name" />
                                            </td>
                                        </s:if>
                                        <s:elseif test="isTeacher()">
                                            <td>
                                                <s:property value="student.name" />
                                            </td>
                                        </s:elseif>
                                        <td>
                                            <s:property value="tutorialStatus.name" />
                                        </td>
                                        <s:if test="isTeacher()">
                                            <td class="no-sort">
                                                <a class="btn btn-info" href="<s:url action="goToViewTutorialAction" namespace="/tutorial"><s:param name="selectedTutorial.id" value="#tutoria.id" /></s:url>">Ver</a>
                                                <s:if test="#tutoria.tutorialStatus.name eq 'Pendiente' ">
                                                    <a class="btn btn-warning" href="<s:url action="goToEditTutorialAction" namespace="/tutorial"><s:param name="selectedTutorial.id" value="#tutoria.id" /></s:url>">Contestar</a>
                                                </s:if>
                                            </td>
                                        </s:if>
                                    </tr>
                                </s:iterator>
                            </tbody>
                        </table>
                    </div>
                    <div class="row-fluid">
                        <s:if test="hasActionErrors()">
                            <div class="alert alert-danger" role="alert">
                                <s:actionerror/>
                            </div>
                        </s:if>
                    </div>
                </section>
            </div>
        </div>
    </body>
    <jsp:include page="/WEB-INF/pages/common/footer.jsp">
        <jsp:param value="" name="" />
    </jsp:include>
</html>