<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%@ taglib prefix="dgcacademy" tagdir="/WEB-INF/tags/" %>

<!DOCTYPE HTML>
<html>
    <head>
        <s:head/>
        <dgcacademy:header tittle="Ver Tutoría"></dgcacademy:header>
    </head>
    <body>
        <div class="container-fluid" id="generalContainer">
            <div class="navbar">
                <dgcacademy:container/>
            </div>

            <div class="row" id="internalContainer">
                <aside id="application-status" class="span3">
                    <dgcacademy:menu/>
                </aside>
                <section id="main" class="span12 ml-5 mt-5">
                    <div class="dgc-inner-main" align="center">
                        <s:form action="goToViewTutorialAction" namespace="/tutorial" >
                            <table class="form-horizontal" id="editProfileTable">
                                <tbody>
                                    <tr>
                                        <td>
                                            <input type="hidden" id="ident" name="ident"
                                                   value="${selectedTutorial.id}"/>
                                            <input type="hidden" id="idTutorial" name="idTutorial"
                                                   value="${selectedTutorial.id}" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <p>Alumno Solicitante</p>
                                            <input type="text" size="32" name="student" id="student"
                                                   value="${selectedTutorial.student.name}" required readonly="readonly" /></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <p>Fecha de comienzo</p>
                                            <input type="text" size="32" name="startTime" id="startTime"
                                                   value="${selectedTutorial.startTime}" required readonly="readonly" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <p>Fecha de fin</p>
                                            <input type="text" size="32" name="endTime" id="endTime"
                                                   value="${selectedTutorial.endTime}" required readonly="readonly" />
                                        </td>
                                    </tr>
                                    <c:if test="${not empty selectedTutorial.comment}">
                                        <tr>
                                            <td>
                                                <p>Nota/Comentario</p>
                                                <input type="text" size="32" name="comment" id="comment"
                                                       value="${selectedTutorial.comment}" required readonly="readonly" />
                                            </td>
                                        </tr>
                                    </c:if>
                                    <tr>
                                        <td>
                                            <p>Estado</p>
                                            <input type="text" name="status" id="status"
                                                   value="${selectedTutorial.tutorialStatus.name}" readonly="readonly" />
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <br>
                            <button class="btn" type="button" onclick="javascript:history.back();"><i class='cus-no-sign'></i>Volver</button>
                        </s:form>
                    </div>
                    <div class="row-fluid">
                        <s:if test="hasActionErrors()">
                            <div class="alert alert-danger" role="alert">
                                <s:actionerror/>
                            </div>
                        </s:if>
                    </div>
                </section>
            </div>
        </div>
    </body>
    <jsp:include page="/WEB-INF/pages/common/footer.jsp">
        <jsp:param value="" name="" />
    </jsp:include>
</html>