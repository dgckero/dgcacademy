<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>

<%@ taglib prefix="dgcacademy" tagdir="/WEB-INF/tags/" %>
<%@ taglib uri="/struts-tags" prefix="s"%>

<!DOCTYPE HTML>
<html>
    <head>
        <s:head/>
        <dgcacademy:header tittle="Ver Perfil" />
    </head>
    <body>
        <div class="container-fluid" id="generalContainer">
            <div class="navbar">
                <dgcacademy:container/>
            </div>
            <div class="row" id="internalContainer">
                <aside id="application-status" class="span3">
                    <dgcacademy:menu/>
                </aside>
                <section id="main" class="span12 ml-5">
                    <div class="container dgc-inner-main pt-5">
                        <form role="form" action="goToEditProfileAction" method="post">
                            <div class="col-lg-6 push-lg-3 personal-info">
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label form-control-label">Nombre</label>
                                    <div class="col-lg-9">
                                        <input class="form-control" type="text" readonly="readonly" value="${userToBeUpdated.name}" />
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label form-control-label">Apellido</label>
                                    <div class="col-lg-9">
                                        <input class="form-control" type="text" readonly="readonly" value="${userToBeUpdated.surname}" />
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label form-control-label">login</label>
                                    <div class="col-lg-9">
                                        <input class="form-control" type="text" readonly="readonly" value="${userToBeUpdated.login}" />
                                    </div>
                                </div>
                                <div class="dgc-buttons-div">
                                    <div class="dgc-button">
                                        <s:submit cssClass="btn btn-primary btn-lg" type="button" action="goToEditProfileAction"><i class='cus-write'></i> Editar</s:submit>
                                    </div>
                                    <div class="dgc-button">
                                        <s:submit cssClass="btn btn-secondary btn-lg" type="button" action="goToIndexAction" ><i class='cus-no-sign'></i> Menú</s:submit>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </section>
            </div>
        </div>
    </body>
    <jsp:include page="/WEB-INF/pages/common/footer.jsp">
        <jsp:param value="" name="" />
    </jsp:include>
</html>