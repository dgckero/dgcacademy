<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="dgcacademy" tagdir="/WEB-INF/tags/" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE HTML>
<html>
    <head>
        <s:head/>
        <dgcacademy:header tittle="Introduzca los datos del nuevo Usuario"></dgcacademy:header>
    </head>
    <body>
        <div class="container-fluid" id="generalContainer">
            <div class="navbar">
                <dgcacademy:container/>
            </div>

            <div class="row" id="internalContainer">
                <aside id="application-status" class="span3">
                    <dgcacademy:menu/>
                </aside>
                <section id="main" class="span12 ml-5">
                    <div class="dgc-inner-main" align="center">
                        <s:form action="newUserAction" namespace="/user">
                            <table class="form-horizontal" id="newSubjectTable">
                                <tbody>
                                    <tr>
                                        <td>
                                            <s:textfield name="name" label="Nombre" class="input-xlarge" requiredLabel="true"/><s:fielderror fieldName="name"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <s:textfield  name="surname" label="Apellidos"  class="input-xlarge" requiredLabel="true" /><s:fielderror fieldName="surname"/>
                                        </td>
                                    </tr>								
                                    <tr>
                                        <td>
                                            <s:textfield name="login" label="Login" class="input-xlarge" requiredLabel="true" /><s:fielderror fieldName="login"/>
                                        </td>
                                    </tr>								
                                    <tr>
                                        <td>
                                            <s:password name="password" label="Contraseña" class="input-xlarge" requiredLabel="true" /><s:fielderror fieldName="password"/>
                                        </td>
                                    </tr>								
                                    <tr>
                                        <td>
                                            <s:password name="password2" label="Repetir Contraseña" class="input-xlarge" requiredLabel="true" /><s:fielderror fieldName="password2"/>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>							
                            <s:if test="isAdmin()">
                                <div class="control-group">
                                    <div class="controls">
                                        <s:select label="Rol" name="selectedRoleId"
                                                  list="activeRoles" listKey="id" listValue="description" headerKey="-1" headerValue="Seleccione un Rol"/>
                                    </div>
                                </div>
                            </s:if>
                            <div class="dgc-buttons-div">
                                <div class="dgc-button">
                                    <s:submit cssClass="btn btn-primary large" type="button" action="newUserAction"><i class='cus-save'></i> Enviar</s:submit>
                                    </div>
                                    <div class="dgc-button">
                                    <s:submit type="button" action="goToIndexAction" ><i class='cus-no-sign'></i> Cancelar</s:submit>
                                    </div>
                                </div>
                        </s:form>
                    </div>
                    <div class="row-fluid">
                        <s:if test="hasActionErrors()">
                            <div class="alert alert-danger" role="alert">
                                <s:actionerror/>
                            </div>
                        </s:if>
                    </div>
                </section>
            </div>
        </div>
    </body>
    <jsp:include page="/WEB-INF/pages/common/footer.jsp">
        <jsp:param value="" name="" />
    </jsp:include>
</html>