<%@ taglib prefix="dgcacademy" tagdir="/WEB-INF/tags/" %>
<%@ taglib prefix="s" uri="/struts-tags" %>

<!DOCTYPE HTML>
<html>
    <head>
        <s:head/>
        <dgcacademy:header tittle="Ver Horario"></dgcacademy:header>

            <script>
                $(document).ready(function () {
                    $("div[class^='sk-chase']").show();
                    $.ajax({
                        type: 'GET',
                        url: '/DgcAcademy/json/getCalendarAction',
                        dataType: 'json',
                        success: function (data) {

                            var date = new Date();
                            var d = date.getDate();
                            var m = date.getMonth();
                            var y = date.getFullYear();
                            var calendarEl = document.getElementById('calendar');

                            var calendar = new FullCalendar.Calendar(calendarEl, {
                                plugins: ['dayGrid', 'bootstrap', 'list'],
                                events: data.allEventList,
                                themeSystem: 'bootstrap4',
                                eventLimit: true,
                                header: {
                                    right: 'dayGridMonth, dayGridWeek, dayGridDay,timeGridWeek,timeGridDay,listWeek'
                                },
                                defaultView: 'dayGridMonth',
                                locale: 'es',
                                displayEventTime: true,
                                eventTimeFormat: {
                                    hour: '2-digit',
                                    minute: '2-digit'
                                },
                                height: 500,
                                validRange: function () {
                                    var firstDayOfMonth = new Date(y, m, 0);
                                    var lastDayOfMonth = new Date(y, m + 1, 1);
                                    return {start: firstDayOfMonth, end: lastDayOfMonth};
                                },
                                eventRender: function (info) {
                                    var tooltip = new Tooltip(info.el, {
                                        title: info.event.extendedProps.title,
                                        placement: 'top',
                                        trigger: 'hover',
                                        container: 'body'
                                    });
                                }
                            });

                            calendar.render();
                            $("div[class^='sk-chase']").hide();
                        }});
                });
            </script>
    </head>
    <body>
        <div class="container-fluid" id="generalContainer">
            <div class="navbar">
                <dgcacademy:container/>
            </div>

            <div class="row" id="internalContainer">
                <aside id="application-status" class="span3">
                    <dgcacademy:menu/>
                </aside>
                <section id="main" class="span12 ml-5">
                    <div class="dgc-inner-main mt-3" align="center">
                        <div id="calendar" class="dgc-calendar"/>
                        <div class="sk-chase">
                            <div class="sk-chase-dot"/>
                            <div class="sk-chase-dot"/>
                            <div class="sk-chase-dot"/>
                            <div class="sk-chase-dot"/>
                            <div class="sk-chase-dot"/>
                            <div class="sk-chase-dot"/>
                        </div>
                    </div>
                    <div class="row">
                        <s:if test="hasActionErrors()">
                            <div class="alert alert-danger" role="alert">
                                <s:actionerror/>
                            </div>
                        </s:if>
                    </div>
                </section>
            </div>
        </div>
    </body>
    <jsp:include page="/WEB-INF/pages/common/footer.jsp">
        <jsp:param value="" name="" />
    </jsp:include>
</html>