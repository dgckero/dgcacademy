<%@ taglib prefix="dgcacademy" tagdir="/WEB-INF/tags/"%>
<%@	taglib uri="/struts-tags" prefix="s"%>

<!DOCTYPE HTML>
<html>
    <head>
        <s:head/>
        <dgcacademy:header tittle="Contacto"></dgcacademy:header>
    </head>
    <body>
        <div class="container-fluid" id="generalContainer">
            <div class="navbar">
                <dgcacademy:container/>
            </div>
            <div class="row" id="internalContainer">
                <aside id="application-status" class="span3">
                    <dgcacademy:menu/>
                </aside>
                <section id="main" class="span10 ml-5" >
                    <div class="dgc-inner-main pt-5" align="center">
                        <div class="page-header">
                            <h2>Env�anos un mensaje</h2>
                        </div>
                        <p>Queremos saber de ti! S�lo tienes que introducir tu nombre, direcci�n de correo electr�nico y el mensaje en el siguiente formulario y enviarlo</p>
                        <p>Todos los campos son obligatorios</p>
                        <s:form action="sendMailAction" namespace="/" >
                            <fieldset>
                                <div class="clearfix">
                                    <div class="input">
                                        <s:textfield label="Nombre" name="name" class="input-xlarge" requiredLabel="true" />
                                    </div>
                                </div>
                                <div class="clearfix">
                                    <div class="input">
                                        <s:textfield label="Email" name="emailFrom" class="input-xlarge" requiredLabel="true" />
                                    </div>
                                </div>
                                <div class="clearfix">
                                    <div class="input">
                                        <s:textarea label="Mensaje" name="message" cols="20" rows="7" requiredLabel="true"/>
                                    </div>
                                </div>
                                <div class="actions">
                                    <s:submit cssClass="btn primary large" type="button" ><i class='cus-email'></i> Enviar email</s:submit>
                                    </div>
                                </fieldset>
                        </s:form>
                    </div>
                    <div class="row-fluid">
                        <s:if test="hasActionErrors()">
                            <div class="alert alert-danger" role="alert">
                                <s:actionerror/>
                            </div>
                        </s:if>
                    </div>
                </section>
            </div>
        </div>
    </body>
    <jsp:include page="/WEB-INF/pages/common/footer.jsp">
        <jsp:param value="" name="" />
    </jsp:include>
</html>