<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>

<%@ taglib prefix="dgcacademy" tagdir="/WEB-INF/tags/" %>
<%@ taglib uri="/struts-tags" prefix="s"%>

<!DOCTYPE HTML>
<html>
    <head>
        <s:head/>
        <dgcacademy:header tittle="Editar Perfil" />

        <script>
            $(document).ready(function () {
                $('#togglePassword').change(function () {
                    if ($(this).prop('checked')) {
                        $("#password-div").removeClass('invisible');
                    } else {
                        $("#password-div").addClass('invisible');
                    }
                    $("input#modifyPasswordActive").val($(this).prop("checked"));
                });
            });

        </script>

    </head>
    <body>
        <div class="container-fluid" id="generalContainer">
            <div class="navbar">
                <dgcacademy:container/>
            </div>
            <div class="row" id="internalContainer">
                <aside id="application-status" class="span3">
                    <dgcacademy:menu/>
                </aside>
                <section id="main" class="span12 ml-5">
                    <div class="container dgc-inner-main">
                        <div class="col-lg-8 push-lg-4 personal-info">

                            <form role="form" action="editProfileAction" method="post">
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label form-control-label">Nombre</label>
                                    <div class="col-lg-9">
                                        <input class="form-control" type="text" name="name" id="name" value="${userToBeUpdated.name}" />
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label form-control-label">Apellido</label>
                                    <div class="col-lg-9">
                                        <input class="form-control" type="text" name="surname" id="surname" value="${userToBeUpdated.surname}" />
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label form-control-label">login</label>
                                    <div class="col-lg-9">
                                        <input class="form-control" type="text" name="login" id="login" readonly="readonly" value="${userToBeUpdated.login}" />
                                    </div>
                                </div>
                                <input type="hidden" name="id" id="id" value="${userToBeUpdated.id}" />
                                <br/>
                                <br/>
                                <div class="justify-content-md-center row">
                                    <s:textfield type="hidden" name="modifyPasswordActive" id="modifyPasswordActive" />
                                    <label class="col-lg-3 col-form-label form-control-label">Cambiar Contraseña</label>
                                    <input id="togglePassword" type="checkbox" data-toggle="toggle" data-on="Si" data-off="No" data-onstyle="success" data-offstyle="danger" data-size="lg"/>
                                </div>
                                <br/>
                                <br/>
                                <div id="password-div" class="invisible">
                                    <div class="form-group row">
                                        <label class="col-lg-3 col-form-label form-control-label">Password</label>
                                        <div class="col-lg-9">
                                            <input class="form-control" type="password" name="newPassword" id="newPassword" />
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-3 col-form-label form-control-label">Confirm password</label>
                                        <div class="col-lg-9">
                                            <input class="form-control" type="password" name="newPasswordConfirmed" id="newPasswordConfirmed" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row-fluid">
                                    <s:if test="hasActionErrors()">
                                        <div class="alert alert-danger" role="alert">
                                            <s:actionerror/>
                                        </div>
                                    </s:if>
                                </div>
                                <div class="dgc-buttons-div">
                                    <div class="dgc-button">
                                        <s:submit cssClass="btn btn-warning btn-lg" type="button" action="editProfileAction"><i class='cus-save'></i> Guardar</s:submit>
                                    </div>
                                    <div class="dgc-button">
                                        <s:submit cssClass="btn btn-secondary btn-lg" type="button" action="goToIndexAction" ><i class='cus-no-sign'></i> Menú</s:submit>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <hr/>
                </section>
            </div>
        </div>
    </body>
    <jsp:include page="/WEB-INF/pages/common/footer.jsp">
        <jsp:param value="" name="" />
    </jsp:include>
</html>