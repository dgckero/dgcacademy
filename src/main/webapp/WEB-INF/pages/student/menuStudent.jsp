<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>


<ul class="list-unstyled components">
    <li>
        <a href="/DgcAcademy/user/goToViewScheduleAction">Ver Horario de Clases</a>
    </li>
    <li>
        <a href="/DgcAcademy/tutorial/goToCreateTutorialAction">Solicitar Tutoría</a>
    </li>
    <li>
        <a href="/DgcAcademy/tutorial/goToViewTutorialRequestedAction">Ver Tutorías Seleccionadas</a>
    </li>
    <li>
        <a href="/DgcAcademy/goToInfoAction">Información</a>
    </li>
</ul>