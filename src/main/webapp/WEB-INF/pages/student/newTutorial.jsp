<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="dgcacademy" tagdir="/WEB-INF/tags/" %>
<%@ taglib prefix="s" uri="/struts-tags" %>

<!DOCTYPE HTML>
<html>
    <head>
        <s:head/>
        <dgcacademy:header tittle="Introduzca los datos de la Tutoría"></dgcacademy:header>

            <script>
                $(document).ready(function () {
                    $('#datetimes').daterangepicker({
                        timePicker24Hour: true,
                        singleMonth: true,
                        timePicker: true,
                        timePickerIncrement: 15,
                        locale: {
                            separator: ' a ',
                            language: 'es',
                            format: 'DD-MM HH:mm',
                            daysOfWeek: [
                                "Do",
                                "Lu",
                                "Ma",
                                "Mi",
                                "Ju",
                                "Vi",
                                "Sa"
                            ],
                            monthNames: [
                                "Enero",
                                "Febrero",
                                "Marzo",
                                "Abril",
                                "Mayo",
                                "Junio",
                                "Julio",
                                "Agosto",
                                "Setiembre",
                                "Octubre",
                                "Noviembre",
                                "Diciembre"
                            ],
                            applyLabel: "Seleccionar",
                            cancelLabel: "Cancelar",
                            resetLabel: "Resetear",
                            fromLabel: "Desde",
                            toLabel: "Hasta",
                            customRangeLabel: "Personalizar"
                        },
                        opens: "center",
                        startDate: moment().add(1, 'days'),
                        endDate: moment().add(1, 'days').add(1, 'hours'),
                        minDate: moment().add(1, 'days').add(1, 'hours').set({hour: 8, minute: 30, second: 0, millisecond: 0}),
                        maxDate: moment().add(1, 'month').set({hour: 21, minute: 00, second: 0, millisecond: 0}).set({hour: 21, minute: 00, second: 0, millisecond: 0})
                    }, function (start, end) { //callback
                        $('input[name="startTime"]').val(start.format());
                        $('input[name="endTime"]').val(end.format());
                        console.log(start.format(), end.format());
                    }).on('setStartDate.daterangepicker', function (ev, picker) {
                        picker.setEndDate(moment(picker.startDate).add(1, 'hours'));
                        picker.container.find(".calendar-table").hide(); //Hide calendar
                    }).on('reset.daterangepicker', function (ev, picker) {
                        picker.container.find(".calendar-table").show(); //Show calendar
                    });

                    $('#datetimes').val($('#datetimes').attr("placeholder"));
                });
            </script>

    </head>
    <body>
        <div class="container-fluid" id="generalContainer">
            <div class="navbar">
                <dgcacademy:container/>
            </div>

            <div class="row" id="internalContainer">
                <aside id="application-status" class="span3">
                    <dgcacademy:menu/>
                </aside>
                <section id="main" class="span12 ml-5">
                    <div class="dgc-inner-main pt-5" align="center">
                        <s:form action="newTutorialAction" namespace="/tutorial" class="simple_form form-vertical">
                            <table class="form-horizontal" id="newSubjectTable">
                                <tbody>
                                    <tr>
                                        <td>
                                            <label for="datetimes">Fecha de la tutoría:
                                            </label>
                                            <input type="text" name="datetimes" id="datetimes" class="dgc-time-range" placeholder=" Seleccione la fecha de la tutoría" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <s:select label="Profesor" name="selectedTeacherId" list="teachers" listKey="id" listValue="name"/>
                                        </td>
                                    </tr>								
                                    <tr>
                                        <td>
                                            <s:textarea label="Nota/Motivo" name="newTutorial.comment" cols="20" rows="3"/>
                                            <input type="hidden" name="startTime" value="${newTutorial.startTime}" />
                                            <input type="hidden" name="endTime" value="${newTutorial.endTime}" />
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <div class="dgc-buttons-div">
                                <div class="dgc-button">
                                    <s:submit cssClass="btn btn-primary large" type="button" action="newTutorialAction"><i class='cus-save'></i> Guardar</s:submit>
                                    </div>
                                    <div class="dgc-button">
                                    <s:submit type="button" action="goToIndexAction" ><i class='cus-no-sign'></i> Cancelar</s:submit>
                                    </div>
                                </div>
                        </s:form>
                    </div>
                    <div class="row-fluid">
                        <s:if test="hasActionErrors()">
                            <div class="alert alert-danger" role="alert">
                                <s:actionerror/>
                            </div>
                        </s:if>
                    </div>
                </section>
            </div>
        </div>
    </body>
    <jsp:include page="/WEB-INF/pages/common/footer.jsp">
        <jsp:param value="" name="" />
    </jsp:include>
</html>