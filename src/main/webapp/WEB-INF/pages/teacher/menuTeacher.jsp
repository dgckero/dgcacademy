<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>

<ul class="list-unstyled components">
    <li>
        <a href="/DgcAcademy/user/goToViewScheduleAction">Ver Horario de Clases</a>
    </li>
    <li>
        <a href="/DgcAcademy/tutorial/goToViewTeacherTutorialRequestedAction">Contestar Peticiones de Tutorías</a>
    </li>
    <li>
        <a href="/DgcAcademy/subject/showSubjectsAction">Asignaturas impartidas</a>
    </li>
    <li>
        <a href="/DgcAcademy/goToInfoAction">Información</a>
    </li>
</ul>
