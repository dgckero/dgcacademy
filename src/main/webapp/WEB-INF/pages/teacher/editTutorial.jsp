<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>

<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%@ taglib prefix="dgcacademy" tagdir="/WEB-INF/tags/" %>

<!DOCTYPE HTML>
<html>
    <head>
        <s:head/>
        <dgcacademy:header tittle="Editar Tutoría"></dgcacademy:header>
    </head>
    <body>

        <div class="container-fluid" id="generalContainer">
            <div class="navbar">
                <dgcacademy:container/>
            </div>

            <div class="row" id="internalContainer">
                <aside id="application-status" class="span3">
                    <dgcacademy:menu/>
                </aside>
                <section id="main" class="span12 ml-5 mt-3">
                    <div class="dgc-inner-main" align="center">
                        <s:form action="editTutorialAction" namespace="/tutorial" >
                            <table class="form-horizontal" id="editProfileTable">
                                <tbody>
                                    <s:textfield type="hidden" name="selectedTutorial.id" />
                                    <s:textfield type="hidden" name="selectedTutorial.student.id" />
                                    <tr>
                                        <td>
                                            <p>Alumno Solicitante</p>
                                            <s:textfield type="text" name="selectedTutorial.student.name" readonly="true"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <p>Fecha de comienzo</p>
                                            <s:textfield name="selectedTutorial.startTime" value="%{getText('{0,date,dd/MM/yyyy hh:mm}',{selectedTutorial.startTime})}" readonly="true" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <p>Fecha de fin</p>
                                            <s:textfield name="selectedTutorial.endTime" value="%{getText('{0,date,dd/MM/yyyy hh:mm}',{selectedTutorial.endTime})}" readonly="true"/>
                                        </td>
                                    </tr>
                                    <c:if test="${not empty selectedTutorial.comment}">
                                        <tr>
                                            <td>
                                                <p>Nota/Comentario</p>
                                                <input type="text" size="32" name="comment" id="comment"
                                                       value="${selectedTutorial.comment}" required readonly="readonly" />
                                            </td>
                                        </tr>
                                    </c:if>
                                    <tr>
                                        <td>
                                            <p>Estado</p>
                                            <s:select name="selectedTutorialStatusId" list="activeTutorialStatus"
                                                      listKey="id" listValue="name" value="%{selectedTutorial.tutorialStatus.id}"/>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <br>
                            <div class="dgc-buttons-div">
                                <div class="dgc-button">
                                    <s:submit cssClass="btn btn-primary large" type="button" action="editTutorialAction"><i class='cus-save'></i> Guardar</s:submit>
                                    </div>
                                    <div class="dgc-button">
                                    <s:submit type="button" action="goToIndexAction" ><i class='cus-no-sign'></i> Cancelar</s:submit>
                                    </div>
                                </div>
                        </s:form>
                    </div>
                    <div class="row-fluid">
                        <s:if test="hasActionErrors()">
                            <div class="alert alert-danger" role="alert">
                                <s:actionerror/>
                            </div>
                        </s:if>
                    </div>
                </section>
            </div>
        </div>
    </body>
    <jsp:include page="/WEB-INF/pages/common/footer.jsp">
        <jsp:param value="" name="" />
    </jsp:include>
</html>