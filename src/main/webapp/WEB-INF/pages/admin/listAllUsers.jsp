<%@ taglib prefix="dgcacademy" tagdir="/WEB-INF/tags/" %>
<%@ taglib prefix="s" uri="/struts-tags" %>

<!DOCTYPE HTML>
<html>
    <head>
        <s:head/>
        <dgcacademy:header tittle="Listado de Usuarios"></dgcacademy:header>

            <script>
                $(document).ready(function () {
                    var table = $('#subjectsTable').dataTable({
                        "createdRow": function (row, data) {
                            $.each($('td', row, data), function () {
                                if (data[3] === "No") {
                                    $(row).attr('title', 'Usuario deshabilitado');
                                    $(row).addClass('dgc-row-inactive');
                                }
                            });
                        },
                        "columnDefs": [{
                                "targets": 'no-sort',
                                "orderable": false,
                            }]
                    });
                });
            </script>

    </head>
    <body>
        <div class="container-fluid" id="generalContainer">
            <div class="navbar">
                <dgcacademy:container/>
            </div>
            <div class="row" id="internalContainer">
                <aside id="application-status" class="span3">
                    <dgcacademy:menu/>
                </aside>
                <section id="main" class="span12 ml-5">
                    <div class="dgc-inner-main" align="center">

                        <s:form id="listUsersFormId" name="listUsersForm" namespace="/user" action="goToNewUserAction">
                            <s:submit cssClass="btn btn-primary large" type="button" ><i class='cus-save'></i> Nuevo Usuario</s:submit>
                        </s:form>
                        <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="subjectsTable">
                            <thead>
                                <tr class="success">
                                    <th>login</th>
                                    <th>Nombre</th>
                                    <th>Apellido</th>
                                    <th>Activo</th>
                                    <th>Rol</th>
                                    <th class="no-sort">Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                <s:iterator value="users" var="user" status="status">
                                    <tr>
                                        <td>
                                            <s:property value="login" />
                                        </td>
                                        <td>
                                            <s:property value="name" />
                                        </td>
                                        <td>
                                            <s:property value="surname" />
                                        </td>
                                        <td>
                                            <s:if test="#user.active eq true">
                                                Si
                                            </s:if>
                                            <s:else>
                                                No
                                            </s:else>
                                        </td>
                                        <td>
                                            <s:property value="role.description" />
                                        </td>
                                        <td class="no-sort">
                                            <a class="btn btn-info" href="<s:url action="goToViewUserAction" namespace="/user"><s:param name="selectedUser.id" value="#user.id" ></s:param></s:url>"> Ver </a>
                                            <a class="btn btn-warning" href="<s:url action="goToEditUserAction" namespace="/user"><s:param name="selectedUser.id" value="#user.id" ></s:param></s:url>"> Editar </a>
                                            <a class="btn btn-danger" href="<s:url action="deleteUserAction" namespace="/user"><s:param name="selectedUser.id" value="#user.id" ></s:param></s:url>"> Borrar </a>
                                        </td>
                                    </tr>
                                </s:iterator>
                            </tbody>
                        </table>
                    </div>
                    <div class="row-fluid">
                        <s:if test="hasActionErrors()">
                            <div class="alert alert-danger" role="alert">
                                <s:actionerror/>
                            </div>
                        </s:if>
                    </div>
                </section>
            </div>
        </div>
    </body>
    <jsp:include page="/WEB-INF/pages/common/footer.jsp">
        <jsp:param value="" name="" />
    </jsp:include>
</html>