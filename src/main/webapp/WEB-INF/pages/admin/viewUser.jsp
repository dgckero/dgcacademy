<%@ taglib prefix="dgcacademy" tagdir="/WEB-INF/tags/" %>
<%@ taglib prefix="s" uri="/struts-tags" %>

<!DOCTYPE HTML>
<html>
    <head>
        <s:head/>
        <dgcacademy:header tittle="Ver Usuario"></dgcacademy:header>
    </head>
    <body>
        <div class="container-fluid" id="generalContainer">
            <div class="navbar">
                <dgcacademy:container/>
            </div>

            <div class="row" id="internalContainer">
                <aside id="application-status" class="span3">
                    <dgcacademy:menu/>
                </aside>
                <section id="main" class="span12 ml-5">
                    <div class="dgc-inner-main" align="center">
                        <form id="viewUserForm" >
                            <table class="form-horizontal"
                                   id="editProfileTable">
                                <tbody>
                                    <tr>
                                        <td>
                                            <p>Identificador</p>
                                            <input type="text" size="11" id="ident" name="ident"
                                                   value="${selectedUser.id}" disabled="disabled" />
                                            <input type="hidden" id="idUser" name="idUser"
                                                   value="${selectedUser.id}"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <p>Nombre</p>
                                            <input type="text" size="32" name="name" id="name"
                                                   value="${selectedUser.name}" required readonly="readonly" /></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <p>Apellido</p>
                                            <input type="text" size="32" name="surname" id="surname"
                                                   value="${selectedUser.surname}" required readonly="readonly" /></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <p>Login</p>
                                            <input type="text" size="32" name="login" id="login"
                                                   value="${selectedUser.login}" disabled="disabled" /></td>
                                    </tr>
                                </tbody>
                            </table>
                            <br>
                            <button class="btn" type="button" onclick="javascript:history.back();"><i class='cus-no-sign'></i> Cancelar</button>
                        </form>
                    </div>
                    <div class="row-fluid">
                        <s:if test="hasActionErrors()">
                            <div class="alert alert-danger" role="alert">
                                <s:actionerror/>
                            </div>
                        </s:if>
                    </div>
                </section>
            </div>
        </div>
    </body>
    <jsp:include page="/WEB-INF/pages/common/footer.jsp">
        <jsp:param value="" name="" />
    </jsp:include>
</html>