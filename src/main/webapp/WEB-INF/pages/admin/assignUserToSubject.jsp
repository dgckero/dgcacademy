<%@ taglib prefix="dgcacademy" tagdir="/WEB-INF/tags/" %>
<%@ taglib prefix="s" uri="/struts-tags" %>

<!DOCTYPE HTML>
<html>
    <head>
        <s:head/>
        <dgcacademy:header tittle="Asignar usuario a Asignatura"></dgcacademy:header>

            <script>
                $(document).ready(function () {
                    $("#studentSelect").prop("disabled", false);

                    $('input[name="userType"]').click(function () {
                        if ($('input[name="userType"]').is(':checked')) {
                            var radioValue = $("input[name='userType']:checked").val();
                            if (radioValue == "student") {
                                $("#studentSelect").prop("disabled", false);
                                $("#teacherSelect").prop("disabled", true);
                                $('#teacherSelect option').eq(0).prop('selected', true);
                            } else {
                                $('#studentSelect option').eq(0).prop('selected', true);
                                $("#studentSelect").prop("disabled", true);
                                $("#teacherSelect").prop("disabled", false);
                            }
                        }
                    });
                });
            </script>

    </head>
    <body>
        <div class="container-fluid" id="generalContainer">
            <div class="navbar">
                <dgcacademy:container/>
            </div>

            <div class="row" id="internalContainer">
                <aside id="application-status" class="span3">
                    <dgcacademy:menu/>
                </aside>
                <section id="main" class="span12 ml-5">
                    <div class="dgc-inner-main" align="center">
                        <s:form action="assignUserToSubjectAction" namespace="/user" class="simple_form form-vertical">
                            <table class="form-horizontal" id="newSubjectTable">
                                <tbody>
                                    <tr>
                                        <td>
                                            <fieldset class="form-group">
                                                <div class="dgc-buttons-div">
                                                    <div class="dgc-button">
                                                        <input class="form-check-input" type="radio" name="userType" id="gridRadios1" value="student" checked/>
                                                        <label class="dgc-check-label" for="gridRadios1">
                                                            Estudiante
                                                        </label>
                                                    </div>
                                                    <div class="dgc-button">
                                                        <input class="form-check-input" type="radio" name="userType" id="gridRadios2" value="teacher"/>
                                                        <label class="dgc-check-label" for="gridRadios2">
                                                            Profesor
                                                        </label>
                                                    </div>
                                                </div>
                                            </fieldset>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>											
                                            <s:select id="inputSubject" label="Asignatura" class="form-control" name="selectedSubjectId" list="subjects" listKey="id" listValue="name"
                                                      headerKey="-1" headerValue="Selecciona una Asignatura"/>
                                        </td>
                                    </tr>								
                                    <tr>
                                        <td>
                                            <s:select class="form-control" label="Profesor" disabled="true" id="teacherSelect" name="selectedTeacherId" list="teachers" listKey="id" listValue="name"
                                                      headerKey="-1" headerValue="Selecciona un Profesor"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <s:select class="form-control" label="Estudiante" disabled="true" id="studentSelect" name="selectedStudentId" list="students" listKey="id" listValue="name"
                                                      headerKey="-1" headerValue="Selecciona un Estudiante"/>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>							

                            <div class="dgc-buttons-div">
                                <div class="dgc-button">
                                    <s:submit cssClass="btn btn-primary large" type="button" ><i class='cus-save'></i> Guardar</s:submit>
                                </div>
                                <div class="dgc-button">
                                    <s:submit type="button" action="goToIndexAction" ><i class='cus-no-sign'></i> Cancelar</s:submit>
                                </div>
                            </div>
                        </s:form>
                    </div>
                    <div class="row-fluid">
                        <s:if test="hasActionErrors()">
                            <div class="alert alert-danger" role="alert">
                                <s:actionerror/>
                            </div>
                        </s:if>
                    </div>
                </section>
            </div>
        </div>
    </body>
    <jsp:include page="/WEB-INF/pages/common/footer.jsp">
        <jsp:param value="" name="" />
    </jsp:include>
</html>