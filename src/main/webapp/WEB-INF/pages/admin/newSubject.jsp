<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="dgcacademy" tagdir="/WEB-INF/tags/" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE HTML>
<html>
    <head>
        <s:head/>
        <dgcacademy:header tittle="Crear Nueva Asignatura"></dgcacademy:header>

            <script>
                $(document).ready(function () {
                    $('#datetimes').daterangepicker({
                        timePicker24Hour: true,
                        singleMonth: true,
                        timePicker: true,
                        timePickerIncrement: 15,
                        locale: {
                            separator: ' a ',
                            language: 'es',
                            format: 'HH:mm',
                            applyLabel: "Seleccionar",
                            cancelLabel: "Cancelar",
                            fromLabel: "Desde",
                            toLabel: "Hasta",
                            customRangeLabel: "Personalizar"
                        },
                        opens: "center",
                        startDate: moment().set({hour: 8, minute: 30, second: 0, millisecond: 0}),
                        endDate: moment().set({hour: 9, minute: 30, second: 0, millisecond: 0}),
                        minDate: moment().set({hour: 8, minute: 30, second: 0, millisecond: 0}),
                        maxDate: moment().set({hour: 21, minute: 00, second: 0, millisecond: 0}),
                    }, function (start, end) { //callback
                        $('input[name="startTime"]').val(start.format('HH:mm'));
                        $('input[name="endTime"]').val(end.format('HH:mm'));
                        console.log(start.format('HH:mm'), end.format('HH:mm'));
                    }).on('apply.daterangepicker', function (ev, picker) {
                        $('input[name="startTime"]').val(picker.startDate.format('HH:mm'));
                        $('input[name="endTime"]').val(picker.endDate.format('HH:mm'));
                        console.log("apply.daterangepicker start " + picker.startDate.format('HH:mm') + " end " + picker.endDate.format('HH:mm'));
                    }).on('show.daterangepicker', function (ev, picker) {
                        picker.container.find(".calendar-table").hide(); //Hide calendar
                    });

                    $('#datetimes').val($('#datetimes').attr("placeholder"));
                });
            </script>

    </head>
    <body>
        <div class="container-fluid" id="generalContainer">
            <div class="navbar">
                <dgcacademy:container/>
            </div>

            <div class="row" id="internalContainer">
                <aside id="application-status" class="span3">
                    <dgcacademy:menu/>
                </aside>
                <section id="main" class="span12 ml-5">
                    <div class="dgc-inner-main" align="center">
                        <s:form action="newSubjectAction" namespace="/subject" validate="true">
                            <table class="form-horizontal" id="newSubjectTable">
                                <tbody>
                                    <tr>
                                        <td>
                                            <s:textfield label="Nombre" name="subject.name" class="input-xlarge" requiredLabel="true" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <s:select id="selectedLevelId" label="Curso al que pertenece" name="selectedLevelId"
                                                      list="activeLevels" listKey="id" listValue="name" headerKey="-1" headerValue="Seleccione el Curso al que pertenece"/>
                                        </td>
                                    </tr>								
                                    <tr>
                                        <td>
                                            <s:select id="weekDayId" label="Día que se imparte" name="weekDayId"
                                                      list="activeWeekDays" listKey="id" listValue="name" headerKey="-1" headerValue="Seleccione el día que se impartirá la asignatura"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            </br>
                                            <input type="text" id="datetimes" class="dgc-time-range" placeholder=" Selecciona la hora a la que se imparte" />
                                            <s:hidden name="startTime"/>
                                            <s:hidden name="endTime"/>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <div class="dgc-buttons-div">
                                <div class="dgc-button">
                                    <s:submit cssClass="btn btn-primary large" type="button" action="newSubjectAction"><i class='cus-save'></i> Guardar</s:submit>
                                </div>
                                <div class="dgc-button">
                                    <s:submit type="button" action="goToIndexAction" ><i class='cus-no-sign'></i> Cancelar</s:submit>
                                </div>
                            </div>
                        </s:form>
                    </div>
                    <div class="row-fluid">
                        <s:if test="hasActionErrors()">
                            <div class="alert alert-danger" role="alert">
                                <s:actionerror/>
                            </div>
                        </s:if>
                    </div>
                </section>                
            </div>
        </div>
    </body>
    <jsp:include page="/WEB-INF/pages/common/footer.jsp">
        <jsp:param value="" name="" />
    </jsp:include>
</html>