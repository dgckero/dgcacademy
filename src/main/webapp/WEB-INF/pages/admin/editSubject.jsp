<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="dgcacademy" tagdir="/WEB-INF/tags/" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE HTML>
<html>
    <head>
        <s:head/>        
        <dgcacademy:header tittle="Editar Asignatura"></dgcacademy:header>

            <script>
                $(document).ready(function () {
                    if (${selectedSubject.active}) {
                        $('#toggleSubject').bootstrapToggle('on');
                    } else {
                        $('#toggleSubject').bootstrapToggle('off');
                    }

                    $('#toggleSubject').change(function () {
                        $("input#active").val($(this).prop("checked"));
                    });
                });

        </script>

    </head>
    <body>
        <div class="container-fluid" id="generalContainer">
            <div class="navbar">
                <dgcacademy:container/>
            </div>

            <div class="row" id="internalContainer">
                <aside id="application-status" class="span3">
                    <dgcacademy:menu/>
                </aside>
                <section id="main" class="span12 ml-5">
                    <div class="dgc-inner-main" align="center">
                        <s:form action="editSubjectAction" namespace="/subject" >
                            <s:textfield type="hidden" name="selectedSubject.id" />
                            <table class="form-horizontal"
                                   id="editSubjectTable">
                                <tbody>
                                    <tr>
                                        <td>
                                            <p>Nombre</p>
                                            <s:textfield name="selectedSubject.name" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <p>Curso</p>
                                            <s:select name="selectedLevelId" list="activeLevels" listKey="id" listValue="name" value="%{selectedSubject.level.id}"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <s:textfield type="hidden" name="selectedSubject.active" id="active" />
                                            <input id="toggleSubject" type="checkbox" data-toggle="toggle" data-on="Activa" data-off="Inactiva" data-onstyle="success" data-offstyle="danger" data-size="large">
                                        </td>
                                    </tr>
                                </tbody>
                            </table>

                            <div class="dgc-buttons-div">
                                <div class="dgc-button">
                                    <s:submit cssClass="btn btn-warning large" type="button" action="editSubjectAction" ><i class='cus-write'></i> Editar</s:submit>
                                    </div>
                                    <div class="dgc-button">
                                    <s:submit type="button" action="goToListAllSubjectsAction" ><i class='cus-no-sign'></i> Cancelar</s:submit>
                                    </div>
                                </div>
                        </s:form>
                    </div>
                    <div class="row-fluid">
                        <s:if test="hasActionErrors()">
                            <div class="alert alert-danger" role="alert">
                                <s:actionerror/>
                            </div>
                        </s:if>
                    </div>
                </section>
            </div>
        </div>
    </body>
    <jsp:include page="/WEB-INF/pages/common/footer.jsp">
        <jsp:param value="" name="" />
    </jsp:include>
</html>