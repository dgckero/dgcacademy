<%@ taglib prefix="dgcacademy" tagdir="/WEB-INF/tags/" %>
<%@ taglib prefix="s" uri="/struts-tags" %>

<!DOCTYPE HTML>
<html>
    <head>
        <s:head/>
        <dgcacademy:header tittle="Listado de Asignaturas"></dgcacademy:header>

            <script>
                $(document).ready(function () {
                    var table = $('#subjectsTable').dataTable({
                        "createdRow": function (row, data) {
                            $.each($('td', row, data), function () {
                                if (data[2] === "No") {
                                    $(row).attr('title', 'Asignatura deshabilitada');
                                    $(row).addClass('dgc-row-inactive');
                                }
                            });
                        },
                        "columnDefs": [{
                                "targets": 'no-sort',
                                "orderable": false,
                            }]
                    });
                });
            </script>

    </head>
    <body>
        <div class="container-fluid" id="generalContainer">
            <div class="navbar">
                <dgcacademy:container/>
            </div>

            <div class="row" id="internalContainer">
                <aside id="application-status" class="span3">
                    <dgcacademy:menu/>
                </aside>
                <section id="main" class="span12 ml-5">
                    <div class="dgc-inner-main" align="center">

                        <s:form id="listSubjectsFormId" name="listSubjectsForm" namespace="/subject" action="goToNewSubjectAction">
                            <s:submit cssClass="btn btn-lg btn-primary" value="Nueva Asignatura"></s:submit>
                        </s:form>
                        <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="subjectsTable">
                            <thead>
                                <tr class="success">
                                    <th>Nombre</th>
                                    <th>Curso</th>
                                    <th>Activo</th>
                                    <th class="no-sort">Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                <s:iterator value="subjects" var="subject" status="status">
                                    <tr>
                                        <td>
                                            <s:property value="name" />
                                        </td>
                                        <td>
                                            <s:property value="level.name" />
                                        </td>
                                        <td>
                                            <s:if test="#subject.active eq true">
                                                Si
                                            </s:if>
                                            <s:else>
                                                No
                                            </s:else>
                                        </td>
                                        <td class="no-sort">
                                            <a class="btn btn-info" href="<s:url action="goToViewSubjectAction" namespace="/subject" ><s:param name="selectedSubject.id" value="#subject.id" ></s:param></s:url>"> Ver </a>
                                            <a class="btn btn-warning" href="<s:url action="goToEditSubjectAction" namespace="/subject" ><s:param name="selectedSubject.id" value="#subject.id" ></s:param></s:url>"> Editar </a>
                                            <a class="btn btn-danger" href="<s:url action="deleteSubjectAction" namespace="/subject" ><s:param name="selectedSubject.id" value="#subject.id" ></s:param></s:url>"> Borrar </a>
                                        </td>
                                    </tr>
                                </s:iterator>
                            </tbody>
                        </table>
                    </div>
                    <div class="row-fluid">
                        <s:if test="hasActionErrors()">
                            <div class="alert alert-danger" role="alert">
                                <s:actionerror/>
                            </div>
                        </s:if>
                    </div>
                </section>
            </div>
        </div>
    </body>
    <jsp:include page="/WEB-INF/pages/common/footer.jsp">
        <jsp:param value="" name="" />
    </jsp:include>
</html>