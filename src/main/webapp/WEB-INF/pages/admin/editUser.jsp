<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="dgcacademy" tagdir="/WEB-INF/tags/" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE HTML>
<html>
    <head>
        <s:head/>
        <dgcacademy:header tittle="Editar Usuario"></dgcacademy:header>

            <script>
                $(document).ready(function () {
                    // Set change password
                    $('#togglePassword').change(function () {
                        if ($(this).prop('checked')) {
                            $("#password-div").removeClass('invisible');
                        } else {
                            $("#password-div").addClass('invisible');
                        }
                        $("input#modifyPasswordActive").val($(this).prop("checked"));
                    });

                    // Set active/inactive user
                    if (${selectedUser.active}) {
                        $('#toggleUser').bootstrapToggle('on');
                    } else {
                        $('#toggleUser').bootstrapToggle('off');
                    }

                    $('#toggleUser').change(function () {
                        $("input#active").val($(this).prop("checked"));
                    });
                });

        </script>

    </head>
    <body>
        <div class="container-fluid" id="generalContainer">
            <div class="navbar">
                <dgcacademy:container/>
            </div>

            <div class="row" id="internalContainer">
                <aside id="application-status" class="span3">
                    <dgcacademy:menu/>
                </aside>
                <section id="main" class="span12 ml-5">
                    <div class="dgc-inner-main" align="center">
                        <s:form action="editUserAction" namespace="/user" >
                            <table class="form-horizontal"
                                   id="editSubjectTable">
                                <tbody>
                                    <tr>
                                        <td>
                                            <s:hidden name="id" id="id" />
                                            <div class="form-group row">
                                                <label class="col-lg-3 col-form-label form-control-label">Nombre</label>
                                                <div class="col-lg-9">
                                                    <input class="form-control" type="text" name="name" id="name" value="${selectedUser.name}"/>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-lg-3 col-form-label form-control-label">Apellido</label>
                                                <div class="col-lg-9">
                                                    <input class="form-control" type="text" name="surname" id="surname" value="${selectedUser.surname}" />
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-lg-3 col-form-label form-control-label">Login</label>
                                                <div class="col-lg-9">
                                                    <input class="form-control" type="text" name="login" id="login" value="${selectedUser.login}" />
                                                </div>
                                            </div>
                                            <s:if test="isAdmin()">
                                                <div class="float-left row">
                                                    <s:select name="selectedRoleId" label="Seleccione un Rol" list="activeRoles" listKey="id" listValue="description" value="%{selectedUser.role.id}"/>
                                                </div>
                                            </s:if>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <br/>
                            <br/>
                            <div class="float-center row">
                                <s:hidden name="modifyPasswordActive" id="modifyPasswordActive" />
                                <label class="col-lg-3 col-form-label form-control-label">Cambiar Contraseña</label>
                                <input id="togglePassword" type="checkbox" data-toggle="toggle" data-on="Si" data-off="No" data-onstyle="success" data-offstyle="danger" data-size="lg"/>
                            </div>
                            <br/>
                            <br/>
                            <div id="password-div" class="invisible">
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label form-control-label">Password</label>
                                    <div class="col-lg-9">
                                        <input class="form-control" type="password" name="password1" id="password1" />
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label form-control-label">Confirm password</label>
                                    <div class="col-lg-9">
                                        <input class="form-control" type="password" name="password2" id="password2" />
                                    </div>
                                </div>
                            </div>
                            <s:textfield type="hidden" name="active" id="active" />
                            <input id="toggleUser" type="checkbox" data-toggle="toggle" data-on="Activo" data-off="Inactivo"
                                   data-onstyle="success" data-offstyle="danger" data-size="large"/>
                            <div class="dgc-buttons-div">
                                <div class="dgc-button">
                                    <s:submit cssClass="btn btn-warning large" type="button" action="editUserAction" ><i class='cus-write'></i> Editar</s:submit>
                                    </div>
                                    <div class="dgc-button">
                                        <button class="btn" type="button" onclick="javascript:history.back();"><i class='cus-no-sign'></i> Cancelar</button>
                                    </div>
                                </div>
                        </s:form>
                    </div>
                    <div class="row-fluid">
                        <s:if test="hasActionErrors()">
                            <div class="alert alert-danger" role="alert">
                                <s:actionerror/>
                            </div>
                        </s:if>
                    </div>
                </section>
            </div>
        </div>
    </body>
    <jsp:include page="/WEB-INF/pages/common/footer.jsp">
        <jsp:param value="" name="" />
    </jsp:include>
</html>