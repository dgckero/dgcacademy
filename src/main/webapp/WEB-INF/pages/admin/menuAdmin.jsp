<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>

<ul class="list-unstyled components">
    <li>
        <a href="/DgcAcademy/user/goToListAllUsersAction">Ver Usuarios</a>
    </li>
    <li>
        <a href="/DgcAcademy/user/goToNewUserAction">Alta de Usuarios</a>
    </li>
    <li>
        <a href="/DgcAcademy/subject/goToListAllSubjectsAction">Ver Asignaturas</a>
    </li>
    <li>
        <a href="/DgcAcademy/subject/goToNewSubjectAction">Alta de Asignaturas</a>
    </li>
    <li>
        <a href="/DgcAcademy/user/goToAddUsersToSubjectAction">Asignar Profesores/Alumnos a Asignatura</a>
    </li>
    <li>
        <a href="/DgcAcademy/goToInfoAction">Información</a>
    </li>
</ul>
