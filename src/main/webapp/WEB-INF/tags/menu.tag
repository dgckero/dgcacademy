<%@ taglib prefix="s" uri="/struts-tags" %>

<div class="wrapper">
    <!-- Sidebar -->
    <nav id="sidebar">
        <div class="sidebar-header">
            <s:if test="user == null">
                <h3>Men�</h3>
            </s:if>
            <s:else>
                <s:if test="isAdmin()">
                    <h3>Men� Administrador</h3>
                </s:if>
                <s:if test="isTeacher()">
                    <h3>Men� Profesor</h3>
                </s:if>
                <s:if test="isStudent()">
                    <h3>Men� Estudiante</h3>
                </s:if>
            </s:else>
        </div>

        <s:if test="user == null">
            <ul class="list-unstyled components">
                <li>
                    <a href="/DgcAcademy/goToInfoAction">Informaci�n</a>
                </li>
            </ul>
        </s:if>
        <s:if test="isAdmin()">
            <jsp:include page="/WEB-INF/pages/admin/menuAdmin.jsp">
                <jsp:param value="" name="" />
            </jsp:include>
        </s:if>
        <s:if test="isTeacher()">
            <jsp:include page="/WEB-INF/pages/teacher/menuTeacher.jsp">
                <jsp:param value="" name="" />
            </jsp:include>
        </s:if>
        <s:if test="isStudent()">
            <jsp:include page="/WEB-INF/pages/student/menuStudent.jsp">
                <jsp:param value="" name="" />
            </jsp:include>
        </s:if>
    </nav>

</div>
