<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib prefix="s" uri="/struts-tags" %>

<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
    <a class="dgc-brand" href="/DgcAcademy/goToIndexAction.action">DgcAcademy</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsContainer" aria-controls="navbarsContainer" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse dropdown dropleft" id="navbarsContainer">

        <!-- Notificaciones -->
        <s:if test="hasNotifications()">
            <div class="toast dgc-toast" role="alert" aria-live="assertive" aria-atomic="true">
                <div class="toast-header">
                    <img src="" class="cus-comment-speech rounded mr-2" alt="">
                    <strong class="mr-auto">Notificación</strong>
                    <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="toast-body">
                    <s:if test="isTeacher()">
                        <a class="nav-link" href="/DgcAcademy/tutorial/goToViewTeacherTutorialRequestedAction">Tiene Peticiones de Tutoría Sin Contestar</a>
                    </s:if>
                    <s:elseif test="isStudent()">
                        <a class="nav-link" href="/DgcAcademy/tutorial/goToViewTutorialRequestedAction">Ver Actualizaciones en las Peticiones de Tutoría Solicitadas</a>
                    </s:elseif>
                </div>
            </div>
        </s:if>

        <ul class="nav navbar-nav ml-auto">
            <li class="nav-item dropdown">
                <c:choose>
                    <c:when test="${empty userLogged}">
                        <a class="dgc-dropdown-options nav-link dropdown-toggle" href="" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Login</a>
                    </c:when>
                    <c:otherwise>
                        <a class="dgc-dropdown-options nav-link dropdown-toggle" href="" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="cus-user"></i></a>
                        </c:otherwise>
                    </c:choose>
                <div class="dropdown-menu p-4" id="dropdown-menu-div">
                    <c:choose>
                        <c:when test="${ (not empty actionErrors) or (empty userLogged) }">
                            <form class="px-4 py-2" action="/DgcAcademy/user/loginAction" validate="true" method="post">
                                <div class="form-group">
                                    <label for="exampleDropdownFormEmail1">Usuario</label>
                                    <input type="text" required id="userInput" name="userName" class="form-control" id="exampleDropdownFormEmail1" placeholder="Usuario">
                                </div>
                                <div class="form-group">
                                    <label for="exampleDropdownFormPassword1">Contraseña</label>
                                    <input type="password" required id="passwordInput" name="password" class="form-control" id="exampleDropdownFormPassword1" placeholder="Contraseña">
                                </div>
                                <button type="submit" class="btn btn-primary" >Enviar</button>
                                <s:if test="( hasActionErrors() and #request['struts.actionMapping'].name == 'loginAction' )">
                                    <script>
                                        $('#dropdown-menu-div').show();
                                    </script>
                                    <div class="errors">
                                        <s:actionerror/>
                                    </div>
                                </s:if>
                            </form>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="/DgcAcademy/goToNewStudentAction">Eres Nuevo? Apúntate</a>
                        </c:when>
                        <c:otherwise>
                            <a class="dropdown-item" href="#">
                                <a class="dropdown-item" href="/DgcAcademy/user/showProfileAction"> ${userLogged.name}</a>
                                <a class="dropdown-item" href="/DgcAcademy/user/logoutAction">Log Out</a>                            
                            </a>                            
                        </c:otherwise>
                    </c:choose>
                </div>
            </li>
        </ul>
    </div>
</nav>
