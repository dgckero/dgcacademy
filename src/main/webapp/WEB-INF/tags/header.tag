<%@ attribute name="tittle" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sb" uri="/struts-bootstrap-tags" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-nested" prefix="nested" %>

<!-- javascript -->
<script src="/DgcAcademy/js/jquery-3.4.1.min.js"></script>
<script>window.jQuery || document.write('<script src="/DgcAcademy/js/jquery-3.4.1.min.js"><\/script>')</script>
<script src="/DgcAcademy/js/popper.min.js"</script>
<script src="/DgcAcademy/js/bootstrap.min.js"></script>
<script src="/DgcAcademy/js/jquery.dataTables.min.js"></script>
<script src="/DgcAcademy/js/moment.min.js"></script>
<script src="/DgcAcademy/js/daterangepicker.js"></script>
<script src="/DgcAcademy/js/tooltip.min.js"></script>
<script src="/DgcAcademy/js/dataTables.bootstrap4.min.js"></script>
<script src="/DgcAcademy/js/bootstrap4-toggle.min.js"></script>
<script src="/DgcAcademy/js/fullcalendar/core/main.js"></script>
<script src="/DgcAcademy/js/fullcalendar/list/main.js"></script>
<script src="/DgcAcademy/js/fullcalendar/daygrid/main.js"></script>
<script src="/DgcAcademy/js/fullcalendar/bootstrap/main.js"></script>
<script language="JavaScript" type="text/javascript">
    // Dojo configuration
    djConfig = {
        isDebug: false,
        bindEncoding: "UTF-8",
        baseRelativePath: "/DgcAcademy/struts/dojo/",
        baseScriptUri: "/DgcAcademy/struts/dojo/",
        parseWidgets: false
    };
</script>

<head>
    <sb:head/>
    <s:head/>  
    <!-- Required meta tags -->
    <meta charset="UTF-8">
    <title>${tittle}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="DgcAcademy">
    <meta name="author" content="DGC">
    <link rel="icon" type="image/icon" href="/DgcAcademy/ico/favicon.ico">

    <!-- estilos -->
    <link rel="stylesheet" type="text/css" href="/DgcAcademy/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="/DgcAcademy/css/dataTables.bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="/DgcAcademy/css/jquery.dataTables.css" />
    <link rel="stylesheet" type="text/css" href="/DgcAcademy/css/bootstrap4-toggle.min.css" />
    <link rel="stylesheet" type="text/css" href="/DgcAcademy/css/bootstrap-grid.css" />
    <link rel="stylesheet" type="text/css" href="/DgcAcademy/css/daterangepicker.css" />
    <link rel="stylesheet" type="text/css" href="/DgcAcademy/css/fullcalendar/core/main.css" />
    <link rel="stylesheet" type="text/css" href="/DgcAcademy/css/fullcalendar/list/main.css" />
    <link rel="stylesheet" type="text/css" href="/DgcAcademy/css/fullcalendar/daygrid/main.css" />
    <link rel="stylesheet" type="text/css" href="/DgcAcademy/css/fullcalendar/bootstrap/main.css" />
    <link rel="stylesheet" type="text/css" href="/DgcAcademy/css/dgcStyle.css" media="all" />
</head>
